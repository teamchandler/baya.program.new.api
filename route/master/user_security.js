
var user_security  = require('../../repo/master/user_security');
var util  = require('../../util'),
    audit = require('../audit_trail');
var logger =        util.get_logger("user_securty");
logger.info("user_security master started");

var keep_trail = function(business_id, business_id_name,
                          collection_name, db_name, field_name,
                          old_data, new_data,logger_msg){
    var a = new audit();
    a.business_id = business_id;
    a.business_id_name = business_id_name;
    a.collection_name = collection_name;
    a.db_name = db_name;
    a.field_name = field_name;
    a.old_data = old_data;
    a.new_data =  new_data;
    a.keep_trail();
    logger.info(logger_msg);
}

"use strict";


    this.help = function(req, res, next){

        var fs = require('fs');
        var content = fs.readFileSync('help/user_security.html');
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(content);

}

    this.add = function(req, res, next){
        var x=req.body.data;
        var user_security_data = {
            user_id : x.user_id,
            email : x.email||"",
            pwd : x.pwd||"",
            verification : {
                status : x.status||"",
                code : x.code||"",
                started :  new Date(),
                completed : x.completed||"",
                initiated_by:x.initiated_by||""
            }
        };

        var m = new user_security(user_security_data);

        m.save(function (err) {

            if(err){
                logger.info("error in user_security_data,err:" + err.message);
                util.send_to_response({msg:'record not saved',err:err}, res);
            }else{

                keep_trail(m._doc.user_id, "user_id", "user_security", "blaupunkt.node.api", "", "", m._doc, 'record saved successfully');
                util.send_to_response({msg:'record saved successfully'}, res);

            }
        });


    }
    this.edit = function(req, res, next){
        var x=req.body.data;
        var query = {"user_id": x.user_id};
        var user_security_data = { $set:{
            user_id : x.user_id,
            email : x.email||"",
            pwd : x.pwd||"",
            verification : {
                status : x.status||"",
                code : x.code||"",
                started :  new Date(),
                completed : x.completed||"",
                initiated_by:x.initiated_by||""
            }

        }};

        user_security.update_details(query,user_security_data)
            .then (
            function (results){
                keep_trail(user_security_data.user_id, "user_id", "user_security", "blaupunkt.node.api", "", "", user_security_data, 'record saved successfully');
                util.send_to_response({msg:'record saved successfully'}, res);
            },
            function (err){
                logger.info("error in user_data,err:" + err.message);
                util.send_to_response({msg:'record not saved',err:err}, res);

            });


    }
    this.delete_by_id = function(req, res, next){

    var query;
    if(req.body.user_id){
        query = {"user_id": req.body.user_id};
        user_security.delete(query).then(function (results){
                keep_trail(req.body.user_id, "user_id", "user_security", "blaupunkt.node.api", "", "", {}, 'record deleted successfully');
                util.send_to_response({msg:'record deleted successfully'}, res);
            },
            function (err){
                logger.info("error in user_data delete,err:" + err.message);
                util.send_to_response({msg:'record not deleted',err:err}, res);

            });
    }

};
    this.list_search = function(req, res, next){

        var query;
        if(req.body.user_id){
            query = {"user_id": new RegExp("^"+req.body.user_id,"i")};
        }

        if(req.body.email){
            query = {"email": new RegExp("^"+req.body.email,"i")};
        }
        if(req.body.status){
            query = {"verification.status": new RegExp("^"+req.body.status,"i")};
        }


        user_security.get_list(query,req.body.skip,req.body.limit).then(function (result) {
            util.send_to_response(result, res);
        },
        function (err) {
            logger.info("error in list_search,err:" + err.message);
            util.send_to_response(err, res);
        });
};
    this.get_detail = function(req, res, next){

        var query;
        if(req.body.user_id){
            query = {"user_id": req.body.user_id};
        }
        if(req.body.email){
            query = {"email": req.body.email};
        }
        if(req.body.status){
            query = {"verification.status": req.body.status};
        }


        user_security.get_list(query,0,0).then(function (result) {
                util.send_to_response(result, res);
            },
            function (err) {
                logger.info("error in list_search,err:" + err.message);
                util.send_to_response(err, res);
            });
};
    this.update_partially = function(req, res, next){

        var obj = {};
        for (var v in req.body.data) {
            obj[v] = req.body.data[v];
        }

        var query = req.body.id;
        var user_data = { $set:obj};

        user_security.update_details(query,user_data)
            .then (
            function (results){
                keep_trail(req.body.id.user_id, "user_id", "user_security", "blaupunkt.node.api", "", "", obj, 'record saved successfully');
                util.send_to_response({msg:'record saved successfully'}, res);
            },
            function (err){
                logger.info("error in user_data,err:" + err.message);
                util.send_to_response({msg:'record not saved',err:err}, res);

            });
};







