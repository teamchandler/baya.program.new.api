/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/2/14
 * Time: 6:16 AM
 * To change this template use File | Settings | File Templates.
 */
/* The API controller
 Exports 3 methods:
 * post - Creates a new thread
 * list - Returns a list of threads
 * show - Displays a thread and its posts
 */
//var fs = require ("fs");
var mysql = require("../../db_connect/mysql.js");
var util = require('../../util.js');
var token = require('../../token.js');
var mongoose = require('mongoose')
//var du = require('date-utils');
var cache = require('../../cache.js');
var shortid = require('shortid');
var aws = require('aws-sdk');
var knox = require('knox');
var Busboy = require('busboy');
//var pkgcloud = require('pkgcloud');
var fs = require('fs');
var path = require('path');
var mongodb = require('mongodb');

var async = require('async');
var _ = require("underscore");
//var inspect = require('util').inspect;


var claim = require('../../repo/master/claim');
var program = require('../../repo/master/program');
var logger = util.get_logger("api");
var region = require('../../repo/master/region');
var sku = require('../../repo/master/sku');
//var workflow = require('../model/workflow.js');
var user = require('../../repo/master/user');
var retailer = require('../../repo/master/retailer');

var sales_registry = require('../../repo/master/sales_registry');
var override_sku = require('../../repo/master/override_sku');
var pos = require('../../repo/master/pos');
var pointAccrual = require('../../repo/master/points_accrual');
var payment = require('../../repo/master/payment');

var pow_master = require('../../repo/master/pow_master');
//var user_points = require('../../repo/master/user_points');
//var sku_tran = require('../../repo/master/sku_tran');
//var sku_wise_point_history = require('../../repo/master/sku_wise_point_history');

var Execute_Time = new Date();

var counter_var = 0;


var MongoClient = mongodb.MongoClient;
var url = util.get_connection_string("mongo");

var serverDB = '';

//Open the Connection of MongoDB to carry out operations
MongoClient.connect(url, function (err, db) {
    if (err) {
        logger.info("Unable To Connect To The MongoDB Server. Error:" + err);
        serverDB = null;
    }
    else {
        //Database has been connected.
        logger.info("Connection Established To Big MongoDB");
        serverDB = db;
    }
})
exports.get_retailer_list = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Retailer List Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('retailer');
            // console.log(sku)
            var find_qry = {"active": 1};

            var project = {"_id": 1, "name": 1, "phone": 1, "city": 1};

            col_todo.find(find_qry, project).toArray(function (err, retailer_result) {
                if (err) {
                    logger.info("Error in getting retailer list " + err);
                }
                else {
                    logger.info("Sucessfully getting the retailer list" + retailer_result.length);
                    db.close();
                    if (retailer_result.length > 0) {
                        util.send_to_response(retailer_result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get Retailer List counter Value" + counter_var);
    });
};

exports.get_invoice_list = function (req, res) {

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Invoice List" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var find_qry = {"active": 1};
            var project = {"invoice_number": 1, "-_id": 1};
            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting invoice list" + err);
                }
                else {
                    logger.info("Sucessfully getting invoice list" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get Inovoice List" + counter_var);
    });
};

//********************  workflow  methods   *************************************

exports.get_workflow_by_company_region = function (req, res) {
    workflow.get_workflow_by_company_region(req.params.company, req.params.region)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_workflow_by_company_region ');
                });
                if (results.length > 0) {
                    send_to_response(results, res);
                } else {
                    mongoose.connection.close(function () {
                        console.log('   Mongoose connection disconnected get_workflow_by_company_region ');
                    });
                    send_empty_recordset(res);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_workflow_by_company_region ');
                });
                res.send(err);
            }
        );
};

exports.get_sku_list_detailed = function (req, res) {
    sku.get_detailed_sku_list(req.params.company, req.params.region_id)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_detailed_sku_list ');
                });
                send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_detailed_sku_list ');
                });
                res.send(err);
            }
        );
};


exports.get_all_sku_master = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get All SKU Master" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sku_master');
            // console.log(sku)

            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };

            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in fetching  sku master" + err);
                }
                else {
                    logger.info("Sucessfully getting sku master" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get All SKU Master" + counter_var);
    });
};

/*exports.get_sku_by_search_key = function (req, res) {

 var qry_todo = {};
 if (req.body.sku_search_key) {
 if (req.body.sku_search_key != null && req.body.sku_search_key != "") {
 qry_todo = {"sku": new RegExp("^.*?" + req.body.sku_search_key + ".*?", "i")};
 }
 }

 var MongoClient = mongodb.MongoClient;
 var url = util.get_connection_string("mongo");
 counter_var++;

 logger.info("Get SKU by Search Key"+counter_var);
 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 //Close connection
 db.close();
 }
 else {
 logger.info("Connection Established To MongoDB");
 var col_todo = db.collection('sku_master');
 // console.log(sku)

 var project = {
 "brand": 1,
 "sku": 1,
 "description": 1,
 "model": 1,
 "active": 1,
 "mrp": 1,
 "bluerewards": 1,
 "series": 1,
 "pow_status": 1
 };

 col_todo.find(qry_todo, project).toArray(function (err, result) {
 if (err) {
 logger.info("Error in checking part code in product of the week" + err);
 }
 else {
 logger.info("Sucessfully getting the part code in pow" + result.length);
 db.close();
 if (result.length > 0) {
 util.send_to_response(result, res)
 }
 }
 })
 }
 counter_var--;

 logger.info("End Get Sku By Search Key"+counter_var);
 });
 };*/

//******************** end  sku  methods   *************************************

//********************* Program Management ****************************************

exports.get_company_name = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Company Name Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)

            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };


            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting company name" + err);
                }
                else {
                    logger.info("Sucessfully getting the company name" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get Company Name  counter Value" + counter_var);
    });
};

exports.get_user_name_by_company = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    claim.get_user_name_by_company(req.params.name)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_user_name_by_company ');
                });
                send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_user_name_by_company ');
                });
                res.send(err);
            }
        );
};

//**************************************************************************

//********************  sku  methods   *************************************

exports.get_sku_list = function (req, res) {

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get SKU list counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sku_master');
            // console.log(sku)
            var company = req.params.company;
            var region_id = req.params.region_id;
            var pipeline = [{$unwind: "$region_value"},
                {
                    $match: {
                        $and: [
                            {"region_value.id": region_id},
                            {"company": company},
                            {"active": 1}
                        ]
                    }
                },
                {$project: {_id: 0, sku: 1, "value": "$region_value.value"}},
                {$sort: {sku: 1}}
            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting sku list" + err);
                }
                else {
                    logger.info("Sucessfully getting sku list" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get SKU List counter Value" + counter_var);
    });
};

exports.retailer_mongo_points_display=function (req,res)
{
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get SKU list counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var company = req.params.company;
            var region_id = req.params.region_id;
            var pipeline = [{$unwind: "$region_value"},
                {
                    $match: {
                        $and: [
                            {"region_value.id": region_id},
                            {"company": company},
                            {"active": 1}
                        ]
                    }
                },
                {$project: {_id: 0, sku: 1, "value": "$region_value.value"}},
                {$sort: {sku: 1}}
            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting sku list" + err);
                }
                else {
                    logger.info("Sucessfully getting sku list" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get SKU List counter Value" + counter_var);
    });

}

exports.get_sku_list_detailed = function (req, res) {
    sku.get_detailed_sku_list(req.params.company, req.params.region_id)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected get_detailed_sku_list ');
                });
                send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_detailed_sku_list ');
                });
                res.send(err);
            }
        );
};

exports.get_monthly_sku_wise_sale = function (req, res) {
    sales_registry.get_all_skus()
        .then(
            function (results) {
                try {
                    mongoose.connection.close(function () {
                        console.log('   Mongoose connection disconnected get_monthly_sku_wise_sale ');
                    });
                    if (results.length > 0) {

                        //console.log("********Execute Time : Total SKUs Form Schneider ********* ");
                        //console.log(results);

                        for (var i = 0; i < results.length; i++) {
                            results[i].qty_nov = 0;
                            results[i].qty_dec = 0;
                            results[i].qty_jan = 0;
                            results[i].qty_feb = 0;
                            results[i].qty_mar = 0;
                            results[i].qty_apr = 0;
                            results[i].qty_may = 0;
                            results[i].qty_june = 0;
                            results[i].qty_july = 0;
                            results[i].qty_august = 0;
                            results[i].qty_sep = 0;
                            results[i].qty_oct = 0;
                            results[i].qty_till_date = 0;
                            results[i].qty_today = 0;
                        }

                        //Get Already Credited Points For These Users
                        async.map(results, get_nov_sku_qty, function (err, r) {
                            for (var i = 0; i < r.length; i++) {
                                if (r[i][0]._id == null) {
                                    r[i][0].qty_nov = 0;
                                }
                                if (results[i]._id == r[i][0]._id) {
                                    results[i].qty_nov = r[i][0].qty_nov;
                                }
                            }

                            //Get All User Wise Sales Information
                            async.map(results, get_dec_sku_qty, function (err, r) {
                                for (var i = 0; i < r.length; i++) {
                                    if (r[i][0]._id == null) {
                                        r[i][0].qty_dec = 0;
                                    }
                                    if (results[i]._id == r[i][0]._id) {
                                        results[i].qty_dec = r[i][0].qty_dec;
                                    }
                                }

                                async.map(results, get_jan_sku_qty, function (err, r) {
                                    for (var i = 0; i < r.length; i++) {
                                        if (r[i][0]._id == null) {
                                            r[i][0].qty_jan = 0;
                                        }
                                        if (results[i]._id == r[i][0]._id) {
                                            results[i].qty_jan = r[i][0].qty_jan;
                                        }
                                    }

                                    async.map(results, get_feb_sku_qty, function (err, r) {
                                        for (var i = 0; i < r.length; i++) {
                                            if (r[i][0]._id == null) {
                                                r[i][0].qty_feb = 0;
                                            }
                                            if (results[i]._id == r[i][0]._id) {
                                                results[i].qty_feb = r[i][0].qty_feb;
                                            }
                                        }

                                        async.map(results, get_mar_sku_qty, function (err, r) {
                                            for (var i = 0; i < r.length; i++) {
                                                if (r[i][0]._id == null) {
                                                    r[i][0].qty_mar = 0;
                                                }
                                                if (results[i]._id == r[i][0]._id) {
                                                    results[i].qty_mar = r[i][0].qty_mar;
                                                }
                                            }

                                            async.map(results, get_apr_sku_qty, function (err, r) {
                                                for (var i = 0; i < r.length; i++) {
                                                    if (r[i][0]._id == null) {
                                                        r[i][0].qty_apr = 0;
                                                    }
                                                    if (results[i]._id == r[i][0]._id) {
                                                        results[i].qty_apr = r[i][0].qty_apr;
                                                    }
                                                }

                                                async.map(results, get_may_sku_qty, function (err, r) {
                                                    for (var i = 0; i < r.length; i++) {
                                                        if (r[i][0]._id == null) {
                                                            r[i][0].qty_may = 0;
                                                        }
                                                        if (results[i]._id == r[i][0]._id) {
                                                            results[i].qty_may = r[i][0].qty_may;
                                                        }
                                                    }

                                                    async.map(results, get_june_sku_qty, function (err, r) {
                                                        for (var i = 0; i < r.length; i++) {
                                                            if (r[i][0]._id == null) {
                                                                r[i][0].qty_june = 0;
                                                            }
                                                            if (results[i]._id == r[i][0]._id) {
                                                                results[i].qty_june = r[i][0].qty_june;
                                                            }
                                                        }

                                                        async.map(results, get_july_sku_qty, function (err, r) {
                                                            for (var i = 0; i < r.length; i++) {
                                                                if (r[i][0]._id == null) {
                                                                    r[i][0].qty_july = 0;
                                                                }
                                                                if (results[i]._id == r[i][0]._id) {
                                                                    results[i].qty_july = r[i][0].qty_july;
                                                                }
                                                            }

                                                            async.map(results, get_august_sku_qty, function (err, r) {
                                                                for (var i = 0; i < r.length; i++) {
                                                                    if (r[i][0]._id == null) {
                                                                        r[i][0].qty_august = 0;
                                                                    }
                                                                    if (results[i]._id == r[i][0]._id) {
                                                                        results[i].qty_august = r[i][0].qty_august;
                                                                    }
                                                                }

                                                                async.map(results, get_sep_sku_qty, function (err, r) {
                                                                    for (var i = 0; i < r.length; i++) {
                                                                        if (r[i][0]._id == null) {
                                                                            r[i][0].qty_sep = 0;
                                                                        }
                                                                        if (results[i]._id == r[i][0]._id) {
                                                                            results[i].qty_sep = r[i][0].qty_sep;
                                                                        }
                                                                    }

                                                                    async.map(results, get_oct_sku_qty, function (err, r) {
                                                                        for (var i = 0; i < r.length; i++) {
                                                                            if (r[i][0]._id == null) {
                                                                                r[i][0].qty_oct = 0;
                                                                            }
                                                                            if (results[i]._id == r[i][0]._id) {
                                                                                results[i].qty_oct = r[i][0].qty_oct;
                                                                            }
                                                                        }

                                                                        async.map(results, get_today_sku_qty, function (err, r) {
                                                                            for (var i = 0; i < r.length; i++) {
                                                                                if (r[i][0]._id == null) {
                                                                                    r[i][0].qty_today = 0;
                                                                                }
                                                                                if (results[i]._id == r[i][0]._id) {
                                                                                    results[i].qty_today = r[i][0].qty_today;
                                                                                }
                                                                            }

                                                                            if (results.length > 0) {
                                                                                for (var i = 0; i < results.length; i++) {
                                                                                    results[i].qty_till_date = (parseInt(results[i].qty_nov) + parseInt(results[i].qty_dec) + parseInt(results[i].qty_jan) + parseInt(results[i].qty_feb) + parseInt(results[i].qty_mar) + parseInt(results[i].qty_apr) + parseInt(results[i].qty_may) + parseInt(results[i].qty_june) + parseInt(results[i].qty_july) + parseInt(results[i].qty_august) + parseInt(results[i].qty_sep) + parseInt(results[i].qty_oct));
                                                                                }
                                                                            }

                                                                            res.send(results);
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    }
                } catch (e) {

                    //console.log("entering catch block");
                    console.log(e);
                    //console.log("leaving catch block");
                } finally {
                    console.log("entering and leaving the finally block");
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_monthly_sku_wise_sale ');
                });
                res.send(err);
            });

};

var get_nov_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 11)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_nov_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {
                    var qty_nov = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_nov = qty_nov + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_nov": qty_nov};
                    out_results.push(first_val);

                    return done_callback(null, out_results);
                }
                else {
                    first_val = {_id: null, "qty_nov": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_nov_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_dec_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 12)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_dec_sku_qty ');
                });
                var out_results = [];

                var first_val;

                if (results.length > 0) {

                    var qty_dec = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        qty_dec = qty_dec + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_dec": qty_dec};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_dec": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected get_dec_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_jan_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 1)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_jan_sku_qty ');
                });

                var out_results = [];
                var first_val;

                if (results.length > 0) {

                    var qty_jan = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_jan = qty_jan + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_jan": qty_jan};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                } else {
                    first_val = {_id: null, "qty_jan": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected get_jan_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_feb_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 2)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_feb_sku_qty ');
                });

                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_feb = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_feb = qty_feb + parseInt(obj_seleced_sku[0].quantity);
                    }
                    first_val = {_id: r._id, "qty_feb": qty_feb};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
                else {
                    first_val = {_id: null, "qty_feb": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('   error Mongoose connection disconnected get_feb_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_mar_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 3)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected get_mar_sku_qty ');
                });

                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_mar = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_mar = qty_mar + parseInt(obj_seleced_sku[0].quantity);
                    }
                    first_val = {_id: r._id, "qty_mar": qty_mar};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
                else {
                    first_val = {_id: null, "qty_mar": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('   error Mongoose connection disconnected get_mar_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_apr_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 4)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected get_apr_sku_qty ');
                });
                //console.log("Data For April");

                var out_results = [];
                if (results.length > 0) {

                    var qty_apr = 0;
                    var first_val;
                    //  console.log(results);
                    for (var i = 0; i < results.length; i++) {
                        //                    console.log("Enter Loop");
                        //                    console.log(results[i].claim_details);
                        //                    console.log(r._id);
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        //                    console.log(obj_seleced_sku);
                        qty_apr = qty_apr + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_apr": qty_apr};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_apr": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('   error Mongoose connection disconnected get_apr_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_may_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 5)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected get_may_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_may = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_may = qty_may + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_may": qty_may};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_may": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('   error Mongoose connection disconnected get_may_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_june_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 6)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected get_june_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_june = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_june = qty_june + parseInt(obj_seleced_sku[0].quantity);
                    }
                    first_val = {_id: r._id, "qty_june": qty_june};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_june": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error   Mongoose connection disconnected get_june_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_july_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 7)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected get_july_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_july = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_july = qty_july + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_july": qty_july};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_july": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('   error Mongoose connection disconnected get_july_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_august_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 8)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_august_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_august = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_august = qty_august + parseInt(obj_seleced_sku[0].quantity);
                    }
                    first_val = {_id: r._id, "qty_august": qty_august};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_august": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('   error Mongoose connection disconnected get_august_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_sep_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 9)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_sep_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_sep = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_sep = qty_sep + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_sep": qty_sep};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_sep": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error Mongoose connection disconnected get_sep_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_oct_sku_qty = function (r, done_callback) {

    sales_registry.get_month_wise_sku_qty(r._id, 10)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('  Mongoose connection disconnected get_oct_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_oct = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_oct = qty_oct + parseInt(obj_seleced_sku[0].quantity);
                    }
                    first_val = {_id: r._id, "qty_oct": qty_oct};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_oct": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error Mongoose connection disconnected get_oct_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

var get_today_sku_qty = function (r, done_callback) {

    var cur_date = new Date();

    sales_registry.get_today_sku_qty(r._id, cur_date.getFullYear(), (cur_date.getMonth() + 1), cur_date.getDate())
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_today_sku_qty ');
                });
                var out_results = [];
                var first_val;
                if (results.length > 0) {

                    var qty_today = 0;

                    for (var i = 0; i < results.length; i++) {
                        var obj_seleced_sku = _.where(results[i].claim_details, {sku: r._id});
                        // console.log(obj_seleced_sku);
                        qty_today = qty_today + parseInt(obj_seleced_sku[0].quantity);
                    }

                    first_val = {_id: r._id, "qty_today": qty_today};
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {_id: null, "qty_today": 0};
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error Mongoose connection disconnected get_today_sku_qty ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};
/*
 exports.get_all_sku_master = function (req, res) {
 counter_var++;
 logger.info("Get All SKU Counter value" + counter_var);
 var MongoClient = mongodb.MongoClient;
 var url = util.get_connection_string("mongo");

 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 //Close connection
 db.close();
 }
 else {
 logger.info("Connection Established To MongoDB");
 var col_sku_master = db.collection('sku_master');
 // console.log(sku)

 var project = {
 "brand": 1,
 "sku": 1,
 "description": 1,
 "model": 1,
 "active": 1,
 "mrp": 1,
 "bluerewards": 1,
 "series": 1,
 "pow_status": 1
 };
 col_sku_master.find({}, project).toArray(function (err, result) {
 if (err) {
 logger.info("Error in getting sku master" + err);
 }
 else {
 logger.info("Sucessfully getting sku master" + result.length);
 db.close();
 if (result.length > 0) {
 util.send_to_response(result, res)
 }
 }
 })
 }
 counter_var--;
 logger.info("Get All SKU Counter value" + counter_var);
 });

 };*/


//******************** end  sku  methods   *************************************
//********************  claims  methods   *************************************
exports.get_all_submissions = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get All Submissions Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)
            var user_id = req.params.user;
            var limit = req.params.limit;
            var pipeline = [
                {
                    $match: {
                        $and: [{user_id: user_id},
                            {active: 1}

                        ]
                    }
                },
                {
                    $project: {
                        _id: 0, claim_id: 1, invoice_date: 1,
                        'invoice_amount': 1, 'invoice_number': 1,
                        'retailer': 1, 'supporting_doc': 1,
                        'status': 1, 'claim_details': 1, 'approval_comments': 1
                    }
                },
                {$sort: {invoice_date: 1}},
                {$limit: parseInt(limit)}
            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting all submissions" + err);
                }
                else {
                    logger.info("Sucessfully getting all submissions" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get All Submission counter Value" + counter_var);
    });
};

exports.get_total_submitted_value = function (req, res) {


    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Total Submitted Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)
            var user_id = req.params.user;
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1}, {user_id: user_id},
                            {'status': 'pending'}
                        ]
                    }
                },
                {$group: {_id: "user_id", total: {$sum: "$invoice_amount"}}},
                {$project: {_id: 0, total_submitted_sale: "$total"}}
            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting total submiited values" + err);
                }
                else {
                    logger.info("Sucessfully getting  total submiited values" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get All submitted Value counter Value" + counter_var);
    });
};


exports.get_details_by_user = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get details By user counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var user_id = req.params.user;

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            {"user_id": user_id}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {'program_name': '$program_name', 'user_id': "$user_id"},
                        total_sale: {$sum: "$invoice_amount_excl_vat"},
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0, program_name: '$_id.program_name', 'user_id': '$_id.user_id',
                        total_sale: '$total_sale', total_count: '$total_count'
                    }
                },

                {$sort: {'user_id': 1}}


            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting details by user" + err);
                }
                else {
                    logger.info("Sucessfully getting details by user" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get Details By user   counter Value" + counter_var);
    })
};

exports.get_details_total_sale_by_user = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Details Total Sales Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var user_id = req.params.user;
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {"user_id": user_id}
//            {"status":"Verified & Approved"}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {'status': '$status', 'user_id': "$user_id"},
                        total_sale: {$sum: "$invoice_amount_excl_vat"},
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0, status: '$_id.status', 'user_id': '$_id.user_id',
                        total_sale: '$total_sale', total_count: '$total_count'
                    }
                }


            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in total sales details by user" + err);
                }
                else {
                    logger.info("Sucessfully getting total sales details by user" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
    counter_var--;
    logger.info("Get Details Total Sales Counter Value" + counter_var);
};

exports.get_user_details_for_zone_address = function (req, res) {
    var conn = mysql.get_mysql_connection();
    counter_var++;

    logger.info("Get User Details For Zone Address counter Value" + counter_var);
    var sql = "select * from user_registration where email_id = '" + req.params.user + "';";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
        });
    counter_var--;

    logger.info("End Get User Details Zone Address counter Value" + counter_var);
};

exports.posting_goal_value = function (req, res) {
    var conn = mysql.get_mysql_connection();
    counter_var++;

    logger.info("End Get Posting Goal Value  counter Value" + counter_var);
    var sql = "insert into ecomm.user_goal (email_id, goal, customized_goal) values ('" + req.body.user + "', '" + req.body.goal + "', 0)";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);

        });
    counter_var--;

    logger.info("End Get Posting Goal Value List counter Value" + counter_var);
};

exports.updating_goal_value = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "update ecomm.user_goal set goal = '" + req.body.goal + "' where email_id = '" + req.body.user + "'";
    counter_var++;

    logger.info("Updating Goal Value" + counter_var);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;
            res.send(rows);
        });
    counter_var--;

    logger.info("End Updating Goal Value counter Value" + counter_var);
};

exports.get_user_goal_value = function (req, res) {
    var conn = mysql.get_mysql_connection();
    counter_var++;
    logger.info("Get User Goal Counter Value" + counter_var);
    var sql = "select * from user_goal where email_id = '" + req.params.user + "';";
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;
            res.send(rows);
            //console.log(rows);
        });
    counter_var--;
    logger.info("End Get User Goal Counter Value" + counter_var);
};

exports.get_user_redeemed_points = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT sum(txn_amount) as redeemed FROM user_points where txn_type = 'd' and user_id = '" + req.params.user + "';";
    counter_var++;

    logger.info("Get User Redemmed Points" + counter_var);
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
            //console.log(rows);
        });
    counter_var--;

    logger.info("End Get Retailer List counter Value" + counter_var);
};

exports.get_user_proceedings_invoice_current_month = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get User Proceeding Invoice Current Month" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');

            var user = req.params.user;
            var current_date = req.params.current_date;
            // console.log(sku)
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {invoice_date: {$gte: new Date(current_date)}},
                            {user_id: user}
                        ]
                    }

                },
                {
                    $project: {
                        _id: 0,
                        companyname: '$company_name',
                        invoiceAmount: '$invoice_amount',
                        invoiceDate: '$invoice_date'
                    }
                },
                {$sort: {total_sale: -1}}

            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting user precedding invoice month" + err);
                }
                else {
                    logger.info("Sucessfully getting user precedding invoice month" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });

    counter_var--;
    logger.info("End Get User Proceeding Invoice Current Month" + counter_var);
};

exports.get_user_si_region_dashboard = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get User Si Region Dashboard" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var regionName = req.params.regionName;

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {region: regionName},
                            {status: 'Verified & Approved'}]
                    }
                },
                {
                    $group: {
                        _id: {'region': '$region', 'SI': "$company_name", 'user_id': "$user_id"},
                        total_sale: {$sum: "$invoice_amount"},
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0, region: '$_id.region', 'SI': '$_id.SI', 'user_id': '$_id.user_id',
                        total_sale: '$total_sale', total_count: '$total_count'
                    }
                },
                {$sort: {total_sale: -1}}

            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting user si region dashboard" + err);
                }
                else {
                    logger.info("Sucessfully getting user si region dashboard" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
    counter_var--;

    logger.info("End User SI Region Dashboard" + counter_var);

};

exports.get_user_si_allIndia_dashboard = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get User Si All Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'}]
                    }
                },
                {
                    $group: {
                        _id: {'region': '$region', 'SI': "$company_name", 'user_id': "$user_id"},
                        total_sale: {$sum: "$invoice_amount"},
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0, region: '$_id.region', 'SI': '$_id.SI', 'user_id': '$_id.user_id',
                        total_sale: '$total_sale', total_count: '$total_count'
                    }
                },
                {$sort: {total_sale: -1}}

            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting si all india dashboard" + err);
                }
                else {
                    logger.info("Sucessfully getting si all india dashborad" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });

    counter_var--;
    logger.info("End Get User Si All Counter Value" + counter_var);
};

//    exports.claims_by_company = function (req, res) {
//        //send_to_response([{"response": "from claim list"}], res);
//        claim.claims_by_company(req.params.company)
//            .then (
//                function (results){
//                    send_to_response(results, res);
//                },
//                function (err){
//                    res.send(err);
//                }
//
//        );
//    };
//
//    exports.claim_list = function (req, res) {
//
//        claim.claim_list()
//            .then (
//                function (results){
//                    send_to_response(results, res);
//                },
//                function (err){
//                    res.send(err);
//                }
//
//        );
//    };

exports.add_claim = function (req, res) {
    counter_var++;

    logger.info("Add Claim Dashboard Counter Value" + counter_var);
    var l_claim_id = util.get_program_name() + "/" + shortid.generate();
    var claim_data = {
        claim_id: l_claim_id,
        user_id: req.body.user_id,
        active: 1,
        invoice_date: new Date(req.body.invoice_date) //todo accept date here from caller
        ,
        invoice_amount: req.body.invoice_amount,
        invoice_number: req.body.invoice_number,
        retailer: req.body.retailer,
        status: req.body.status,
        supporting_doc: req.body.supporting_doc,
        approval_comment: req.body.approval_comment,
        claim_details: []
    };

    var c = new claim(claim_data);

    c.save(function () {
        mongoose.connection.close(function () {
            console.log('   Mongoose connection disconnected save ');
        });
        res.send('item saved');
    });
    counter_var--;

    logger.info(" End Add Claim Counter Value" + counter_var);
};

//exports.update_sku_quantity = function (req, res) {
//
//    var sku_data = {
//        "sku" : req.body.sku,
//        "frm_participant" :"" ,
//        "to_participant" : "",
//        "flow_type" : 1,
//        "tran_type" : "d",
//        "quantity" :0 ,
//        "credit_quantity" : 0,
//        "debit_quantity" :req.body.debit_quantity,
//        "active" : 1,
//        "created_date" : new Date()
//    };
//
//    var c = new sku_tran(sku_data);
//
//    c.save(function () {
//        res.send('record saved successfully');
//    });
//};

exports.add_override_sku = function (req, res) {
    counter_var++;

    logger.info("Get Add Overrride SKU Counter Value " + counter_var);
    var override_sku_data = {
        claim_id: req.body.claim_id,
        user_id: req.body.user_id,
        active: 1,
        sku: req.body.sku,
        quantity: req.body.quantity,
        created_date: new Date()
    };

    var c = new override_sku(override_sku_data);

    c.save(function () {
        mongoose.connection.close(function () {
            console.log('Mongoose connection disconnected save ');
        });
        res.send('record saved successfully');
    });
    counter_var--;

    logger.info("Get Add Overrride SKU Counter Value" + counter_var);
};

exports.add_sku_details = function (req, res) {

    counter_var++;

    logger.info("Get Add SKU Details Counter Value" + counter_var);
    var sku_detail_data = [];

    for (var i = 0; i < req.body.length; i++) {

        var sku_details_data = {
            sku: req.body[i].sku,
            description: req.body[i].description,
            active: 1,
            model: req.body[i].model,
            mrp: req.body[i].mrp,
            bluerewards: req.body[i].bluerewards
        };

        var c = new sku(sku_details_data);

        c.save(function () {
            mongoose.connection.close(function () {
                console.log('Mongoose connection disconnected save ');
            });
            res.send('record saved successfully');
        });

        // sku_detail_data.push(sku_details_data);
    }
    counter_var--;

    logger.info("End Add SKu Details" + counter_var);
};

//**********Invoce Save Schneider***********//

function get_program_name(programs, invoce_date) {
    counter_var++;

    logger.info("Get Program Counter Value " + counter_var);
    var prg_name = "nrml";

    if (programs.length > 0) {
        for (var i = 0; i < programs.length; i++) {
            if (dateCheck(programs[i].start_date, programs[i].end_date, invoce_date)) {
                prg_name = programs[i].code;
            }
        }
    }
    counter_var--;

    logger.info("End Get Program Counter Value" + counter_var);
    return prg_name;
}


function save_part_code_details(data) {

    if (data.length > 0) {

        for (var i = 0; i < data.length; i++) {

            var sku_details_data = {
                sku: data[i].sku,
                description: data[i].description,
                active: 1,
                model: data[i].model,
                mrp: data[i].mrp,
                bluerewards: data[i].bluerewards
            };

            var c = new sku(sku_details_data);

            c.save(function () {
                mongoose.connection.close(function () {
                    console.log('Mongoose connection disconnected save ');
                });
                res.send('record saved successfully');
            });

            // sku_detail_data.push(sku_details_data);
        }
    }
}

function dateCheck(from, to, check) {
    counter_var++;

    logger.info("Get Date Check Counter Value" + counter_var);
    var fDate, lDate, cDate;
    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);

    if ((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    counter_var--;

    logger.info("End Get Date Check Counter Value" + counter_var);
    return false;
}

var search_invoice = function (inv, done_callback) {
    counter_var++;

    logger.info("Get Search Invoice" + counter_var);
    sales_registry.get_all_invoice(inv.invoice_number, inv.retailer)
        .then(
            function (results) {
                var out_results = [];
                try {
                    if (results.length > 0) {
                        out_results.push(results);
                        return done_callback(null, out_results);
                    } else {
                        out_results.push(results);
                        return done_callback(null, out_results);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('error Mongoose connection disconnected save ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
    counter_var--;

    logger.info("End  Search Invoice Counter Value" + counter_var);
};

var get_distiwise_sku_detail = function (inv, done_callback) {
    counter_var++;

    logger.info("Get DistiWise SKu Details Counter Value" + counter_var);
    sales_registry.get_distiwise_sku_detail()
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('Mongoose connection disconnected get_distiwise_sku_detail ');
                });
                //            console.log('Get Sales Registry Data');
                //            console.log(results);
                var out_results = [];

                try {
                    if (results.length > 0) {
                        out_results.push(results);
                        return done_callback(null, out_results);

                    } else {
                        out_results.push(results);
                        return done_callback(null, out_results);
                    }
                } catch (ex) {

                    console.log(ex);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('error Mongoose connection disconnected get_distiwise_sku_detail ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
    counter_var--;

    logger.info("End  Get DistiWise SKu Details Counter Value" + counter_var);
};


var get_distiwise_sku_detail_desc = function (inv, done_callback) {
    counter_var++;

    logger.info("Get DistiWise SKu Details Counter Value" + counter_var);
    sales_registry.get_distiwise_sku_detail_desc()
        .then(
            function (results) {

                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_distiwise_sku_detail_desc ');
                });
                //            console.log('Get Sales Registry Data');
                //            console.log(results);
                var out_results = [];

                try {
                    if (results.length > 0) {

                        out_results.push(results);

                        return done_callback(null, out_results);

                    } else {
                        out_results.push(results);
                        return done_callback(null, out_results);
                    }
                } catch (ex) {

                    console.log(ex);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('error Mongoose connection disconnected get_distiwise_sku_detail_desc ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
    counter_var--;

    logger.info(" End Get DistiWise SKu Details Counter Value" + counter_var);
};

var get_regionwise_sku_detail = function (inv, done_callback) {

    sales_registry.get_regionwise_sku_detail()
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_regionwise_sku_detail ');
                });
                //            console.log('Get Sales Registry Data');
                //            console.log(results);
                var out_results = [];

                try {
                    if (results.length > 0) {
                        out_results.push(results);
                        return done_callback(null, out_results);
                    } else {
                        out_results.push(results);
                        return done_callback(null, out_results);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('error Mongoose connection disconnected get_regionwise_sku_detail ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

/*exports.add_invoice = function (req, res) {

 counter_var++;
 logger.info("Add Invoice Counter Value" + counter_var);
 logger.info("Invoice Add Call In api.js");
 console.log("incoming" + JSON.stringify(req.body));

 var l_claim_id = util.get_program_name() + "-" + shortid.generate();
 var programs = util.get_programs_details();
 var prog_name = get_program_name(programs, req.body.invoice_date);

 var amt_arr = [];
 //    var amt_val={user_id: req.body.user_id,track_date: new Date(),invoice_amount: req.body.invoice_amount,verified_amount:0};
 //    amt_arr.push(amt_val);

 var amt_val = {
 user_id: req.body.user_id,
 track_date: new Date(),
 invoice_amount: req.body.invoice_amount,
 invoice_amount_excl_vat: req.body.invoice_amount_excl_vat
 };
 amt_arr.push(amt_val);
 var inv_date_arr = [];
 var inv_date_val = {user_id: req.body.user_id, inv_date: req.body.invoice_date, track_date: new Date()};
 inv_date_arr.push(inv_date_val);

 var test = [];

 var acroot_point = (req.body.invoice_amount / 100) * 1;

 var claim_data = {
 sales_id_1: shortid,
 claim_id: l_claim_id,
 user_id: req.body.user_id,
 active: 1,
 invoice_date: req.body.invoice_date, //todo accept date here from caller
 invoice_amount: req.body.invoice_amount,
 invoice_amount_excl_vat: req.body.invoice_amount_excl_vat,
 invoice_number: req.body.invoice_number,
 retailer: req.body.retailer,
 status: "pending",
 supporting_doc: req.body.supporting_doc,
 program_name: prog_name,
 region: req.body.region,
 claim_details: [],
 approval_comments: [],
 amount_track: amt_arr,
 inv_date_track: inv_date_arr,
 company_name: req.body.company_name,
 created_date: new Date(),
 modified_by: "",
 modified_date: "",
 ispoint_calculated: 1,
 retailer_code: req.body.retailer_code,
 retailer_name: req.body.retailer_name,
 participant_id: req.body.participant_id,
 upload_from: req.body.upload_from,
 uploaded_month: req.body.uploaded_month,
 payment_received: "N",
 credit_period: req.body.credit_period,
 points_accrued: acroot_point,
 created_by: req.body.created_by,
 disti_champ_code: req.body.distributor_data[0].disti_champ_code,
 disti_champ_name: req.body.distributor_data[0].disti_champ_name,
 distributor_code: req.body.distributor_data[0].distributor_code,
 distributor_name: req.body.distributor_data[0].distributor_name,
 distributor_city: req.body.distributor_data[0].city
 };

 // logger.info("Creating The Invoice Data");

 //console.log('claim_data');
 //console.log(claim_data);

 var accrual_data = {
 user_id: req.body.user_id,
 accrual_point: acroot_point,
 active: 1,
 invoice_number: req.body.invoice_number,
 created_date: new Date(),
 invoice_amount: req.body.invoice_amount
 };

 //test.push(claim_data);
 // var check = search_invoice(req.body.invoice_number);
 logger.info("Checking Invoice Existance : " + claim_data.invoice_number, claim_data.retailer);

 sales_registry.get_all_invoice(claim_data.invoice_number, claim_data.retailer).then(
 function (results) {

 var out_results = [];
 logger.info("Checking Invoice Existance Result");
 logger.info(results);
 try {
 if (results.length > 0) {
 logger.info("Invoice Already Exist");

 res.send('Invoice Already Exist');
 } else {
 logger.info("Invoice Not Exist");
 var c = new sales_registry(claim_data);
 var d = new pointAccrual(accrual_data);
 //logger.info(claim_data);
 c.save(function (err, save_results) {
 if (err) {
 logger.info("Hassan Error Occured." + err);
 res.send("Error Occured." + err);
 } else {
 logger.info("Invoice Added In Sales Registry");
 d.save(function () {
 logger.info("Points Added In Point Accrual");


 res.send('Claim Saved,Please Check Status In MY CLAIM Page');
 });
 // res.send('Claim saved,Please check status in MY CLAIM page');
 }
 });
 }
 } catch (ex) {
 console.log(ex);
 }
 },
 function (err) {
 res.send("Error Occured." + err);
 }
 );

 };*/

exports.add_invoice = function (req, res) {

    logger.info("Invoice Add Call In api.js");

    var l_claim_id = util.get_program_name() + "-" + shortid.generate();
    var programs = util.get_programs_details();
    var prog_name = get_program_name(programs, req.body.invoice_date);
console.log("get_all_invoice"+JSON.stringify(req.body.invoice_date));
    var amt_arr = [];
    //    var amt_val={user_id: req.body.user_id,track_date: new Date(),invoice_amount: req.body.invoice_amount,verified_amount:0};
    //    amt_arr.push(amt_val);

    var amt_val = {
        user_id: req.body.user_id,
        track_date: new Date(),
        invoice_amount: req.body.invoice_amount,
        invoice_amount_excl_vat: req.body.invoice_amount_excl_vat
    };
    amt_arr.push(amt_val);


    var inv_date_arr = [];
    var inv_date_val = {user_id: req.body.user_id, inv_date: req.body.invoice_date, track_date: new Date()};
    inv_date_arr.push(inv_date_val);

    var test = [];

    var acroot_point = (req.body.invoice_amount / 100) * 1;

    var claim_data = {
        sales_id_1: shortid,
        claim_id: l_claim_id,
        user_id: req.body.user_id,
        active: 1,
        invoice_date: req.body.invoice_date, //todo accept date here from caller
        invoice_amount: req.body.invoice_amount,
        invoice_amount_excl_vat: req.body.invoice_amount_excl_vat,
        invoice_number: req.body.invoice_number,
        retailer: req.body.retailer,
        status: "pending",
        supporting_doc: req.body.supporting_doc,
        program_name: prog_name,
        region: req.body.region,
        claim_details: [],
        approval_comments: [],
        amount_track: amt_arr,
        inv_date_track: inv_date_arr,
        company_name: req.body.company_name,
        created_date: new Date(),
        modified_by: "",
        modified_date: "",
        ispoint_calculated: 1,
        retailer_code: req.body.retailer_code,
        retailer_name: req.body.retailer_name,
        participant_id: req.body.participant_id,
        upload_from: req.body.upload_from,
        uploaded_month: req.body.uploaded_month,
        payment_received: "N",
        credit_period: req.body.credit_period,
        points_accrued: acroot_point,
        created_by: req.body.created_by,

        disti_champ_code: req.body.distributor_data[0].disti_champ_code,
        disti_champ_name: req.body.distributor_data[0].disti_champ_name,
        distributor_code: req.body.distributor_data[0].distributor_code,
        distributor_name: req.body.distributor_data[0].distributor_name,
        distributor_city: req.body.distributor_data[0].city

    };

    logger.info("Creating The Invoice Data");

    console.log('claim_data');
    console.log(claim_data);

    var accrual_data = {
        user_id: req.body.user_id,
        accrual_point: acroot_point,
        active: 1,
        invoice_number: req.body.invoice_number,
        created_date: new Date(),
        invoice_amount: req.body.invoice_amount
    };

    //test.push(claim_data);
    // var check = search_invoice(req.body.invoice_number);
    logger.info("Checking Invoice Existance : " + claim_data.invoice_number, claim_data.retailer);

    sales_registry.get_all_invoice(claim_data.invoice_number, claim_data.retailer)
        .then(
            function (results) {
                var out_results = [];
                logger.info("Checking Invoice Existance Result");
                logger.info(results);
                try {
                    if (results.length > 0) {
                        logger.info("Invoice Already Exist");
                        res.send('Invoice Already Exist');
                    } else {
                        logger.info("Invoice Not Exist");
                        var c = new sales_registry(claim_data);
                        var d = new pointAccrual(accrual_data);
                        //logger.info(claim_data);
                        c.save(function (err, save_results) {
                            if (err) {
                                logger.info("Hassan Error Occured." + err);
                                res.send("Error Occured." + err);
                            } else {
                                logger.info("Invoice Added In Sales Registry");
                                d.save(function () {
                                    logger.info("Points Added In Point Accrual");
                                    // res.send('Claim Saved,Please Check Status In MY CLAIM Page');
                                    mongoose.connection.close(function () {
                                        console.log('Mongoose connection disconnected add_invoice ');
                                    });
                                    util.send_to_response('Claim Saved,Please Check Status In MY CLAIM Page', res)

                                });
                                // res.send('Claim saved,Please check status in MY CLAIM page');
                            }
                        });
                    }
                } catch (ex) {
                    console.log(ex);
                }
            },
            function (err) {
                res.send("Error Occured." + err);
            }
        );

};


exports.add_invoice_csv_upload = function (req, res) {
    console.log('Input');
    console.log(req.body.all_data);
    var l_claim_id = util.get_program_name() + "-" + shortid.generate();
    var programs = util.get_programs_details();
    var prog_name = get_program_name(programs, req.body.all_data.invoice_date);

    var amt_arr = [];
    //    var amt_val={user_id: req.body.user_id,track_date: new Date(),invoice_amount: req.body.invoice_amount,verified_amount:0};
    //    amt_arr.push(amt_val);

    var amt_val = {
        user_id: req.body.all_data.user_id,
        track_date: new Date(),
        invoice_amount: req.body.all_data.invoice_amount,
        invoice_amount_excl_vat: req.body.all_data.invoice_amount_excl_vat
    };
    amt_arr.push(amt_val);
    var inv_date_arr = [];
    var inv_date_val = {
        user_id: req.body.all_data.user_id,
        inv_date: req.body.all_data.invoice_date,
        track_date: new Date()
    };
    inv_date_arr.push(inv_date_val);

    var test = [];

    var acroot_point = (req.body.all_data.invoice_amount / 100) * 1;

    var claim_data = {
        claim_id: l_claim_id,
        user_id: req.body.all_data.user_id,
        active: 1,
        invoice_date: req.body.all_data.invoice_date //todo accept date here from caller
        ,
        invoice_amount: req.body.all_data.invoice_amount,
        invoice_amount_excl_vat: req.body.all_data.invoice_amount_excl_vat,
        invoice_number: req.body.all_data.invoice_number,
        retailer: req.body.all_data.retailer,
        status: "pending",
        supporting_doc: req.body.all_data.supporting_doc,
        program_name: prog_name,
        region: req.body.region,
        claim_details: [],
        approval_comments: [],
        amount_track: amt_arr,
        inv_date_track: inv_date_arr,
        company_name: req.body.all_data.company_name,
        created_date: new Date(),
        modified_by: "",
        modified_date: "",
        ispoint_calculated: 1,
        retailer_code: req.body.all_data.retailer_code,
        retailer_name: req.body.all_data.retailer_name,
        participant_id: req.body.all_data.participant_id,
        upload_from: req.body.all_data.upload_from,
        uploaded_month: req.body.all_data.uploaded_month,
        payment_received: "N",
        credit_period: req.body.all_data.credit_period,
        points_accrued: acroot_point
    };

    var accrual_data = {
        user_id: req.body.all_data.user_id,
        accrual_point: acroot_point,
        active: 1,
        invoice_number: req.body.all_data.invoice_number,
        created_date: new Date(),
        invoice_amount: req.body.all_data.invoice_amount
    };

    test.push(claim_data);
    // var check = search_invoice(req.body.invoice_number);

    async.map(test, search_invoice, function (err, r) {

        if (r[0][0].length > 0) {
            res.send('Invoice Exist');

        } else {
            var c = new sales_registry(claim_data);

            var d = new pointAccrual(accrual_data);

            c.save(function () {

                d.save(function () {
                    mongoose.connection.close(function () {
                        console.log('Mongoose connection disconnected add_invoice_csv_upload');
                    });
                    res.send('Claim saved,Please check status in MY CLAIM page');

                });
                // res.send('Claim saved,Please check status in MY CLAIM page');
            });
        }
    });
};

/*****Done By Souvick on Dated 29/1/16******/

// Add Payment Info
exports.add_payment_details = function (req, res) {

    var payment_track_array = []; //assigning an array

    //assigning an object
    var payment_track = {
        amount_received: req.body.amount_received,
        payment_received: req.body.payment_received,
        payment_date: req.body.payment_date,
        modified_date: new Date(),
        modified_by: req.body.user_id,
        payment_comment: req.body.payment_comment
    };

    //get payment track details by invoice number
    sales_registry.payment_track_details(req.body.invoice_number)
        .then(
            function (results) {
                console.log("Invoice Details : " + JSON.stringify(results[0]));
                var q = results[0].payment_track;
                var credit_period_date = results[0].credit_period;
                var within_credit_period = "N";
                var inv_dt = results[0].invoice_date;
                payment_track_array = q;
                // Checking whether the credit period date is over or notar
                //console.log("Payment Date : " + req.body.payment_date);
                //console.log("Invoice Date : " + inv_dt);

                var payment_date = new Date(req.body.payment_date);
                var invoice_dt = new Date(inv_dt);

                //console.log("Payment Date : " + payment_date);
                //console.log("Invoice Date : " + invoice_dt);

                var diffDays = parseInt((payment_date - invoice_dt) / (1000 * 60 * 60 * 24)) + 1;

                console.log("2 Date Difference : " + diffDays);

                var credit_period = new Date(credit_period_date);
                var payment_date1 = payment_date.getFullYear() + '-' + (payment_date.getMonth() + 1) + '-' + payment_date.getDate();
                var credit_period1 = credit_period.getFullYear() + '-' + (credit_period.getMonth() + 1) + '-' + credit_period.getDate();

                if (diffDays <= 22 && diffDays > 0) {
                    within_credit_period = "Y";
                    //console.log('Payment Received Within Due Date .');
                } else {
                    within_credit_period = "N";
                }

                // End

                //check if duplicate invoice number exist if so then it update the same table if not then it create a new table
                payment.get_duplicate_invoice(req.body.invoice_number)
                    .then(
                        function (results) {
                            if (results.length > 0) {
                                payment.payment_update(req.body)
                                    .then(
                                        function () {
                                            console.log("Updates"); //check

                                            payment_track_array.push(payment_track); //it pushes the object in array

                                            //Update the payment table
                                            sales_registry.update_payment_recieved_new(req.body, payment_track_array, within_credit_period, req.body.amount_received)
                                                .then(
                                                    function () {
                                                        console.log("Updates");
                                                        mongoose.connection.close(function () {
                                                            console.log('Mongoose connection disconnected get_duplicate_invoice ');
                                                        });
                                                        res.send('Payment Information Saved');
                                                    },
                                                    function (err) {
                                                        mongoose.connection.close(function () {
                                                            console.log('Mongoose connection disconnected get_duplicate_invoice ');
                                                        });
                                                        res.send(err);
                                                    }
                                                );
                                            //                                res.send('Payment Information Updated');
                                        },
                                        function (err) {
                                            mongoose.connection.close(function () {
                                                console.log('Mongoose connection disconnected get_duplicate_invoice ');
                                            });
                                            res.send(err);
                                        }
                                    )
                            }
                            else {
                                //create a object payment data
                                var payment_data = {
                                    retailer_name: req.body.retailer_name,
                                    invoice_number: req.body.invoice_number,
                                    active: 1,
                                    payment_date: req.body.payment_date,
                                    amount_received: req.body.amount_received,
                                    payment_received: req.body.payment_received,
                                    payment_comment: req.body.payment_comment
                                };

                                payment_track_array.push(payment_track); //Pushes object in array

                                var p = new payment(payment_data); //put data in repo
                                //save data
                                p.save(function () {
                                    sales_registry.update_payment_recieved(req.body, payment_track_array, within_credit_period, req.body.amount_received)
                                        .then(
                                            function () {
                                                console.log("Updates");
                                                mongoose.connection.close(function () {
                                                    console.log('Mongoose connection disconnected update_payment_recieved ');
                                                });
                                                res.send('Payment Information Saved');
                                            },
                                            function (err) {
                                                mongoose.connection.close(function () {
                                                    console.log(' error Mongoose connection disconnected update_payment_recieved ');
                                                });
                                                res.send(err);
                                            }
                                        );
                                });
                            }
                        },
                        function (err) {
                            mongoose.connection.close(function () {
                                console.log('error Mongoose connection disconnected update_payment_recieved ');
                            });
                            res.send(err);
                        }
                    );
                //send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error Mongoose connection disconnected update_payment_recieved ');
                });
                res.send(err);
            }
        );
};

// End

// Add Payment Info By CSV

exports.add_payment_details_from_csv = function (req, res) {

    var received_data = req.body;
    var pr = received_data.payment_received.replace(/(\r\n|\n|\r)/gm, "");
    req.body.payment_received = pr;
    req.body.user_id = "Admin";

    var within_credit_period = "";
    var payment_track_array = []; //assigning an array

    //assigning an object
    var payment_track = {
        amount_received: req.body.amount_received,
        payment_received: req.body.payment_received,
        payment_date: req.body.payment_date,
        modified_date: new Date(),
        modified_by: req.body.user_id
    };

    //get payment track details by invoice number
    sales_registry.payment_track_details(req.body.invoice_number)
        .then(
            function (results) {
                console.log(results[0].payment_track);
                var q = results[0].payment_track;
                var credit_period_date = results[0].credit_period;

                //console.log("track details");
                //console.log(q);

                payment_track_array = q;

                //console.log(payment_track_array);
                //console.log(q);

                var payment_date = new Date(req.body.payment_date);
                var credit_period = new Date(credit_period_date);
                var payment_date1 = payment_date.getFullYear() + '-' + (payment_date.getMonth() + 1) + '-' + payment_date.getDate();
                var credit_period1 = credit_period.getFullYear() + '-' + (credit_period.getMonth() + 1) + '-' + credit_period.getDate();
                if (payment_date1 > credit_period1) {
                    within_credit_period = "N";
                    //console.log('Payment date greater than credit period');
                }

                //check if duplicate invoice number exist if so then it update the same table if not then it create a new table
                payment.get_duplicate_invoice(req.body.invoice_number)
                    .then(
                        function (results) {
                            if (results.length > 0) {

                                payment.payment_update(req.body)
                                    .then(
                                        function () {
                                            //console.log("Updates"); //check
                                            mongoose.connection.close(function () {
                                                console.log('  Mongoose connection disconnected payment_update ');
                                            });
                                            payment_track_array.push(payment_track); //it pushes the object in array

                                            //Update the sales_registry table
                                            sales_registry.update_payment_recieved_new(req.body, payment_track_array, within_credit_period, req.body.amount_received)
                                                .then(
                                                    function () {
                                                        //console.log("Updates");
                                                        mongoose.connection.close(function () {
                                                            console.log('  Mongoose connection disconnected update_payment_recieved_new ');
                                                        });
                                                        res.send('Payment Information Saved');
                                                    },
                                                    function (err) {
                                                        mongoose.connection.close(function () {
                                                            console.log('  error Mongoose connection disconnected update_payment_recieved_new ');
                                                        });
                                                        res.send(err);
                                                    }
                                                );
                                            //                                res.send('Payment Information Updated');
                                        },
                                        function (err) {
                                            mongoose.connection.close(function () {
                                                console.log(' error   Mongoose connection disconnected update_payment_recieved_new ');
                                            });
                                            res.send(err);
                                        })
                            } else {

                                //create a object payment data
                                var payment_data = {
                                    retailer_name: req.body.retailer_name,
                                    invoice_number: req.body.invoice_number,
                                    active: 1,
                                    payment_date: req.body.payment_date,
                                    amount_received: req.body.amount_received,
                                    payment_received: req.body.payment_received
                                };

                                payment_track_array.push(payment_track); //Pushes object in array

                                var p = new payment(payment_data); //put data in repo
                                //save data
                                p.save(function () {
                                    sales_registry.update_payment_recieved(req.body, payment_track_array, req.body.amount_received)
                                        .then(
                                            function () {
                                                console.log("Updates");
                                                mongoose.connection.close(function () {
                                                    console.log('    Mongoose connection disconnected update_payment_recieved ');
                                                });
                                                res.send('Payment Information Saved');
                                            },
                                            function (err) {
                                                mongoose.connection.close(function () {
                                                    console.log(' error   Mongoose connection disconnected update_payment_recieved ');
                                                });
                                                res.send(err);
                                            }
                                        );
                                });
                            }
                        },
                        function (err) {
                            mongoose.connection.close(function () {
                                console.log(' error   Mongoose connection disconnected update_payment_recieved ');
                            });
                            res.send(err);
                        }
                    );
                send_to_response(results, res);
            },
            function (err) {
                res.send(err);
            }
        );
};

// End

// Get Invoice Details

exports.get_details_by_invoice_number = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Details By Invoice Number Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            // logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var number = req.params.invoice_number;
            var find_qry = {invoice_number: number};
            var project = {
                "retailer_name": 1,
                "invoice_number": 1,
                "invoice_amount": 1,
                "invoice_date": 1,
                " payment_date": 1,
                "payment_received": 1,
                " user_id": 1,
                " status ": 1,
                "_id": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting details by invoice number" + err);
                }
                else {
                    logger.info("Sucessfully getting details by invoice number" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End  Details By Invoice Number Counter Value" + counter_var);
    });

};

// End

//Get payment details
exports.get_payment_details = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Payment Details Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var number = req.body.invoice_number;
            var find_qry = {invoice_number: number};

            var project = {
                "retailer_name": 1,
                "invoice_number": 1,
                "invoice_amount": 1,
                "invoice_date": 1,
                "payment_date": 1,
                " payment_received": 1,
                "user_id": 1,
                "status": 1,
                " -_id": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting payment details" + err);
                }
                else {
                    logger.info("Sucessfully getting payment details" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Payment Details Counter Value" + counter_var);
    });
};

//Update invoice
exports.update_invoice = function (req, res) {
    user.update_invoice(req.body.data)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected update_invoice ');
                });
                send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected update_invoice ');
                });
                res.send(err);
            }
        );
};

exports.get_inv_by_retailer = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var qry = {};
    if (req.body.retailer_code) {
        qry = {
            "retailer_code": req.body.retailer_code,
            "payment_received": "N",
            "status": "Verified & Approved"
        }
    }

    //console.log(qry);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Invoice By Retailers Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var project = {
                "retailer_name": 1,
                "invoice_number": 1,
                "invoice_amount": 1,
                "invoice_date": 1,
                "payment_date": 1,
                "payment_received": 1,
                "user_id": 1,
                "status": 1,
                "-_id": 1
            };

            col_todo.find(qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Invoice By Retailers Counter Value" + counter_var);
    });
};


// Done by Souvick

exports.get_invoice_details_by_invoice_number = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Details By Invoice Number Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var number = req.body.invoice_number;
            var find_qry = {invoice_number: number};

            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting invoice details by number" + err);
                }
                else {
                    logger.info("Sucessfully getting the inoivce details by number" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Details By Invoice Number Counter Value" + counter_var);
    });
};

// End

exports.get_all_company_name = function (req, res) {

    var qry_todo = {};
    if (req.body.retailer_search) {
        if (req.body.retailer_search != null && req.body.retailer_search != "") {
            qry_todo = {
                $or: [
                    {"retailer_name": new RegExp("^.*?" + req.body.retailer_search + ".*?", "i")}
                ]
            };
        }
    }

    //logger.info("Query");
    //logger.info(qry_todo);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get All Company Name Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)

            var project = {
                "retailer_name": 1, "retailer_code": 1, "-_id": 1
            };
            col_todo.find(qry_todo, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting all company name" + err);
                }
                else {
                    logger.info("Sucessfully getting all company name" + result.length);
                    db.close();
                    try {
                        var ret_list = [];
                        ret_list = _.uniq(results, 'retailer_code');
                        send_to_response(ret_list, res);
                    }
                    catch (ex) {
                        console.log("Error");
                        console.log(ex);
                        send_to_response(ex, res);
                    }
                }
            })
        }

        counter_var--;
        logger.info("End All Company Name sCounter Value" + counter_var);
    });
};

/*****End*****/

exports.get_invoice_by_name = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    counter_var++;
    logger.info("Get Invoice By Name" + counter_var);

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var user = req.params.user;
            var find_qry = {user_id: user};

            var project = {
                "active": 1,
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "status": 1,
                "supporting_doc": 1,
                "claim_details": 1,
                " approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount": 1,
                "name": 1,
                "distributor_code": 1,
                " retailer_name": 1,
                "invoice_amount_excl_vat": 1,
                "payment_received": 1,
                "_id": 1,
                " payment_track": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Successfully getting the part code in pow" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
    counter_var--;
    logger.info("End Get Invoice By Name" + counter_var);
};

exports.get_invoice_details_by_name_inv_no = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Details By Invoice Number Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var find_qry = {user_id: req.params.user, invoice_number: req.params.inv};

            var project = {
                "active": 1, "claim_id": 1, "user_id": 1, "invoice_date": 1, "invoice_amount": 1, "invoice_number": 1,
                "retailer": 1, "status": 1, "supporting_doc": 1, "claim_details": 1,
                "approval_comments": 1, "amount_track": 1, "inv_date_track": 1, "company_name": 1,
                "participant_code": 1, "participant_name": 1, "verified_amount": 1, "name": 1,
                "distributor_code": 1,
                "retailer_name ": 1,
                "invoice_amount_excl_vat": 1,
                "payment_received": 1,
                "-_id": 1,
                "payment_track": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting invoice details" + err);
                }
                else {
                    logger.info("Sucessfully getting invoice details" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End   By Invoice Number Counter Value" + counter_var);
    });
};


exports.get_invoice_by_retailer = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Invoice By Retailer Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var retailer = req.params.retailer;
            var find_qry = {retailer: retailer};
            var project = {
                "active": 1,
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "status": 1,
                "supporting_doc": 1,
                "claim_details": 1,
                "approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount": 1,
                "name": 1,
                "distributor_code": 1,
                "retailer_name": 1,
                "invoice_amount_excl_vat": 1,
                "payment_received": 1,
                "_id": 1,
                "payment_track": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting invoice by retailer" + err);
                }
                else {
                    logger.info("Sucessfully getting invoice by retailer" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Invoice By Retailer Counter Value" + counter_var);
    });
};

exports.calc_user_id_wise_invoice_amount = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    //console.log("fs" + JSON.stringify(req.params.user_id));
    counter_var++;
    logger.info("Calculate User Id Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var pipeline = [
                {
                    $unwind: "$claim_details"
                },
                {
                    $match: {
                        $and: [
                            {status: "Verified & Approved"},
                            {user_id: req.params.user_id},
                            {active: 1}
                        ]
                    }
                },
                {
                    $project: {
                        _id: 0,
                        user_id: 1,
                        retailer_code: 1,
                        claim_details: 1
                    }
                }
            ];

            col_todo.aggregate(pipeline).toArray(function (err, results) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + results.length);
                    console.log('invoice Details ' + JSON.stringify(results));
                    console.log('invoice_list.length ' + results.length);
                    var total_calc_invoice_amount = 0;

                    for (var i = 0; i < results.length; i++) {
                        if (results[i].claim_details.series == "A") {
                            if (results[i].claim_details.pow_status && results[i].claim_details.pow_status == 1) {
                                total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1.5 + (parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1.5) * 0.25;
                            }
                            else {
                                total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1.5;
                            }
                            //total_calc_invoice_amount = total_calc_invoice_amount + parseInt(obj_sku.quantity) * ( obj_sku.unit_price * 1) * 1.5;
                        }

                        if (results[i].claim_details.series == "B") {
                            if (results[i].claim_details.pow_status && results[i].claim_details.pow_status == 1) {
                                total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1 + (parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1) * 0.25;
                            }
                            else {
                                total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1;
                            }

                            //total_calc_invoice_amount = total_calc_invoice_amount + parseInt(obj_sku.quantity) * ( obj_sku.unit_price * 1) * 1;
                        }

                        if (results[i].claim_details.series == "C") {
                            if (results[i].claim_details.pow_status && results[i].claim_details.pow_status == 1) {
                                total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 0.75 + (parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 0.75) * 0.25;
                            }
                            else {
                                total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 0.75;
                            }

                            // total_calc_invoice_amount = total_calc_invoice_amount + parseInt(obj_sku.quantity) * ( obj_sku.unit_price * 1) * .75;
                        }

//                if (results[i].claim_details.series == "A") {
//                    total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1.5;
//                }
//                if (results[i].claim_details.series == "B") {
//                    total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * 1;
//                }
//                if (results[i].claim_details.series == "C") {
//                    total_calc_invoice_amount = total_calc_invoice_amount + parseInt(results[i].claim_details.quantity) * ( results[i].claim_details.unit_price * 1) * .75;
//                }
                    }
                    db.close();

                    //console.log('total_calc_invoice_amount   ' + total_calc_invoice_amount);

                    send_to_response({total_calc_invoice_amount: total_calc_invoice_amount}, res);
                }
            })
        }
    });
    counter_var--;
    logger.info("End Point Calculate Counter Value" + counter_var);
};

exports.search_duplicate_invoice_list = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Search Duplicate Invoice List  Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var invoice_number = req.body.invoice_number;
            var find_qry = {'invoice_number': invoice_number};
            var project = {
                "active": 1,
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "status": 1,
                "supporting_doc": 1,
                "claim_details": 1,
                "approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount": 1,
                "name": 1,
                "distributor_code": 1,
                "retailer_name": 1,
                "invoice_amount_excl_vat": 1,
                "payment_received": 1,
                "_id": 1,
                "payment_track": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in searching duplicate invoice number" + err);
                }
                else {
                    logger.info("Sucessfully searching duplicate invoice number" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End  Search Duplicate Invoice List Counter Value" + counter_var);
    });
};


/*exports.update_invoice_claim = function (req, res) {

 var programs = util.get_programs_details();
 var prog_name = "";
 if (req.body.invoice_date != "Invalid Date") {
 prog_name = get_program_name(programs, req.body.invoice_date);
 }
 console.log("incmoning" +JSON.stringify(req.body));
 console.log("cali_id"+req.body.claim_id);

 counter_var++;
 logger.info("Update Invoice Claim Counter Value" + counter_var);
 /!*
 var MongoClient = mongodb.MongoClient;
 var url = util.get_connection_string("mongo");
 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 //Close connection
 db.close();
 }
 else {
 logger.info("Connection Established To MongoDB");
 var col_todo = db.collection('sales_registry');
 // console.log(sku)



 var query_dis2 = {"claim_id":  req.body.claim_id};

 //Update Statement
 var upd_qry = {
 $set: {
 "claim_details": req.body.claim_details,
 "status": req.body.status,
 "approval_comments": req.body.approval_comments,
 "invoice_amount":req.body.invoice_amount,
 "amount_track": req.body.amount_track,
 "inv_date_track": req.body.inv_date_track,
 "modified_by": req.body.modified_by,
 "modified_date": req.body.modified_date,
 "program_name": prog_name,
 "invoice_date": req.body.invoice_date,
 "invoice_amount_excl_vat": req.body.invoice_amount_excl_vat
 }
 };


 col_todo.update(query_dis2, upd_qry,function (err, result) {
 if (err) {
 logger.info("Error in Updating default_roster Data,err:" + err.message);
 //Close connection
 db.close();

 } else {
 console.log("sd")
 }
 })
 /!*        col_todo.update(q,upd_qry,function (err, result) {
 if (err) {
 logger.info("Error in checking part code in product of the week" + err);
 }
 else {
 logger.info("Sucessfully getting the part code in pow" + result.length);
 if (result.length > 0) {
 util.send_to_response(result, res)
 }
 }
 })*!/
 }


 })*!/

 sales_registry.update_invoice_claim(req.body, prog_name)
 .then(

 function (results) {
 console.log("results"+JSON.stringify(results));
 mongoose.disconnect();
 /!*res.send("claims updated successfully");*!/
 util.send_to_response('claims updated successfully', res)
 //send_to_response(results, res);
 },
 function (err) {
 mongoose.disconnect();
 res.send(err);
 }
 );

 counter_var--;
 logger.info("End Update Invoice Claim Counter Value" + counter_var);
 };*/

exports.update_invoice_claim = function (req, res) {

    var programs = util.get_programs_details();
    var prog_name = "";
    if (req.body.invoice_date != "Invalid Date") {
        prog_name = get_program_name(programs, req.body.invoice_date);
    }
    // console.log("incom"+JSON.stringify(req.body.invoice_date))
    var invoice_date=new Date(req.body.invoice_date);

    console.log("invoice_date"+JSON.stringify(invoice_date));

        // console.log("claim_details" + JSON.stringify(result[j].claim_details) + "of userid" + result[j].user_id);

        // console.log("result[j].claim_details" + JSON.stringify(result[j].claim_details))
        var total_calc_invoice_amount = 0;
        var sum_calc_invoice_amount = 0;
        var retailer_points_object ={};
        for (var k = 0; k < req.body.claim_details.length; k++) {
            var total_calc_invoice_amount = 0;
            if (req.body.claim_details[k].series == "A") {
                if (req.body.claim_details[k].pow_status &&req.body.claim_details[k].pow_status == 1) {
                    total_calc_invoice_amount += (req.body.claim_details[k].quantity) * ( req.body.claim_details[k].unit_price * 1) * 1.5 * 1.25;
                    total_calc_invoice_amount=total_calc_invoice_amount/1000;

                    console.log("A div pow  total_calc_invoice_amount"+JSON.stringify(total_calc_invoice_amount))
                }

                else {
                    total_calc_invoice_amount += (req.body.claim_details[k].quantity) * ( req.body.claim_details[k].unit_price * 1) * 1.5;
                    total_calc_invoice_amount=total_calc_invoice_amount/1000;
                    console.log("A div total_calc_invoice_amount"+JSON.stringify(total_calc_invoice_amount))
                }
                //total_calc_invoice_amount = total_calc_invoice_amount + (obj_sku.quantity) * ( obj_sku.unit_price * 1) * 1.5;


            }

            if (req.body.claim_details[k].series == "B") {
                if (req.body.claim_details[k].pow_status &&req.body.claim_details[k].pow_status == 1) {
                    total_calc_invoice_amount += (req.body.claim_details[k].quantity) * (req.body.claim_details[k].unit_price * 1) * 1 * 1.25;

                    total_calc_invoice_amount=total_calc_invoice_amount/1000;
                    console.log("B div pow total_calc_invoice_amount"+JSON.stringify(total_calc_invoice_amount))
                }

                else {
                    total_calc_invoice_amount += (req.body.claim_details[k].quantity) * ( req.body.claim_details[k].unit_price * 1) * 1;

                    total_calc_invoice_amount=total_calc_invoice_amount/1000;
                    console.log("B div total_calc_invoice_amount"+JSON.stringify(total_calc_invoice_amount))
                }


                //total_calc_invoice_amount = total_calc_invoice_amount + (obj_sku.quantity) * ( obj_sku.unit_price * 1) * 1;
            }

            if (req.body.claim_details[k].series == "C") {
                if (req.body.claim_details[k].pow_status && req.body.claim_details[k].pow_status == 1) {

                    total_calc_invoice_amount += (req.body.claim_details[k].quantity) * (req.body.claim_details[k].unit_price * 1) * 0.75 * 1.25;
                    total_calc_invoice_amount=total_calc_invoice_amount/1000;
                    console.log("C pow  div total_calc_invoice_amount"+JSON.stringify(total_calc_invoice_amount))
                }

                else {
                    total_calc_invoice_amount += (req.body.claim_details[k].quantity) * ( req.body.claim_details[k].unit_price * 1)* 0.75

                    total_calc_invoice_amount=(total_calc_invoice_amount/1000);
                    console.log("C divid total_calc_invoice_amount"+JSON.stringify(total_calc_invoice_amount));
                }


                // total_calc_invoice_amount = total_calc_invoice_amount + (obj_sku.quantity) * ( obj_sku.unit_price * 1) * .75;


            }
            sum_calc_invoice_amount+=total_calc_invoice_amount;
            console.log("sum_calc_invoice_amount"+JSON.stringify(sum_calc_invoice_amount));
        }
// console.log("id to be updates" +JSON.stringify(record_id))
        var retailer_points = sum_calc_invoice_amount;

        retailer_points_object.retialer_points = retailer_points;
        retailer_points_object.disti_points = retailer_points * 0.02;
        retailer_points_object.disti_champ_points = retailer_points * 0.03;
        retailer_points_object.created_dt = new Date();
    console.log("retailer_points_object" +JSON.stringify(retailer_points_object))
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var number = req.params.claim_id;
            var match = {claim_id: req.body.claim_id};


            var retailer_data = {
                $set: {
                    "claim_details": req.body.claim_details,
                    "status": req.body.status,
                    "approval_comments": req.body.approval_comments,
                    "invoice_amount": req.body.invoice_amount,
                    "amount_track": req.body.amount_track,
                    "inv_date_track": req.body.inv_date_track,
                    "modified_by": req.body.modified_by,
                    "modified_date": req.body.modified_date,
                    "program_name": "nrml",
                    //"invoice_date": req.body.invoice_date,
                    "invoice_amount_excl_vat": req.body.invoice_amount_excl_vat,
                    "retailer_points_details":retailer_points_object

                }
            };

            col_todo.update(match, retailer_data, function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + result.length);
                    db.close();

                    util.send_to_response("claims updated successfully", res)


                }
            })
        }

        counter_var--;
        logger.info("Get Details By Invoice Number Counter Value" + counter_var);
    });

  /*  sales_registry.update_invoice_claim(req.body, prog_name,retailer_points_object)
        .then(
            function (results) {
                // res.send("claims updated successfully");
                console.log("claims updated successfully");
                mongoose.connection.close(function () {
                    console.log('    Mongoose connection disconnected update_invoice_claim ');
                });
                util.send_to_response("claims updated successfully", res)
                //send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected update_invoice_claim ');
                });
                res.send(err);
            }
        );*/
};
exports.update_claim = function (req, res) {

    counter_var++;
    logger.info("Update Claim Counter Value" + counter_var);
    sales_registry.update_claim(req.body.data)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected update_claim ');
                });
                send_to_response(results, res);
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected update_claim ');
                });
                res.send(err);
            }
        );

    counter_var--;
    logger.info("End Update Claim Counter Value" + counter_var);
};

exports.get_invoice_by_number = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Invoice Number Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var number = req.params.claim_id;
            var find_qry = {claim_id: number};

            var project = {
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "retailer_name": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "claim_details": 1,
                "status": 1,
                "approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount name": 1,
                "distributor_code": 1,
                "invoice_amount_excl_vat": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("Get Details By Invoice Number Counter Value" + counter_var);
    });
};

exports.get_invoice_by_user_id = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get  Invoice By User _id  Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            // logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var inv_no = req.body.inv_no;
            var date = req.body.inv_date;
            var inv_amount = req.body.inv_amount;

            var find_qry = {"invoice_number": inv_no, "invoice_date": date, "invoice_amount": inv_amount}

            var project = {
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "retailer_name": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "claim_details": 1,
                "status": 1,
                "approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount": 1,
                "name": 1,
                "distributor_code": 1,
                "invoice_amount_excl_vat": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting invoice by user_id" + err);
                }
                else {
                    logger.info("Sucessfully getting invoice by user_id" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var++;
        logger.info("Get Invoice By User Id  Counter Value" + counter_var);
    });
};

exports.get_invoice_details_by_user_id = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Invoice Details By User Id Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var user_id = req.params.user_id;
            var find_qry = {"user_id": user_id};

            var project = {
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "retailer_name": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "claim_details": 1,
                "status": 1,
                "approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount": 1,
                "name": 1,
                "distributor_code": 1,
                "invoice_amount_excl_vat": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, invoice_result) {
                if (err) {
                    logger.info("Error in invoice details based on user id " + err);
                }
                else {
                    logger.info("Sucessfully invoice details based on user id" + invoice_result.length);
                    // db.close();
                    if (invoice_result.length > 0) {
                        util.send_to_response(invoice_result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Invoice Details By User Id Counter Value" + counter_var);
    });
};

exports.get_early_bird_winners = function (req, res) {

    //sales_registry.get_early_bird_winners (req.params.program_type)
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Early Bird Winner Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            {program_name: 'nrml'},
                            {invoice_date: {$gte: new Date('04/01/2015')}},
                            {invoice_date: {$lte: new Date('12/31/2015')}}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {'user_id': '$user_id', 'company': '$company_name'},
                        'total_sale': {$sum: '$invoice_amount'}
                    }
                },
                {
                    $project: {
                        _id: 0, "company": "$_id.company",
                        "user_id": "$_id.user_id", "total_sale": 1
                    }
                }
            ];
            col_todo.aggregate(pipeline).toArray(function (err, results) {
                if (err) {
                    logger.info("Error in getting early bird winner" + err);
                }
                else {
                    //logger.info("Sucessfully getting early bird winner" + results.length);
                    if (results.length > 0) {

                        for (var i = results.length; i--;) {
                            //                    console.log(results[i].total_sale);
                            if (results[i].total_sale < 100000) {
                                results.splice(i, 1);
                            }
                        }
                    }
                    db.close();
                    send_to_response(results, res);
                }
            })
        }

        counter_var--;
        logger.info("End Get Early Bird Winner Counter Value" + counter_var);
    });
};

exports.new_get_all_points_eligible_users = function (req, res) {

    sales_registry.get_all_users()
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected new_get_all_points_eligible_users ');
                });
                var sqldata = [];
                //console.log('All Users Here 2');
                //console.log(results.length);
                if (results.length > 0) {

                    //                console.log ("Execute Time : " + Execute_Time + " Getting Verified & Approved Invoices User Info For Schneider.....");
                    //                console.log ("Execute Time : " + Execute_Time + " Total User : " + results.length);

                    //                for (var i = 0; i < results.length; i++)
                    //                {
                    //                    results[i].points_credited = 0;
                    //                    results[i].total_sale_nrml = 0;
                    //                    results[i].total_sale_spcl = 0;
                    //                    results[i].sum_of_total_sale_nrml_spcl = 0;
                    //                    results[i].bonus_points_on_indpndsday = 0;
                    //                    results[i].normal_points_on_total_sale = 0;
                    //                    results[i].total_calc_points = 0;
                    //                    results[i].current_points_balance = 0;
                    //                    results[i].eligible_slab = 0;
                    //                    results[i].req_point_next_slab = 0;
                    //                }

                    // console.log("Execute Time : " + Execute_Time + " Getting Verified & Approved Invoices User Info For Schneider.....");
                    // console.log("Execute Time : " + Execute_Time + " Total User : " + results.length);
                    // console.log(results);

                    var exc_results = [
                        {test: 'hello'}
                    ];

                    var extra_default_data = {
                        "points_credited": 0,
                        "total_sale_nrml": 0,
                        "total_sale_spcl": 0,
                        "sum_of_total_sale_nrml_spcl": 0,
                        "bonus_points_on_indpndsday": 0,
                        "normal_points_on_total_sale": 0,
                        "total_calc_points": 0,
                        "current_points_balance": 0,
                        "eligible_slab": 0,
                        "req_point_next_slab": 0,
                        "PointsAlloted": 0
                    };

                    //Get User Wise Credited Points
                    async.map(exc_results, new_get_credited_points, function (err, r) {

                        var credited_point_result = r[0][0];
                        //                  console.log('Get User Wise Credited Points');
                        //                  console.log(credited_point_result);

                        try {
                            _.each(results, function (element, index, list) {
                                //Get Exact User Points
                                var user_sale_data = _.where(credited_point_result, {email_id: element._id});
                                if (user_sale_data.length > 0) {
                                    _.extend(element, user_sale_data[0]);
                                } else {
                                    _.extend(element, extra_default_data);
                                }
                            });
                        } catch (err) {
                            console.log(err);
                        }

                        //                  console.log ("Added Points Data");
                        //                  console.log (results);

                        // Get All User Wise Sales Information

                        async.map(results, get_user_wise_sales_points, function (err, r) {

                            for (var i = 0; i < r.length; i++) {
                                if (results[i]._id == r[i][0]._id) {
                                    try {

                                        var total_points_based_on_sale = 0;
                                        var total_calc_points_based_on_sale = 0;

                                        total_points_based_on_sale = (r[i][0].normal_points_on_total_sale * 1);

                                        results[i].total_sale_nrml = (r[i][0].total_sale_nrml * 1);
                                        results[i].total_sale_spcl = (r[i][0].total_sale_spcl * 1);
                                        results[i].sum_of_total_sale_nrml_spcl = (r[i][0].sum_of_total_sale_nrml_spcl * 1);
                                        results[i].bonus_points_on_indpndsday = (r[i][0].bonus_points_on_indpndsday * 1);
                                        results[i].normal_points_on_total_sale = (r[i][0].normal_points_on_total_sale * 1);


                                        //                                    console.log('total_points_based_on_sale');
                                        //                                    console.log(total_points_based_on_sale);

                                        //                                  results[i].total_calc_points=((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale* 1));

                                        if (total_points_based_on_sale > 250) {

                                            total_calc_points_based_on_sale = parseInt(total_points_based_on_sale / 250) * 250;

                                        } else {
                                            total_calc_points_based_on_sale = 0;
                                        }

                                        //                                    results[i].total_calc_points=parseInt(total_calc_points_based_on_sale);
                                        results[i].total_calc_points = parseInt(results[i].sum_of_total_sale_nrml_spcl / 1000);
                                        results[i].points_credited = parseInt(results[i].PointsAlloted);
                                        results[i].current_points_balance = (parseInt(results[i].total_calc_points) - parseInt(results[i].PointsAlloted));
                                    } catch (er) {
                                        console.log(er);
                                    }
                                }
                            }

                            // console.log('Result Set For All Users ');
                            //  console.log(results);

                            //If Current Balance Is Less Than Equal To 0 Then Delete This User.
                            //                        for (var i = results.length; i--;) {
                            //                            if ((results[i].current_points_balance * 1) <= 0) {
                            //                                results.splice(i, 1);
                            //                            }
                            //                        }

                            for (var i = 0; i < results.length; i++) {
                                try {
                                    var p = [];
                                    if (util.calculate_points(results[i].total_calc_points, "mixed").length == 0) {
                                        p[0] = 0
                                    } else {
                                        p = util.calculate_points(results[i].total_calc_points, "mixed");
                                    }
                                    results[i].eligible_slab = p[0];
                                    results[i].req_point_next_slab = ((results[i].eligible_slab * 1) - (results[i].PointsAlloted * 1));
                                } catch (er) {
                                    console.log(er);
                                }
                            }

                            ////                  If He Is Not Eligible For Any Slab Then Delete This User.
                            //                    for (var i = results.length; i--;) {
                            //                        if ((results[i].eligible_slab * 1) <= 0) {
                            //                            results.splice(i, 1);
                            //                        }
                            //                    }
                            //
                            ////                  If Required Point For Next Slab Is Zero Then Delete This User.
                            //                    for (var i = results.length; i--;) {
                            //                        if ((results[i].req_point_next_slab * 1) <= 0) {
                            //                            results.splice(i, 1);
                            //                        }
                            //                    }

                            // console.log("Execute Time : " + Execute_Time + ' Result Set For Eligible Users For Schneider.');
                            // console.log(results);

                            // Calling Mysql For User Details

                            var conn1 = mysql.get_mysql_connection();

                            var sql = "call get_user_details_for_reporting()";
                            // console.log(sql);
                            conn1.query(sql,
                                function (err, rows) {
                                    if (err) {
                                        logger.info(err);
                                        res.end;
                                    }
                                    logger.info("user login validated from mysql");
                                    conn1.end();
                                    conn1 = null;
                                    sqldata = rows[0];
                                    // console.log(sqldata);

                                    if (results.length > 0) {
                                        // console.log('All users');
                                        //  console.log(results);

                                        _.each(results, function (element, index, list) {
                                            var match_data = _.where(sqldata, {email_id: element.email_id});
                                            // console.log('Match Data');
                                            // console.log(match_data);

                                            if (match_data.length > 0) {
                                                // console.log('inside async1 details');
                                                element.CreditPoints = match_data[0].CreditPoints;
                                                element.REDEEM_POINTS = match_data[0].REDEEM_POINTS;
                                                element.city = match_data[0].city;
                                                element.cart_data = match_data[0].cart_data;

                                                // element = pos_upld_data[0];
                                            }
                                            //                            console.log('Inside Each');
                                            //                            console.log(element);

                                        });
                                    }

                                    //console.log('Final Release');
                                    //console.log(results);

                                    res.send(results);
                                });
                            //                        res.send(results);
                        });
                    });
                } else {
                    res.send(results);
                }
            },
            function (err) {
                res.send(err);
            });
};


// this function gets amount of point credited to the people
// who has earned points in this run
var new_get_credited_points = function (r, done_callback) {

    new_get_user_wise_credited_point(r._id, function (results) {

        if (results.length > 0) {
            return done_callback(null, results);
        } else {
            var first_val = {_id: null, "point_received": 0};
            results.push(first_val);
            return done_callback(null, results);
        }
    });

};

function new_get_user_wise_credited_point(user_id, callback) {

    //    var sql = "select user_id as _id , sum(txn_amount) as point_received from user_points where " +
    //        "user_id='"+user_id+"' and program_type='Mixed' and txn_type='c'; ";
    //
    var conn = mysql.get_mysql_connection();
    var sql = "call get_users_points_credited ('schneider')";
    //  console.log(sql);

    conn.query(sql, function (error, results) {
        if (error) {
            console.log(error);
        }
        if (results.length > 0) {
            callback(results);
        }
    });
}

exports.get_all_points_eligible_users = function (req, res) {

    sales_registry.get_all_users()
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_all_users ');
                });
                //  console.log('All Users Here 1 ');
                // console.log(results.length);
                if (results.length > 0) {

                    //                console.log ("Execute Time : " + Execute_Time + " Getting Verified & Approved Invoices User Info For Schneider.....");
                    //                console.log ("Execute Time : " + Execute_Time + " Total User : " + results.length);

                    for (var i = 0; i < results.length; i++) {
                        results[i].points_credited = 0;
                        results[i].total_sale_nrml = 0;
                        results[i].total_sale_spcl = 0;
                        results[i].sum_of_total_sale_nrml_spcl = 0;
                        results[i].bonus_points_on_indpndsday = 0;
                        results[i].normal_points_on_total_sale = 0;
                        results[i].total_calc_points = 0;
                        results[i].current_points_balance = 0;
                        results[i].eligible_slab = 0;
                        results[i].req_point_next_slab = 0;
                    }

                    //Get Already Credited Points For These Users
                    async.map(results, get_credited_points, function (err, r) {

                        for (var i = 0; i < r.length; i++) {
                            if (r[i][0].point_received == null) {
                                r[i][0].point_received = 0;
                            }
                            if (results[i]._id == r[i][0]._id) {
                                results[i].points_credited = r[i][0].point_received;
                            }
                        }

                        //Get All User Wise Sales Information
                        async.map(results, get_all_eligible_user_wise_sales_data, function (err, r) {

                            for (var i = 0; i < r.length; i++) {

                                if (results[i]._id == r[i][0]._id) {
                                    results[i].total_sale_nrml = (r[i][0].total_sale_nrml * 1);
                                    results[i].total_sale_spcl = (r[i][0].total_sale_spcl * 1);
                                    results[i].sum_of_total_sale_nrml_spcl = (r[i][0].sum_of_total_sale_nrml_spcl * 1);
                                    results[i].bonus_points_on_indpndsday = (r[i][0].bonus_points_on_indpndsday * 1);
                                    results[i].normal_points_on_total_sale = (r[i][0].normal_points_on_total_sale * 1);
                                    results[i].total_calc_points = ((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale * 1));
                                    results[i].current_points_balance = (((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale * 1)) - (results[i].points_credited * 1));
                                }

                            }

                            //                          console.log('Result Set For All Users ');
                            //                          console.log(results);


                            //If Current Balance Is Less Than Equal To 0 Then Delete This User.
                            //                        for (var i = results.length; i--;) {
                            //                            if ((results[i].current_points_balance * 1) <= 0) {
                            //                                results.splice(i, 1);
                            //                            }
                            //                        }

                            for (var i = 0; i < results.length; i++) {

                                var p = [];
                                if (util.calculate_points(results[i].total_calc_points, "mixed").length == 0) {
                                    p[0] = 0
                                } else {
                                    p = util.calculate_points(results[i].total_calc_points, "mixed");
                                }

                                results[i].eligible_slab = p[0];
                                results[i].req_point_next_slab = ((results[i].eligible_slab * 1) - (results[i].points_credited * 1));

                            }

                            //                    //If He Is Not Eligible For Any Slab Then Delete This User.
                            //                        for (var i = results.length; i--;) {
                            //                            if ((results[i].eligible_slab * 1) <= 0) {
                            //                                results.splice(i, 1);
                            //                            }
                            //                        }
                            //
                            //                    //If Required Point For Next Slab Is Zero Then Delete This User.
                            //
                            //                        for (var i = results.length; i--;) {
                            //                            if ((results[i].req_point_next_slab * 1) <= 0) {
                            //                                results.splice(i, 1);
                            //                            }
                            //                        }

                            //                        console.log("Execute Time : " + Execute_Time + ' Result Set For Eligible Users For Schneider.');
                            //                        console.log(results);
                            //  console.log('Result Set For All Users ');
                            //  console.log(results.length);
                            // console.log(results);

                            res.send(results);
                        });
                    });
                }
            },
            function (err) {
                res.send(err);
            });
};

var get_credited_points = function (r, done_callback) {

    get_user_wise_credited_point(r._id, function (results) {

        if (results.length > 0) {
            return done_callback(null, results);
        } else {
            var first_val = {_id: null, "point_received": 0};
            results.push(first_val);
            return done_callback(null, results);
        }
    });
};

function get_user_wise_credited_point(user_id, callback) {

    counter_var++;
    logger.info("Get User Wise Credited Points  Counter Value" + counter_var);
    var conn = mysql.get_mysql_connection();
    var sql = "select user_id as _id , sum(txn_amount) as point_received from user_points where " +
        "user_id='" + user_id + "' and program_type='Mixed' and txn_type='c'; ";

    conn.query(sql, function (error, results) {
        if (error) {
            console.log(error);
        }
        if (results.length > 0) {
            callback(results);
        }
        conn.end();
        conn = null;
    });

    counter_var--;
    logger.info("End User Wise Credited Points Counter Value" + counter_var);
}

var get_all_eligible_user_wise_sales_data = function (r, done_callback) {

    sales_registry.get_user_wise_total_claim(r._id)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_user_wise_total_claim ');
                });
                var out_results = [];
                var first_val;

                if (results.length > 0) {

                    var total_sale_nrml = 0;
                    var total_sale_spcl = 0;
                    var sum_of_total_sale_nrml_spcl = 0;
                    var total_point_on_nrml_spcl = 0;
                    var sum_of_total_points_earned = 0;
                    var bonus_points_on_indpndsday = 0;
                    var normal_points_on_total_sale = 0;

                    for (var i = 0; i < results.length; i++) {

                        if (results[i].program_name == "nrml") {

                            total_sale_nrml = (results[i].total_sale * 1);

                        } else if (results[i].program_name == "spl1") {
                            var p = [];

                            total_sale_spcl = (results[i].total_sale * 1);

                            if (util.calculate_points(results[i].total_sale, results[i].program_name).length == 0) {
                                p[0] = 0
                            } else {
                                p[0] = util.calculate_points(results[i].total_sale, results[i].program_name);
                            }

                            bonus_points_on_indpndsday = p[0];
                        }
                    }

                    normal_points_on_total_sale = Math.round((total_sale_nrml + total_sale_spcl) / 1000);

                    first_val = {
                        _id: results[0].user_id,
                        "total_sale_nrml": total_sale_nrml,
                        "total_sale_spcl": total_sale_spcl,
                        "sum_of_total_sale_nrml_spcl": (total_sale_nrml + total_sale_spcl),
                        "bonus_points_on_indpndsday": bonus_points_on_indpndsday,
                        "normal_points_on_total_sale": normal_points_on_total_sale
                    };
                    out_results.push(first_val);
                    return done_callback(null, out_results);

                } else {
                    first_val = {
                        _id: null,
                        "total_sale_nrml": 0,
                        "total_sale_spcl": 0,
                        "sum_of_total_sale_nrml_spcl": 0,
                        "bonus_points_on_indpndsday": 0,
                        "normal_points_on_total_sale": 0
                    };
                    out_results.push(first_val);
                    return done_callback(null, out_results);
                }
            },
            function (err) {
                //            res.send(err);
                return done_callback(null, err);
            }
        );
};

exports.get_points_credited_users = function (req, res) {

    var conn = mysql.get_mysql_connection();
    var sql = "call get_users_points_credited ('schneider')";

    counter_var++;
    logger.info("Get Points Credited User Counter Value" + counter_var);
    // console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;

            if (rows.length > 0) {
                var results = rows[0];
                for (var i = 0; i < results.length; i++) {
                    results[i].points_credited = (results[i].PointsAlloted * 1);
                    results[i].total_sale_nrml = 0;
                    results[i].total_sale_spcl = 0;
                    results[i].sum_of_total_sale_nrml_spcl = 0;
                    results[i].bonus_points_on_indpndsday = 0;
                    results[i].normal_points_on_total_sale = 0;
                    results[i].total_calc_points = 0;
                    results[i].current_points_balance = 0;
                    results[i].cr_dr_points_balance = 0;
                    results[i].eligible_slab = 0;
                    results[i].req_point_next_slab = 0;
                    results[i].away_next_slab = 0;
                }

                //Get All User Wise Sales Information
                async.map(results, get_user_wise_sales_points, function (err, r) {

                    for (var i = 0; i < r.length; i++) {

                        if (results[i].email_id == r[i][0]._id) {
                            results[i].total_sale_nrml = (r[i][0].total_sale_nrml * 1);
                            results[i].total_sale_spcl = (r[i][0].total_sale_spcl * 1);
                            results[i].sum_of_total_sale_nrml_spcl = (r[i][0].sum_of_total_sale_nrml_spcl * 1);
                            results[i].bonus_points_on_indpndsday = (r[i][0].bonus_points_on_indpndsday * 1);
                            results[i].normal_points_on_total_sale = (r[i][0].normal_points_on_total_sale * 1);
                            results[i].total_calc_points = ((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale * 1));
                            results[i].current_points_balance = (((r[i][0].bonus_points_on_indpndsday * 1) + (r[i][0].normal_points_on_total_sale * 1)) - (results[i].points_credited * 1));
                            results[i].cr_dr_points_balance = ((results[i].current_points_balance * 1) + (results[i].PointsBalance * 1));
                        }
                    }

                    //  console.log('Result Set For All Users ');
                    //  console.log(results);

                    for (var i = 0; i < results.length; i++) {

                        var p = [];
                        if (util.calculate_points(results[i].total_calc_points, "mixed").length == 0) {
                            p[0] = 0
                        } else {
                            p = util.calculate_points(results[i].total_calc_points, "mixed");
                        }
                        results[i].eligible_slab = p[0];
                        results[i].req_point_next_slab = ((results[i].eligible_slab * 1) - (results[i].points_credited * 1));

                        if (((results[i].cr_dr_points_balance * 1) < 4000) && ((results[i].cr_dr_points_balance * 1) >= 0)) {

                            results[i].away_next_slab = 500 - (results[i].cr_dr_points_balance % 500);

                        } else {

                            results[i].away_next_slab = 0;
                        }
                    }

                    res.send(results);
                });
            }
        });


    counter_var--;
    logger.info("Get Points Credited User Counter Value" + counter_var);
};

var get_user_wise_sales_points = function (r, done_callback) {

    counter_var++;
    logger.info("Get User Wise Sales Points Counter Value" + counter_var);
    sales_registry.get_user_wise_total_claim(r.email_id)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' Mongoose connection disconnected get_user_wise_total_claim ');
                });
                var out_results = [];
                var first_val;

                try {
                    if (results.length > 0) {
                        var total_sale_nrml = 0;
                        var total_sale_spcl = 0;
                        var sum_of_total_sale_nrml_spcl = 0;
                        var total_point_on_nrml_spcl = 0;
                        var sum_of_total_points_earned = 0;
                        var bonus_points_on_indpndsday = 0;
                        var normal_points_on_total_sale = 0;

                        for (var i = 0; i < results.length; i++) {

                            if (results[i].program_name == "nrml") {

                                total_sale_nrml = (results[i].total_sale * 1);

                            } else {
                                total_sale_spcl = (results[i].total_sale * 1);
                                var bonus = [];
                                if (util.calculate_points(results[i].total_sale, results[i].program_name).length == 0) {
                                    bonus[0] = 0
                                } else {
                                    bonus[0] = util.calculate_points(results[i].total_sale, results[i].program_name);
                                }
                                bonus_points_on_indpndsday = bonus[0];
                            }
                        }

                        normal_points_on_total_sale = Math.round((total_sale_nrml + total_sale_spcl) / 1000);

                        first_val = {
                            _id: results[0].user_id,
                            "total_sale_nrml": total_sale_nrml,
                            "total_sale_spcl": total_sale_spcl,
                            "sum_of_total_sale_nrml_spcl": (total_sale_nrml + total_sale_spcl),
                            "bonus_points_on_indpndsday": bonus_points_on_indpndsday,
                            "normal_points_on_total_sale": normal_points_on_total_sale
                        };
                        out_results.push(first_val);

                        return done_callback(null, out_results);

                    } else {

                        first_val = {
                            _id: null,
                            "total_sale_nrml": 0,
                            "total_sale_spcl": 0,
                            "sum_of_total_sale_nrml_spcl": 0,
                            "bonus_points_on_indpndsday": 0,
                            "normal_points_on_total_sale": 0
                        };
                        out_results.push(first_val);
                        return done_callback(null, out_results);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error Mongoose connection disconnected get_user_wise_total_claim ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );


    counter_var--;
    logger.info("End User Wise Sales Pints Counter Value" + counter_var);
};

var get_user_data = function (r, done_callback) {


    counter_var++;
    logger.info("Get User Data  Counter Value" + counter_var);
    pos.get_retailer_data(r.retailer)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log(' error Mongoose connection disconnected get_retailer_data ');
                });
                var out_results = [];

                try {
                    return done_callback(null, results);
                } catch (ex) {
                    console.log(ex);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error Mongoose connection disconnected get_retailer_data ');
                });
                //            res.send(err);
                return done_callback(null, err);
            }
        );

    counter_var--;
    logger.info("End  User Data  Counter Value" + counter_var);
};

exports.get_my_order_details = function (req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_user_wise_order ('" + req.params.user_id + "','" + 0 + "')";

    counter_var++;
    logger.info("Get My Order Details Counter Value" + counter_var);
    // console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });


    counter_var--;
    logger.info("Get My Order Details  Counter Value" + counter_var);
};

//*****************Start Distributor APIs for SEEP******************//

//get all distributor
exports.get_distributor_list = function (req, res) {
    counter_var++;
    logger.info("Get Distributor List" + counter_var);
    var conn = mysql.get_mysql_connection();
    // var sql = "call get_distributor ('" + req.params.p_id + "')";

    var sql = "call get_distributor_list('schneider',40);";

    //console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });

    counter_var--;
    logger.info("End Get Distributor List" + counter_var);
};

//*****************END Distributor APIs for SEEP******************//

exports.get_all_distributor = function (req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_distributor ('" + req.params.p_id + "')";


    counter_var++;
    logger.info("Get All Distributor  Counter Value" + counter_var);
    //  console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });


    counter_var--;
    logger.info("End  All Distributor  Counter Value" + counter_var);
};

exports.get_shipping_detail_by_order_id = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call GetShippingDetail (" + req.params.order_id + ")";


    counter_var++;
    logger.info("Get Shipping Details By Order Id Counter Value" + counter_var);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });


    counter_var--;
    logger.info("Get Shipping Details By Order Id Counter Value" + counter_var);
};

exports.get_all_schneider_users = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call get_schneider_user_data_pagewise ('" + req.params.company + "',0,'" + req.params.page_no + "',200,0,0)";

    counter_var++;
    logger.info("Get All Schneider User Counter Value" + counter_var);
    // console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });


    counter_var--;
    logger.info("End All Schnider User Counter Value" + counter_var);
};

exports.get_order_information = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call get_order_information (" + req.params.order_id + ")";


    counter_var++;
    logger.info("Get Oder Information  Counter Value" + counter_var);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //  logger.info("user login validated from mysql");
            conn.end();
            conn = null;
            res.send(rows);
        });


    counter_var--;
    logger.info("End Order Information Counter Value" + counter_var);
};

//Schneider Report

exports.get_top_si = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Top Si Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'}]
                    }
                },
                {
                    $group: {
                        _id: "$retailer",
                        total_sale: {$sum: "$invoice_amount"}
                    }
                },
                {$project: {_id: 0, retailer: '$_id', total_sale: '$total_sale'}},
                {$sort: {total_sale: -1}},
                {$limit: 15}
            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting top si" + err);
                }
                else {
                    logger.info("Sucessfully getting top si" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
    counter_var--;
    logger.info("End Get Top Si Counter Value" + counter_var);
};

exports.get_top_si_by_region = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Top Si By Region Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'}]
                    }
                },
                {
                    $group: {
                        _id: {'region': '$region', 'SI': "$company_name"},
                        total_sale: {$sum: "$invoice_amount"},
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0, region: '$_id.region', 'SI': '$_id.SI',
                        total_sale: '$total_sale', total_count: '$total_count'
                    }
                },
                {$sort: {total_sale: -1}}
            ];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting top si by region" + err);
                }
                else {
                    logger.info("Sucessfully getting top si by region" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("Get Top Si By Region Counter Value" + counter_var);
    });
};

exports.get_si_by_region = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("GetSi By Region Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            {region: {$in: ['East', 'West', 'North', 'South']}}]
                    }
                },
                {
                    $group: {
                        _id: {'region': '$region'},
                        total_sale: {$sum: "$invoice_amount"},
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0, region: '$_id.region',
                        total_sale: '$total_sale', total_count: '$total_count'
                    }
                },
                {$sort: {total_sale: -1}}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting si by region" + err);
                }
                else {
                    logger.info("Sucessfully getting si by region" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Si Region Counter Value" + counter_var);
    });
};

exports.special_program_qualifier = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Special Program Qualifier Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            {program_name: 'spl1'}
                        ]
                    }
                },
                {
                    $group: {
                        _id: '$user_id',
                        total_sale: {$sum: "$invoice_amount"}
                    }
                },
                {
                    $match: {total_sale: {$gte: 100000}}
                },
                {$sort: {total_sale: -1}}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting special program qualifier" + err);
                }
                else {
                    logger.info("Sucessfully getting special program qualifier" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Special Program Qualifier Counter Value" + counter_var);
    });
};

exports.normal_program_qualifier = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Normal Program Qualifier Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            {program_name: 'nrml'}
                        ]
                    }
                },
                {
                    $group: {
                        _id: '$user_id',
                        total_sale: {$sum: "$invoice_amount"}
                    }
                },
                {
                    $match: {total_sale: {$gte: 100000}}
                },
                {$sort: {total_sale: -1}}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting normal prgram qualifier" + err);
                }
                else {
                    logger.info("Sucessfully getting normal prgram qualifier" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("Get Normal Program Qualifier Counter Value" + counter_var);
    });
};

exports.get_total_sale_verified = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get total Sale Verifed Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var user_id = req.params.user_id;

            var pipeline = [{
                $match: {
                    $and: [
                        {active: 1}, {"user_id": user_id},
                        {'status': 'Verified & Approved'},
                        {"payment_received": "Y"},
                        {"within_credit_period": "Y"}
                    ]
                }
            },
                {$group: {_id: "user_id", total: {$sum: "$invoice_amount_excl_vat"}}},
                {$project: {_id: 0, total_sale: "$total"}}];
            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting total sale verified" + err);
                }
                else {
                    logger.info("Sucessfully getting total sale verified" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("Get Details By Invoice Number Counter Value" + counter_var);
    });
};

exports.get_points_pending_for_paymt_appv = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Points Pending For Payment Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var user_id = req.params.user_id;

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1}, {"user_id": user_id},
                            {'status': 'Verified & Approved'},
                            {"payment_received": "N"}
                        ]
                    }
                },
                {
                    $group: {_id: "user_id", total: {$sum: "$invoice_amount_excl_vat"}}
                },
                {$project: {_id: 0, total_sale: "$total"}}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting points payment for approval" + err);
                }
                else {
                    //logger.info("Sucessfully getting points payment for approval" + result.length);
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
    counter_var--;
    logger.info(" End Get Points Pending For Payment Counter Value" + counter_var);
};

exports.get_lostpoint = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Lost Point Api Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var user_id = req.params.user_id;

            var pipeline = ([{
                $match: {
                    $and: [
                        {active: 1}, {user_id: user_id},
                        {'status': 'Verified & Approved'},
                        {'within_credit_period': 'N'}
                    ]
                }
            },
                {$group: {_id: "user_id", total: {$sum: "$invoice_amount_excl_vat"}}},
                {$project: {_id: 0, total_lost_sale: "$total"}}]);

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting lost points" + err);
                }
                else {
                    logger.info("Sucessfully getting lost points" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
    counter_var--;
    logger.info("End  Get Lost Point Api Counter Value" + counter_var)
};

exports.get_claim_by_user = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Claim By User Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var user_id = req.params.user;

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1}, {user_id: user_id},
                            {'status': 'pending'}
                        ]
                    }
                }
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting claim by user id" + err);
                }
                else {
                    logger.info("Sucessfully getting claim by user id" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("Get Claim By User Counter Value" + counter_var);
    });
};

exports.get_invoice_by_status_comp_name = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Invoice By Status Compnay name Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var q = {};

            var status = req.params.status;
            console.log("d" + status);
            var company_name = req.params.company_name;
            q = {active: 1, status: status, company_name: company_name};

            if (status == "All") {
                delete q.status;
            }
            if (status == "payment_rec") {
                q = {active: 1, 'payment_received': 'Y', 'company_name': company_name};
            }
            if (status == "payment_not_rec") {
                q = {active: 1, 'payment_received': 'N', 'status': 'Verified & Approved', 'company_name': company_name};
            }
            if (company_name == "" || company_name == null || company_name == 'undefined') {
                delete q.company_name;
            }

            var project = {
                "active": 1,
                "claim_id": 1,
                "user_id": 1,
                "invoice_date": 1,
                "invoice_amount": 1,
                "invoice_number": 1,
                "retailer": 1,
                "status": 1,
                "supporting_doc": 1,
                "claim_details": 1,
                "approval_comments": 1,
                "amount_track": 1,
                "inv_date_track": 1,
                "company_name": 1,
                "participant_code": 1,
                "participant_name": 1,
                "verified_amount": 1,
                "name": 1,
                "distributor_code": 1,
                "retailer_name": 1,
                "invoice_amount_excl_vat": 1,
                "payment_received": 1,
                "-_id": 1,
                "payment_track": 1
            };

            col_todo.find(q, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Invoice By Status Compnay name Counter Value" + counter_var);
    });
};

/*exports.search_by_sku = function (req, res) {
 //send_to_response([{"response": "from claim list"}], res);
 var MongoClient = mongodb.MongoClient;
 var url = util.get_connection_string("mongo");
 counter_var++;

 logger.info("Get Search By SKU Counter Value"+counter_var);
 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 //Close connection
 db.close();
 }
 else {
 logger.info("Connection Established To MongoDB");
 var col_todo = db.collection('sku_master');
 // console.log(sku)

 var find_qry = {'sku': sku.toUpperCase()};

 var project = {
 "brand": 1,
 "sku": 1,
 "description": 1,
 "model": 1,
 "active": 1,
 "mrp": 1,
 "bluerewards": 1,
 "series": 1,
 "pow_status": 1
 };

 col_todo.find({}, project).toArray(function (err, result) {
 if (err) {
 logger.info("Error in search by sku" + err);
 }
 else {
 logger.info("Sucessfully search by sku" + result.length);
 db.close();
 if (result.length > 0) {
 util.send_to_response(result, res)
 }
 }
 })
 }
 counter_var--;

 logger.info("End Seacrh By Sku Counter Value"+counter_var);
 });
 };*/

exports.top_sku = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Top SKu Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [{$unwind: "$claim_details"}, {
                $match: {
                    $and: [
                        {active: 1},
                        {'status': 'Verified & Approved'}

                    ]
                }
            },
                {$group: {_id: {'sku': "$claim_details.sku"}, total: {$sum: "$claim_details.quantity"}}},
                {
                    $project: {
                        _id: 0,
                        sku: '$_id.sku',
                        total_sku_qty: "$total"
                    }
                }, {$sort: {total_sku_qty: -1}}, {$limit: 5}];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting top sku" + err);
                }
                else {
                    logger.info("Sucessfully getting top sku" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End TOp SKU Counter Value" + counter_var);
    });
};

exports.top_distributor_invoice_amount = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Distributor invoice Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var pipeline = [{
                $match: {
                    $and: [
                        {active: 1},
                        {'status': 'Verified & Approved'}

                    ]
                }
            },
                {$group: {_id: {'retailer': "$retailer"}, total: {$sum: "$invoice_amount_excl_vat"}}},
                {
                    $project: {
                        _id: 0,
                        distributor: '$_id.retailer',
                        total_sale: "$total"
                    }
                }, {$sort: {total_sale: -1}}, {$limit: 5}];

            col_todo.aggregations(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting top distributor invoice amount" + err);
                }
                else {
                    logger.info("Sucessfully getting top distributor invoice amount" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Top Distributor Invoice Counter Value" + counter_var);
    });
};

exports.search_availability_by_sku = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Search Availabiltiy By SKU  Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            // logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sku_wise_tran');
            // console.log(sku)
            var sku = req.params.sku;
            var find_qry = {'sku': sku.toUpperCase()};

            var project = {
                "frm_participant": 1,
                "to_participant": 1,
                "flow_type": 1,
                "tran_type": 1,
                "quantity": 1,
                "credit_quantity": 1,
                "debit_quantity": 1,
                "active": 1,
                "created_date": 1
            };

            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting availability by sku" + err);
                }
                else {
                    logger.info("Sucessfully getting availability by sku" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Search Availability By SKu Counter Value" + counter_var);
    });
};

exports.get_sku_by_name = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get SKU By Name Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            // logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sku_master');
            // console.log(sku)
            var model = req.params.name;
            var find_qry = {'model': model};
            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting sku by name" + err);
                }
                else {
                    logger.info("Sucessfully getting sku by name" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End SKU By Name Counter Value" + counter_var);
    });
};

/*exports.search_by_part_code = function (req, res) {
 //send_to_response([{"response": "from claim list"}], res);
 console.log("incoming body")
 console.log(req.body)
 counter_var++;

 logger.info("Get SKU Search By Part Code Counter Value"+counter_var);
 sku.search_by_part_code(req.body.part_code)
 .then(
 function (sku_results) {

 console.log("and got the result")
 // console.log("result"+sku_results)

 // console.log( req.body.part_code.invoice_dt)
 var sku = req.body.part_code.sku;
 var invoice_date = req.body.part_code.invoice_dt;

 //We need to work with "MongoClient" interface in order to connect to a mongodb server.
 var MongoClient = mongodb.MongoClient;
 var url = util.get_connection_string("mongo");
 console.log("mongo url" + JSON.stringify(url));
 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 //Close connection
 db.close();
 }
 else {
 //Database has been connected.
 //logger.info("Connection Established To MongoDB");

 var col_todo = db.collection('pow_master');
 // console.log(sku)
 var find_qry = {"sku": sku};

 col_todo.find(find_qry).toArray(function (err, result) {
 if (err) {
 logger.info("Error in checking part code in product of the week" + err);
 }
 else {
 logger.info("Sucessfully getting the part code in pow" + result.length);

 if (result.length > 0) {
 if (result && result.length > 0) {
 for (var i = 0; i < result.length; i++) {
 /!*   var active_present = _.where(result, {"active": 1});
 console.log("active length" + active_present.length)

 if (active_present.length > 0) {
 // console.log("return 1")*!/

 var start_dt = result[i].start_dt;
 var end_dt = result[i].end_dt;
 var inv_dt = invoice_date;
 //console.log(start_dt);
 //console.log(end_dt);
 //console.log("invoe" + invoice_date);
 console.log(start_dt <= inv_dt);
 console.log(inv_dt <= end_dt);

 if ((start_dt <= inv_dt) && (inv_dt <= end_dt)) {
 // console.log(sku_results[0])
 console.log("success");
 sku_results[0].pow_status = 1;
 console.log(sku_results);
 }
 else {
 console.log("no success");
 // console.log(sku_results[0])
 sku_results[0].pow_status = 0;
 console.log(sku_results)
 }
 if ((start_dt <= inv_dt) && (inv_dt <= end_dt)) {
 break;
 }
 }
 }
 }
 else {
 console.log("if sku not present");
 // console.log(sku_results[0])
 // sku_results[0].pow_status = 0;
 console.log(sku_results)
 }


 send_to_response(sku_results, res)
 }
 })

 }
 });
 },

 function (err) {

 res.send(err);
 }

 );
 counter_var--;

 logger.info("End Search By Part Code Counter Value"+counter_var);
 };*/

exports.search_by_part_code = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Search By Part Cose Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            var sku_master = db.collection('sku_master');
            // console.log(sku)
            console.log("incoming body" + JSON.stringify(req.body))
            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };
            var sku = req.body.part_code.sku
            var sku_value = sku.toUpperCase();
            var invoice_date = req.body.part_code.invoice_dt;
            var find_qry = {"sku": sku_value};
            sku_master.find(find_qry, project).toArray(function (err, sku_result) {
                if (err) {
                    logger.info("Error in searching sku list" + err);
                }
                else {
                    logger.info("Sucessfully getting sku list" + sku_result.length);
                    db.close();
                    if (sku_result.length > 0) {
                        console.log("sku reult" + JSON.stringify(sku_result))
                        check_pow_invoice(sku, sku_result, invoice_date, function (err, result) {
                            if (err) {
                                logger.info("Error In Inserting Transaction Data.");
                                util.send_to_response({
                                    response_code: 0,
                                    msg: 'Error In Inserting Transaction Data.',
                                    error_msg: err
                                }, res);
                            }
                            else {
                                console.log("back to method " + JSON.stringify(result))
                                util.send_to_response(result, res);
                            }
                        })
                    }
                }
            })
        }

    })
};


exports.get_data_points_push = function (req, res) {

    var final = [];

    var first_slab_array = [];
    var second_slab_array = [];
    var third_slab_array = [];
    var four_slab_array = [];
    var five_slab_array = [];
    var six_slab_array = [];
    var send_object = {};
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Program By Company Region Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {

            var col_sales_registry = db.collection('sales_registry');
            logger.info("Connection Established To MongoDB");
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {status: "Verified & Approved"},
                            {
                                $or: [
                                    {is_process: {$exists: false}},
                                    {is_process: 'No'}
                                ]
                            },
                            {active: 1}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            'user_id': "$user_id", retailer_code: '$retailer_code',
                            retailer_name: '$retailer_name'
                        },
                        total_count: {$sum: 1}

                    }
                },
                {
                    $project: {
                        _id: 0,
                        user_id: '$_id.user_id',
                        retailer_code: '$_id.retailer_code',
                        retailer_name: '$_id.retailer_name',
                        total_count: '$total_count'
                    }
                },
                {$sort: {user_id: -1}}

            ];

            col_sales_registry.aggregate(pipeline).toArray(function (err, sales_registry_result) {
                if (err) {
                    logger.info("Error in Fetching sales_registry Data,err:" + err.message);
                    //Close connection
                    db.close();
                } else {
                    //                //Close connection
                    db.close();
                    logger.info("Fetching sales_registry User Data Successfully From sales_registry Collection");
                    logger.info("Total Retailer Count Sales Registry Result Count : " + sales_registry_result.length);
//                      logger.info(sales_registry_result);

                    var uniq_user_ids = _.uniq(_.pluck(sales_registry_result, 'user_id'));

                    logger.info("Total Unique Retailer From Sales Registry. Result Count : " + uniq_user_ids.length);
                    logger.info("Unique User IDs : " + JSON.stringify(uniq_user_ids));

                    var email_send = 0;

                    if (uniq_user_ids && uniq_user_ids.length > 0) {

                        get_user_wise_credited_point_calc('', function (err, mysql_user_result) {
                            if (err) {
                                logger.info("Error In Fetching Retailer Transaction Data,err : " + err);
                                callback();
                            }
                            else {
                                logger.info("Fetching Retailer Transaction Data From Mysql. Count ==>" + mysql_user_result.length);
//                                    logger.info("Fetching Retailer Transaction Data From Mysql." + JSON.stringify(mysql_user_result));

                                var mysql_credited_points_list = mysql_user_result[0];
                                logger.info("Fetching Retailer Transaction Data From Mysql." + mysql_credited_points_list.length);
                                logger.info("Fetching Retailer Transaction Data From Mysql." + JSON.stringify(mysql_credited_points_list));

                                if (mysql_credited_points_list && mysql_credited_points_list.length > 0) {

                                    var TranCount = 0;

                                    var final = [];

                                    async.each(uniq_user_ids, function (file, callback) {

                                        TranCount = TranCount + 1;
                                        logger.info("Processing User ID No. " + TranCount + " .User ID ==> " + file);

                                        if (file == "" || file == null) {
                                            logger.info('In This Case User ID Is Invalid.');
                                            callback();
                                        }
                                        else {
                                            // Do work to process file here
                                            var process_user_id = file;

                                            logger.info('Valid User ID. ' + process_user_id);

                                            get_user_id_wise_invoice_details(process_user_id, function (err, tran_result) {
                                                if (err) {
                                                    logger.info("Error In Fetching Retailer Transaction Data,err : " + err);
                                                    callback();
                                                }
                                                else {
                                                    logger.info("Fetching Retailer Transaction Data. Count ==> " + tran_result.length);
//                                                        logger.info("Fetching Retailer Success Transaction Data. ==> " + JSON.stringify(tran_result));
//                                                        callback();

                                                    calc_user_id_wise_invoice_amount(process_user_id, tran_result, function (err, calc_invoice_amount) {
                                                        if (err) {
                                                            logger.info("Error In Calculation Invoice Amount Based On SKU Classification ,err : " + err);
                                                            callback();
                                                        }
                                                        else {


                                                            logger.info("Calculation Invoice Amount Based On SKU Classification.==> " + process_user_id + " : Invoice Amount ==> " + calc_invoice_amount);

                                                            var already_credited_points = 0;
                                                            var will_be_credited_points = 0;
                                                            var total_balance_points = 0;

                                                            var obj_mysql_user = _.filter(mysql_credited_points_list, {"email_id": process_user_id});

                                                            var obj_ret_data = _.filter(sales_registry_result, {user_id: process_user_id});


                                                            if (obj_mysql_user && obj_mysql_user.length == 1) {

                                                                already_credited_points = parseInt(obj_mysql_user[0].PointsAlloted);

                                                                logger.info("Already Credited Points. ==> " + process_user_id + " : Points ==> " + already_credited_points);

                                                                will_be_credited_points = parseInt(calc_invoice_amount) - parseInt(already_credited_points);

                                                                total_balance_points = parseInt(calc_invoice_amount) + parseInt(already_credited_points);

                                                                logger.info("Will Be Credited Points. ==> " + process_user_id + " : Points ==> " + will_be_credited_points);
                                                                logger.info("Total Points. ==> " + process_user_id + " : Points ==> " + total_balance_points);

                                                                if (total_balance_points >= 50) {
                                                                    // if (total_balance_points) {

                                                                    var points = parseInt(will_be_credited_points);

                                                                    if (points > 0) {
                                                                        if (obj_ret_data && obj_ret_data.length == 1) {
                                                                            var retailer_name = obj_ret_data[0].retailer_name;
                                                                            /*                                            if(500>points&&points>250)
                                                                             {
                                                                             var fis_slab= 0;
                                                                             fis_slab++;
                                                                             console.log("who and all in 250 slab" +points +"retailer"+process_user_id+"count"+fis_slab)
                                                                             var first_slab={};
                                                                             first_slab.points=points;
                                                                             first_slab.user_id=process_user_id;
                                                                             first_slab.name=retailer_name;
                                                                             first_slab_array.push(first_slab);
                                                                             console.log("first slab" +JSON.stringify(first_slab_array));

                                                                             }
                                                                             else  if(1000>points&&points>500)
                                                                             {
                                                                             console.log("who and all in 500 slab" +points +"retailer"+process_user_id)
                                                                             var second_slab={};
                                                                             second_slab.points=points;
                                                                             second_slab.user_id=process_user_id;
                                                                             second_slab.name=retailer_name;
                                                                             second_slab_array.push(second_slab);
                                                                             console.log("Second slab" +JSON.stringify(second_slab_array));
                                                                             }
                                                                             else  if(1500>points&&points>100)
                                                                             {
                                                                             console.log("who and all in 1000 slab" +points +"retailer"+process_user_id)
                                                                             var third_slab={};
                                                                             third_slab.points=points;
                                                                             third_slab.user_id=process_user_id;
                                                                             third_slab.name=retailer_name
                                                                             third_slab_array.push(third_slab);
                                                                             console.log("Second slab" +JSON.stringify(third_slab_array));
                                                                             }
                                                                             else  if(2000>points&&points>15000)
                                                                             {
                                                                             console.log("who and all in 1500 slab" +points +"retailer"+process_user_id)
                                                                             var four_slab={};
                                                                             four_slab.points=points;
                                                                             four_slab.user_id=process_user_id;
                                                                             four_slab.name=retailer_name
                                                                             four_slab_array.push(four_slab);
                                                                             console.log("Second slab" +JSON.stringify(four_slab_array));
                                                                             }
                                                                             else  if(3000>points&&points>2000)
                                                                             {
                                                                             console.log("who and all in 2000 slab" +points +"retailer"+process_user_id)
                                                                             var five_slab={};
                                                                             five_slab.points=points;
                                                                             five_slab.user_id=process_user_id;
                                                                             five_slab.name=retailer_name
                                                                             five_slab_array.push(five_slab);
                                                                             console.log("Second slab" +JSON.stringify(five_slab_array));
                                                                             }
                                                                             else  if(4000>points&&points>3000)
                                                                             {
                                                                             console.log("who and all in 3000 slab" +points +"retailer"+process_user_id)
                                                                             var six_slab={};
                                                                             six_slab.points=points;
                                                                             six_slab.user_id=process_user_id;
                                                                             six_slab.name=retailer_name
                                                                             six_slab_array.push(six_slab);
                                                                             console.log("Second slab" +JSON.stringify(six_slab_array));
                                                                             }

                                                                             send_object.first_slab=first_slab_array;
                                                                             send_object.second_slab=second_slab_array;
                                                                             send_object.third_slab=third_slab_array;
                                                                             send_object.four_slab=four_slab_array;
                                                                             send_object.five_slab=five_slab_array;
                                                                             send_object.six_slab=six_slab_array;
                                                                             console.log("final Object" +JSON.stringify(send_object));
                                                                             final.push(send_object);*/

                                                                            var send_object = {};
                                                                            send_object.points = Math.round(points / 1000);
                                                                            send_object.name = retailer_name;
                                                                            send_object.user_id = process_user_id;
                                                                            console.log("send" + JSON.stringify(send_object));
                                                                            final.push(send_object);
                                                                            callback();


                                                                        }
                                                                        else {
                                                                            logger.info("Getting Multiple User Or Not Found. ==> " + process_user_id);
                                                                            callback();
                                                                        }
                                                                    }
                                                                    else {
                                                                        logger.info("Will Be Credited Points Is Less Than 0.");
                                                                        callback();
                                                                    }
                                                                }
                                                                else {
                                                                    logger.info("Points Balance Below 50. ==> " + process_user_id + " : Total Points ==> " + total_balance_points);
                                                                    callback();
                                                                }
                                                            }
                                                            else if (obj_mysql_user.length == 0) {
                                                                logger.info("User Not Registered In Mysql.==> " + process_user_id);
                                                                // writer.write([obj_ret_data[0].retailer_code, obj_ret_data[0].account_mobile, obj_ret_data[0].account_mobile, "MySql Import Pending"]);
                                                                callback();
                                                            }
                                                            else {
                                                                logger.info("User Multiple Registered In Mysql.==> " + process_user_id);
                                                                // writer.write([obj_ret_data[0].retailer_code, obj_ret_data[0].account_mobile, obj_ret_data[0].account_mobile, "MySql Import Pending"]);
                                                                callback();
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }, function (err) {
                                        // if any of the file processing produced an error, err would equal that error
                                        if (err) {
                                            // One of the iterations produced an error.
                                            // All processing will now stop.
                                            logger.info('A file failed to process');
                                        }
                                        else {
                                            logger.info('All files have been processed successfully');
                                            util.send_to_response(final, res)
                                            serverDB.close();

                                        }
                                    });
                                }
                                else {
                                    logger.info("In Mysql User Not Present.");

                                    logger.info('Close Mysql Connection');
                                    // mysql_conn.end();

                                    logger.info('Close Big Mongo Connection');
                                    serverDB.close();
                                }
                            }
                        })
                    }
                    else {
                        logger.info('No New User Found For Invoice Upload.');
                        logger.info('Close Mysql Connection');
                        // mysql_conn.end();
                        logger.info('Close Big Mongo Connection');
                        serverDB.close();
                    }
                }
            });

        }

        counter_var--;
        logger.info("End Program By Company Region Counter Value" + counter_var);
    });


};

exports.insert_user_points = function (req, res) {
    console.log("incoming body" + JSON.stringify(req.body))
    var conn = mysql.get_mysql_connection();
    var retailer_points = req.body.points / 1000;

    var sql = "call usp_insert_user_points('" + req.body.points + "','" + req.body.user_id + "','" + req.body.name + "')";
    conn.query(sql, function (err, rows) {
        if (err) {
            console.log(err);
            conn.end();
            conn = null;
        }
        console.log("rows" + JSON.stringify(rows));
        // util.send_to_response(rows,res);
        if (rows.affectedRows = 1) {
            get_disti_data(req.body.user_id, function (err, mysql_user_result) {
                if (err) {
                    console.log("err");
                }
                else {
                    console.log("mysql_user_result" + JSON.stringify(mysql_user_result));
                    var distributor_name = mysql_user_result[0].distributor_name;
                    var disti_champ = mysql_user_result[0].disti_champ_name;
                    get_mysql_data_of_disti(distributor_name, disti_champ, function (err, mysl_disti_data) {
                        if (err) {
                            console.log("err")
                        }
                        else {
                            console.log("mysd" + JSON.stringify(mysl_disti_data));
                            util.send_to_response(mysl_disti_data, res)
                        }

                    })

                }
            })
        }
    })
};
function get_mysql_data_of_disti(disti_name, disti_champ_name, callback) {

    var conn = mysql.get_mysql_connection();
    var sql = "call usp_get_disti_data('" + disti_name + "','" + disti_champ_name + "')";
    conn.query(sql, function (err, rows) {

        if (err) {
            console.log(err)
            conn.end();
            conn = null;
        }
        console.log("rows" + JSON.stringify(rows));
        callback(null, rows)
    })

}
function get_disti_data(user_id, callback) {
    try {
        console.log("enter function")
        var MongoClient = mongodb.MongoClient;
        var url = util.get_connection_string("mongo");
        MongoClient.connect(url, function (err, db) {
            if (err) {
                logger.info("Unable to connect to the mongoDB server. Error:" + err);
                //Close connection
                db.close();
            }
            else {
                //Database has been connected.
                //logger.info("Connection Established To MongoDB");

                var col_todo = db.collection('sales_registry');
                // console.log(sku)
                var find_qry = {"user_id": user_id};

                col_todo.find(find_qry).toArray(function (err, result) {
                    if (err) {
                        logger.info("Error in checking part code in product of the week" + err);
                        callback(err, null);
                    }
                    else {
                        logger.info("Sucessfully getting the part code in pow" + result.length);


                        // send_to_response(sku_results, res)
                        callback(null, result);
                    }

                })

            }

        })

    }
    catch
        (ex) {
        logger.info("Error Occurred In Method get_distributor_retailer_map. : " + ex);
        callback(ex, null);
    }
};

exports.insert_disti_points = function (req, res) {
    var result = [];
    // console.log("incoming body" + JSON.stringify(req.body))
    var conn = mysql.get_mysql_connection();
    var array = req.body;
    var user_array = [];
    // console.log("req.body.disti_array.length" + JSON.stringify(req.body))
    for (var i = 0; i < array.length; i++) {
        console.log(array[i]);
        console.log("user" + req.body[i].user_id);
        user_array.push(req.body[i].user_id)
        console.log("user_array" + JSON.stringify(user_array));
        var round = JSON.stringify(user_array).replace(/]|[[]/g, '');
        // console.log("round" + JSON.stringify(round))


    }
    var round = "";
    for (var i = 0; i < user_array.length; i++) {
        if (i == user_array.length - 1) {
            round += "'" + user_array[i] + "'"
        }
        else {
            round += "'" + user_array[i] + "'" + ","
        }
    }

    console.log("final" + round)
    var sql = "select SUM(txn_amount) as sum_points,user_id from user_points where user_id in(" + round;
    sql += ") and txn_type = 'c' group by user_id;";
    conn.query(sql, function (err, rows) {
        if (err) {
            console.log(err);

        }
        // console.log("rows" + JSON.stringify(rows));
        // util.send_to_response(rows, res);
        if (rows.length > 0) {
            for (var i = 0; i < req.body.length; i++) {
                console.log("roe" + JSON.stringify(req.body[i].user_id));
                var who_is_there = _.filter(rows, {"user_id": req.body[i].user_id});
                console.log("who" + JSON.stringify(who_is_there));
                var insert_ary = [];
                if (who_is_there.length > 0) {
                    for (var j = 0; j < who_is_there.length; j++) {
                        console.log("who can " + who_is_there[j].user_id);
                        var his_data = _.filter(rows, {"user_id": who_is_there[j].user_id});
                        var present_points = who_is_there[j].sum_points;
                        console.log("prese" + present_points);
                        var discount_points = req.body[i].points;
                        console.log("discount_points" + discount_points);
                        var to_give = parseInt(present_points) - parseInt(discount_points);
                        console.log("to_give" + to_give);
                        if (to_give > 0) {
                            console.log('come to insert')
                            var insert_object = {};
                            insert_object.points = req.body[i].points;
                            insert_object.user_id = req.body[i].user_id;
                            insert_object.name = req.body[i].name;
                            insert_ary.push(insert_object);
                            for (var k = 0; k < insert_ary.length; k++) {
                                console.log("insert_object" + JSON.stringify(insert_ary))
                                var sql1 = "call usp_insert_disti_points('" + insert_ary[k].points + "','" + insert_ary[k].user_id + "','" + insert_ary[k].name + "')"

                                conn.query(sql1, function (err, rows1) {
                                    if (err) {
                                        console.log("err" + err);
                                        conn.end();
                                        conn=null;
                                    }
                                    console.log("above zeros" + JSON.stringify(rows1))

                                })
                            }
                        }
                        else if (to_give < 0) {
                            console.log("goes in neagtive");
                        }
                    }
                }
                else {
                    console.log("insert.all" + req.body[i].user_id);
                    var insert_object = {};
                    insert_object.points = req.body[i].points;
                    insert_object.user_id = req.body[i].user_id;
                    insert_object.name = req.body[i].name;
                    insert_ary.push(insert_object);
                    for (var k = 0; k < insert_ary.length; k++) {
                        console.log("insert_object" + JSON.stringify(insert_ary))
                        var sql1 = "call usp_insert_disti_points('" + insert_ary[k].points + "','" + insert_ary[k].user_id + "','" + insert_ary[k].name + "')"

                        conn.query(sql1, function (err, rows1) {
                            if (err) {

                            }
                            console.log("above zeros all" + JSON.stringify(rows1))

                        })
                    }
                }

            }
        }
util.send_to_response("Points Pushed Sucessfully" ,res);

    })


    // util.send_to_response("Points Pushed Sucessfully", res);

}


function get_user_wise_credited_point_calc(user_id, callback) {

    try {
        console.log("inside get_user_wise_credited_point_calc")
        var conn = mysql.get_mysql_connection();
        var sql = "call get_users_points_credited ('schneider')";
        logger.info("SQL Query : " + sql);
//        logger.info(conn);

        conn.query(sql, function (error, results) {
            if (error) {
                logger.info("Error Occurred In Mysql Query Executing : " + error);
                callback(error, null);
            }
            else {
                conn.end();
                if (results.length > 0) {
                    logger.info("Successfully Getting User Details From MySql. Count : " + results.length);
//                    logger.info(results);
                    callback(null, results);
                }
                else {
                    callback(null, results);
                }
            }
        });
    }
    catch (ex) {
        logger.info("Error Occurred In Method get_user_wise_credited_point. : " + ex);
        callback(ex, null);
    }


};

function get_user_id_wise_invoice_details(user_id, callback) {
    console.log("get_user_id_wise_invoice_details")
    try {
        //Initialize allocation Collection To Get Distributor Retailer Transaction Details
        var col_allocation = serverDB.collection('sales_registry');

        //Create Transaction Query


        var pipeline = [
            {
                $unwind: "$claim_details"
            },
            {
                $match: {
                    $and: [
                        {status: "Verified & Approved"},
                        {user_id: user_id},
                        {active: 1}
                    ]
                }
            },
            {
                $project: {
                    _id: 0,
                    user_id: 1,
                    retailer_code: 1,
                    claim_details: 1
                }
            }
        ];

//        logger.info("Invoice Details Query : " + JSON.stringify(pipeline));

        col_allocation.aggregate(pipeline).toArray(function (err, invoice_list) {
                if (err) {
                    logger.info("Error In Fetching Retailer Transaction Data,err:" + err.message);
                    callback(err, null);
                } else {
                    logger.info("Fetching Retailer Transaction Details. Count : " + invoice_list.length);
//                    logger.info(invoice_list);
                    callback(null, invoice_list);
                }
            }
        );
    }
    catch (ex) {
        logger.info("Error Occurred In Method get_user_id_wise_invoice_details. : " + ex);
        callback(ex, null);
    }

};
function calc_user_id_wise_invoice_amount(user_id, transaction_details, callback) {

    try {
        //Initialize allocation Collection To Get Distributor Retailer Transaction Details
        var col_allocation = serverDB.collection('sales_registry');

        //Create Transaction Query
//        logger.info("Invoice Details : " + JSON.stringify(transaction_details));

        var sku_list = _.pluck(transaction_details, 'claim_details');
//        logger.info("Invoice Details : " + JSON.stringify(sku_list));

        var total_calc_invoice_amount = 0;

        if (sku_list && sku_list.length > 0) {
            for (var sk = 0; sk < sku_list.length; sk++) {
                var obj_sku = sku_list[sk];
                if (obj_sku.series == "A") {
                    total_calc_invoice_amount = total_calc_invoice_amount + parseInt(obj_sku.quantity) * ( obj_sku.unit_price * 1) * 1.5;
                }
                if (obj_sku.series == "B") {
                    total_calc_invoice_amount = total_calc_invoice_amount + parseInt(obj_sku.quantity) * ( obj_sku.unit_price * 1) * 1;
                }
                if (obj_sku.series == "C") {
                    total_calc_invoice_amount = total_calc_invoice_amount + parseInt(obj_sku.quantity) * ( obj_sku.unit_price * 1) * .75;
                }

            }
        }
        callback(null, total_calc_invoice_amount);

    }
    catch
        (ex) {
        logger.info("Error Occurred In Method calc_user_id_wise_invoice_amount. : " + ex);
        callback(ex, null);
    }

}

function check_pow_invoice(sku_value, sku_results, invoice_date, callback) {

    try {
        console.log("enter function")
        var MongoClient = mongodb.MongoClient;
        var url = util.get_connection_string("mongo");
        MongoClient.connect(url, function (err, db) {
            if (err) {
                logger.info("Unable to connect to the mongoDB server. Error:" + err);
                //Close connection
                db.close();
            }
            else {
                //Database has been connected.
                //logger.info("Connection Established To MongoDB");

                var col_todo = db.collection('pow_master');
                // console.log(sku)
                var find_qry = {"sku": sku_value};

                col_todo.find(find_qry).toArray(function (err, result) {
                    if (err) {
                        logger.info("Error in checking part code in product of the week" + err);
                    }
                    else {
                        logger.info("Sucessfully getting the part code in pow" + result.length);

                        if (result.length > 0) {
                            if (result && result.length > 0) {
                                for (var i = 0; i < result.length; i++) {
                                    /*   var active_present = _.where(result, {"active": 1});
                                     console.log("active length" + active_present.length)

                                     if (active_present.length > 0) {
                                     // console.log("return 1")*/

                                    var start_dt = result[i].start_dt;
                                    var end_dt = result[i].end_dt;
                                    var inv_dt = invoice_date;
                                    //console.log(start_dt);
                                    //console.log(end_dt);
                                    //console.log("invoe" + invoice_date);
                                    console.log(start_dt <= inv_dt);
                                    console.log(inv_dt <= end_dt);

                                    if ((start_dt <= inv_dt) && (inv_dt <= end_dt)) {
                                        // console.log(sku_results[0])
                                        console.log("success");
                                        sku_results[0].pow_status = 1;
                                        console.log(sku_results);
                                    }
                                    else {
                                        console.log("no success");
                                        // console.log(sku_results[0])
                                        sku_results[0].pow_status = 0;
                                        console.log(sku_results)
                                    }
                                    if ((start_dt <= inv_dt) && (inv_dt <= end_dt)) {
                                        break;
                                    }
                                }
                            }
                        }
                        else {
                            console.log("if sku not present");
                            // console.log(sku_results[0])
                            // sku_results[0].pow_status = 0;
                            console.log(sku_results)
                        }


                        // send_to_response(sku_results, res)
                        callback(null, sku_results);
                    }

                })

            }

        })

    }
    catch
        (ex) {
        logger.info("Error Occurred In Method get_distributor_retailer_map. : " + ex);
        callback(ex, null);
    }
}

exports.search_sku_list = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Search SKU list Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sku_master');
            // console.log(sku)

            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };

            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in searching sku list" + err);
                }
                else {
                    logger.info("Sucessfully getting sku list" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Search SKu List  Counter Value" + counter_var);
    });
};

//***************************//

//********* program **********methods

exports.get_region_by_level = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Region By Level Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)
            var level1 = req.params.level1;
            var level2 = req.params.level2;
            var find_qry = {"level": {$in: [level1, level2]}};

            var project = {
                "company": 1, "id": 1, "name": 1, "description": 1, "level": 1, "parent_id": 1
            };

            col_todo.find(find_qry, project).sort("level name").toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting region by level" + err);
                }
                else {
                    logger.info("Sucessfully getting region by level" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get REgion By level Counter Value" + counter_var);
    });
};

exports.get_all_region = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get all Region  Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)

            var project = {
                "company": 1, "id": 1, "name": 1, "description": 1, "level": 1, "parent": 1, "_id": 1
            };

            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in get all region" + err);
                }
                else {
                    logger.info("Sucessfully get all region" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get  all Region  Counter Value" + counter_var);

    });
};

exports.get_region_by_id = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Region By Id Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('region');
            // console.log(sku)
            var find_qry = {"id": {$regex: '^' + id}};

            var project = {
                "company": 1, "id": 1, "name": 1, "description": 1, "level": 1, "parent_id": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting region by id" + err);
                }
                else {
                    logger.info("Sucessfully getting region by id" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Region By Id Counter Value" + counter_var);
    });
};

exports.get_specific_region_by_id = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Specific Region By Id Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('region');
            // console.log(sku)

            var find_qry = {"id": req.params.id};

            var project = {
                "id": 1,
                "name": 1,
                "description": 1,
                "region": 1,
                "rule": 1,
                "start_date": 1,
                "end_date": 1,
                "company": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting specific region by id" + err);
                }
                else {
                    logger.info("Sucessfully getting specific region by id" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Specific Region By Id Counter Value" + counter_var);
    });
};

exports.save_region = function (req, res) {

    counter_var++;
    logger.info("Save Region Counter Value" + counter_var);
    var region_data = {
        company: req.body.company,
        id: req.body.id,
        name: req.body.name,
        parent_id: req.body.parent_id,
        description: req.body.description,
        level: req.body.level
    };
    var p1 = new region(region_data);
    p1.save(function (req) {
        mongoose.connection.close(function () {
            console.log('  Mongoose connection disconnected save ');
        });
        res.send('item saved');
    });


    counter_var--;
    logger.info("End Save Region Counter Value" + counter_var);
};

// ********* program ****************
//arnab added for programe/add/

exports.add_program = function (req, res) {

    counter_var++;
    logger.info("Add Program Counter Value" + counter_var);
    var program_data = {
        company: req.body.company,
        id: req.body.id,
        name: req.body.name,
        description: req.body.description,
        region: req.body.region,
        start_date: new Date(req.body.start_date),
        end_date: new Date(req.body.end_date)
    };
    var p = new program(program_data);
    p.save(function (req) {
        mongoose.connection.close(function () {
            console.log('  Mongoose connection disconnected save ');
        });
        res.send('Program Saved Successfully');
    });

    counter_var--;
    logger.info("End Add Program  Counter Value" + counter_var);
};

exports.get_all_program = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get All Program  Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('claim');
            // console.log(sku)

            var project = {
                "id": 1,
                "name": 1,
                "description": 1,
                "region": 1,
                "rule": 1,
                "start_date": 1,
                "end_date": 1,
                "company": 1
            };

            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting all program" + err);
                }
                else {
                    logger.info("Sucessfully getting all program" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var++;
        logger.info("End All Program Counter Value" + counter_var);
    });
};

exports.get_program_by_id = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Program By Id Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('program');
            // console.log(sku)
            var find_qry = {"id": req.params.id};
            var project = {
                "id": 1,
                "name": 1,
                "description": 1,
                "region": 1,
                "rule": 1,
                "start_date": 1,
                "end_date": 1,
                "company": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting program by id" + err);
                }
                else {
                    logger.info("Sucessfully getting program by id" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
    });
};
exports.get_program_by_company_region = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Program By Company Region Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('program');
            // console.log(sku)
            var company = req.params.company;
            var region = req.params.region;
            var date = new Date();
            var pipeline = [{$unwind: "$region"},
                {
                    $match: {
                        $and: [
                            {"region": region},
                            {"company": company},
                            {"active": 1},
                            {'start_date': {$lte: date}}
                        ]
                    }
                },
                {$sort: {start_date: -1}},
                {$project: {_id: 0, name: 1, id: 1, rule: 1}},
                {$limit: 6}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting program by company id" + err);
                }
                else {
                    logger.info("Sucessfully getting program by company id" + result.length);
                    db.close();
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Program By Company Region Counter Value" + counter_var);
    });
};

// ********* end program ****************

//********************  user  methods   *************************************
exports.user_login = function (req, res) {
    counter_var++;
    logger.info("Enter login api counter_value" + counter_var);
    var conn = mysql.get_mysql_connection();

    // var sql = "call user_login_new ('" + req.body.email_id + "','" + req.body.password  + "','" + req.body.company + "')";

    var sql = "call user_login_new('" + req.body.email_id + "','" + req.body.password + "','" + req.body.company + "')";

    // console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            logger.info("user login validated from mysql");
            if (rows[0][0].msg == 'login successful') {
                rows[0][0].token = token.assign({email_id: req.body.email_id});
            }
            res.send(rows);
            conn.end();
            conn = null;
        });
    counter_var--;
    logger.info("Closing login api counter_value" + counter_var);
};

exports.terms_condition_update = function (req, res) {

    var conn = mysql.get_mysql_connection();

    var sql = "call update_terms_cond_info ('" + req.body.email_id + "','" + req.body.terms_status + "','" + req.body.terms_date + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //logger.info("user terms and condition validated from mysql");
            conn.end();
            conn = null;

            //rows[0][0].token = token.assign({email_id: req.body.email_id});
            res.send('T&C Updated');
        });
};

exports.get_user_company_name = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "select distinct company_name from extra_customer_info a, user_registration b " +
        "where a.email_id=b.email_id and b.company='" + req.params.company + "' and company_name like '" + req.params.srch_key + "%';";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            conn.end();
            conn = null;
            res.send(rows);
        });
};

exports.get_user_company_name_new = function (req, res) {

    var conn = mysql.get_mysql_connection();

    var sql = "select company_name,a.previous_status,a.previous_status_name,a.current_status from extra_customer_info a, user_registration b " +
        "where a.email_id=b.email_id and b.company='" + req.params.company + "' and company_name like '" + req.params.srch_key + "%';";

    // console.log('SQL');
    // console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            conn.end();
            conn = null;
            //  console.log('Final Result');
            // console.log(rows);
            res.send(rows);
        });
};


exports.change_password = function (req, res) {

    var conn = mysql.get_mysql_connection();

    var sql = "call change_password ('" + req.body.email_id + "','" + req.body.company + "', '" + req.body.old_password + "','" + req.body.new_password + "','" + '' + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //logger.info("password change");
            conn.end();
            conn = null;

            res.send(rows);
        });
};

//mysql company get method

exports.get_retailer_si_by_typeid = function (req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_retailer_si_by_typeid  ( '" + req.body.type_id + "','" + req.body.participant_code + "')";
    //console.log('till here');
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            //logger.info("validate from mysql");
            res.send(rows);
        });
};

exports.get_si_by_retailerid = function (req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call get_si_by_retailerid  ( '" + req.body.retailer_id + "')";
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            logger.info(" validate from mysql");
            res.send(rows);
        });
};

exports.get_company = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT id,name FROM company;";
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            logger.info(" validate from mysql");
            res.send(rows);
        });
};

exports.get_top_retailer_by_points_cr = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "call get_top_retailer_by_points_cr()";
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            logger.info(" validate from mysql");
            res.send(rows);
        });
};

exports.get_user_details = function (req, res) {
    var conn = mysql.get_mysql_connection();

    var sql = "SELECT first_name,last_name,email_id FROM user_registration where company='" + req.params.company + "' order by first_name;"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            logger.info(" validate from mysql");
            res.send(rows);
        });
};

exports.save_user = function (req, res) {

    counter_var++;
    logger.info("Save User Counter Value" + counter_var);
    var user_data = {
        user_id: req.body.user_id,
        company: req.body.company,
        region_id: req.body.region_id,
        region_name: req.body.region_name,
        title: req.body.title
    };
    var p1 = new user(user_data);
    p1.save(function (req) {
        mongoose.connection.close(function () {
            console.log('  Mongoose connection disconnected save ');
        });
        res.send('item saved');
    });

    counter_var--;
    logger.info("End Save User Counter Value" + counter_var);
};

exports.update_user_basic_info = function (req, res) {

    counter_var++;
    logger.info("Update User Basic Info Counter Value" + counter_var);

    var conn = mysql.get_mysql_connection();
    //    var dobFormat= new Date(req.body.user_dob).toFormat('YYYY-MM-DD');
    //    var dojFormat= new Date(req.body.comp_found_date).toFormat('YYYY-MM-DD');

    var dobFormat = req.body.user_dob;
    var dojFormat = req.body.comp_found_date;

    var sql = "update user_registration set first_name='" + req.body.first_name + "', " +
        "last_name='" + req.body.last_name + "'," +
        "mobile_no='" + req.body.mobile_no + "'," +
        "dob='" + dobFormat + "'," +
        "doj='" + dojFormat + "'," +
        "gender='" + req.body.gender + "'," +
        "qualification='" + req.body.qualification + "'," +
        "personal_interests='" + req.body.personal_interests + "'," +
        "schneider_brands='" + req.body.schneider_brands + "'," +
        "non_schneider_brands='" + req.body.non_schneider_brands + "'," +
        "user_pic='" + req.body.profile_img + "' where email_id='" + req.body.email_id + "';";

    //    var sql = "call update_user_details('"+'user_pic'+"','"
    //        +req.body.first_name+ "', '"
    //        +req.body.last_name+ "','"
    //        +req.body.mobile_no+ "','"
    //        + dobFormat + "','"
    //        + dojFormat + "','"
    //        + req.body.gender+ "','"
    //        +req.body.qualification+ "','"
    //        +req.body.profile_img+ "','"
    //        +req.body.email_id+ "')";

    //console.log(sql);

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //logger.info("update user info");
            conn.end();
            conn = null;
            res.send('update user information successfully');
        });


    counter_var--;
    logger.info("Update Use Basic Info Counter Value" + counter_var);
};

exports.update_user_address = function (req, res) {
    var conn = mysql.get_mysql_connection();


    counter_var++;
    logger.info("Update User Address Counter Value" + counter_var);

    var sql = "call update_user_info  ('" + req.body.first_name + "','" + req.body.last_name + "', '" + req.body.gender + "','" + req.body.email_id + "','" + req.body.mobile_no + "','" + req.body.office_no + "','" + req.body.street + "','" + req.body.city + "','" + req.body.address + "','" + req.body.state + "','" + req.body.country + "','" + req.body.zipcode + "','" + "" + "','" + 2 + "','" + new Date() + "','" + req.body.company + "','" + "" + "','" + new Date() + "','" + 1 + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            //logger.info("update address");
            conn.end();
            conn = null;
            res.send('update user address successfully');
        });

    counter_var--;
    logger.info("End Update User Address  Counter Value" + counter_var);
};

//******************** end  post *************************************

//*********************PROFILE START*****************************//
exports.get_user_info = function (req, res) {
    var conn = mysql.get_mysql_connection();
    var sql = "call user_profile_summary ('" + req.params.email_id + "')";

    counter_var++;
    logger.info("Get User Info Counter Value" + counter_var);
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            logger.info(" validate from mysql");
            res.send(rows);
        });


    counter_var--;
    logger.info("End Get User Info Counter Value" + counter_var);
};

//*************************PROFILE END************************//

//************************** NEW PAGE UPLOAD VS POS****************//

exports.get_month_wise_invoice_amount = function (req, res) {


    counter_var++;
    logger.info("Get Month Wise Invoice Amount Counter Value" + counter_var);
    var ret_data = [];
    var ret = {retailer: req.body.retailer};
    ret_data.push(ret);
    // console.log(req.body);
    sales_registry.get_month_wise_invoice_amount(req.body.month, req.body.retailer)
        .then(
            function (results) {
                //console.log('hi');
                // console.log(results);
                if (results.length > 0) {
                    results[0].pos_amount = 0;
                    results[0].balance_amount = 0;
                }
                async.map(ret_data, get_user_data, function (err, r) {

                    //console.log(r[0]);
                    if (r[0].length > 0) {
                        var month_data = r[0][0].pos_details;
                        // console.log('month data');
                        // console.log(month_data);
                        if (results.length > 0) {
                            _.each(results, function (element, index, list) {
                                var pos_upld_data = _.where(month_data, {month: element.uploaded_month});
                                // console.log('inside async details');
                                // console.log(pos_upld_data);

                                if (pos_upld_data.length > 0) {
                                    // console.log('inside async1 details');
                                    element.pos_amount = pos_upld_data[0].pos;
                                    element.balance_amount = pos_upld_data[0].balance_amount;
                                    element.uploaded_month = pos_upld_data[0].month;
                                    element.total_sale = pos_upld_data[0].invoice_amount_excl_vat;

                                    // element = pos_upld_data[0];
                                }
                                // console.log(element);
                            });
                        }
                        else {
                            for (var i = 0; i < month_data.length; i++) {
                                if (month_data[i].month == req.body.month) {
                                    var all_data = {
                                        pos_amount: month_data[i].pos,
                                        balance_amount: month_data[i].balance_amount,
                                        uploaded_month: month_data[i].month,
                                        total_sale: month_data[i].invoice_amount_excl_vat
                                    };
                                    results.push(all_data);
                                }
                            }
                        }
                    }
                    // console.log('Last data');
                    //  console.log(results);
                    send_to_response(results, res);
                })
            },
            function (err) {
                res.send(err);
            }
        );

    counter_var--;
    logger.info("End Month Wise Invoice Amount Counter Value" + counter_var);
};

exports.get_all_month_details = function (req, res) {

    counter_var++;
    logger.info("Get All Month Details Counter Value" + counter_var);
    pos.get_retailer_data(req.body.retailer)
        .then(
            function (results) {
                //  console.log(results);
                send_to_response(results, res);

                counter_var--;
                logger.info("End Get All Month Details Counter Value" + counter_var);
            })

};

exports.insert_pos_details = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);

    counter_var++;
    logger.info("Insert POS Details Counter Value" + counter_var);
    var ret_data = [];
    var ret = {retailer: req.body.retailer};
    ret_data.push(ret);

    async.map(ret_data, get_user_data, function (err, r) {

        // console.log(r[0].length);
        var user_data = r[0];

        var flag = 0;

        if (user_data.length > 0) {
            // console.log(user_data[0].pos_details);
            var details = user_data[0].pos_details;

            for (var i = 0; i < details.length; i++) {
                //   console.log(i);
                if (details[i].month == req.body.pos_details[0].month) {
                    details[i].pos = req.body.pos_details[0].pos;
                    details[i].balance_amount = req.body.pos_details[0].balance_amount;
                    flag = 1;
                    break;
                }
            }

            if (flag == 0) {
                details.push(req.body.pos_details[0]);
            }
            //            _.each(details, function(element, index, list){
            //                var pos_upld_data=_.where(req.body.pos_details, {month: element.month});
            //                console.log('inside async details');
            //                                      console.log(pos_upld_data);
            ////                        console.log(disti_sku_data[0].total_sale);
            //                if(pos_upld_data.length > 0)
            //                {
            //                    console.log('inside async1 details');
            //                    element.pos = pos_upld_data[0].pos;
            //                    element.balance_amount = pos_upld_data[0].balance_amount;
            //                }
            //                else{
            //
            //                    console.log('inside async1 details else part');
            //                    details.push(req.body.pos_details[0]);
            //                    //console.log(req.body.pos_details);
            //
            //                }
            //
            //                console.log(element);
            //                console.log(details);
            //                //_.extend(element.current_month_sale, disti_sku_data.total_sale);
            //            });

            pos.update_pos_data(details, req.body.retailer, req.body.created_by).then(function () {
                send_to_response("pos details saved", res);
            })
        }
        else {
            var pos_array = req.body.pos_details;
            var pos_data = {
                retailer: req.body.retailer,
                created_by: req.body.created_by,
                active: 1,
                created_date: new Date(),
                modified_by: "",
                modified_date: "",
                pos_details: pos_array
            };

            var p1 = new pos(pos_data);
            p1.save(function (req) {
                mongoose.connection.close(function () {
                    console.log('  Mongoose connection disconnected save ');
                });
                res.send('pos details saved');
            });
        }
    })


    counter_var--;
    logger.info("End Pos Details Counter Value" + counter_var);
};

exports.insert_pos_details_for_batchprocess = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);


    counter_var++;
    logger.info("Insert Pos Details For Batch Process Counter Value" + counter_var);

    var ret_data = [];
    var ret = {retailer: req.body.retailer};
    ret_data.push(ret);

    // console.log(req.body);

    async.map(ret_data, get_user_data, function (err, r) {

        // console.log(r);
        //console.log(r[0].length);
        var user_data = r[0];
        var flag = 0;

        if (user_data.length > 0) {
            // console.log('If Part');
            // console.log(user_data[0].pos_details);

            var details = user_data[0].pos_details;
            var req_data = JSON.parse(req.body.pos_details);
            // console.log('After parse');
            // console.log(req_data);

            // console.log(details);

            for (var i = 0; i < details.length; i++) {
                //  console.log(i);
                if (details[i].month == req_data[0].month) {
                    details[i].pos = req_data[0].pos;
                    details[i].balance_amount = req_data[0].balance_amount;
                    flag = 1;
                    break;
                }
            }

            if (flag == 0) {
                details.push(req_data[0]);
            }
            //            _.each(details, function(element, index, list){
            //                var pos_upld_data=_.where(req.body.pos_details, {month: element.month});
            //                console.log('inside async details');
            //                                      console.log(pos_upld_data);
            ////                        console.log(disti_sku_data[0].total_sale);
            //                if(pos_upld_data.length > 0)
            //                {
            //                    console.log('inside async1 details');
            //                    element.pos = pos_upld_data[0].pos;
            //                    element.balance_amount = pos_upld_data[0].balance_amount;
            //                }
            //                else{
            //
            //                    console.log('inside async1 details else part');
            //                    details.push(req.body.pos_details[0]);
            //                    //console.log(req.body.pos_details);
            //
            //                }
            //
            //                console.log(element);
            //                console.log(details);
            //                //_.extend(element.current_month_sale, disti_sku_data.total_sale);
            //            });

            pos.update_pos_data(details, req.body.retailer, req.body.created_by).then(function () {
                mongoose.connection.close(function () {
                    console.log('  Mongoose connection disconnected update_pos_data ');
                });
                send_to_response("pos details saved", res);
            })
        }
        else {
            // console.log('Else Part');
            // console.log(req.body);
            var pos_array = JSON.parse(req.body.pos_details);
            // console.log(pos_array);

            var pos_data = {
                retailer: req.body.retailer,
                created_by: req.body.created_by,
                active: 1,
                bucket: req.body.bucket,
                total_purchased: req.body.total_purchased,
                total_uploaded: req.body.total_uploaded,
                total_balance: req.body.total_balance,
                created_date: new Date(),
                modified_by: "",
                modified_date: "",
                pos_details: pos_array
            };

            // console.log(pos_data);
            var p1 = new pos(pos_data);
            p1.save(function (req1) {
                //send_to_response("pos details saved", res);
                res.send('pos details saved');
            });
        }
    });

    counter_var--;
    logger.info("End Insert Pos Details For Batch Process Counter Value" + counter_var);
    //console.log('Final Out');
    // send_to_response("pos details saved", res);
};

// New Added For Month Entry

exports.insert_pos_details_for_batchprocess_by_month = function (req, res) {
    //send_to_response([{"response": "from claim list"}], res);

    counter_var++;
    logger.info("Insert Pos Details For Batch Process By Month Counter Value" + counter_var);
    var ret_data = [];
    var ret = {retailer: req.body.retailer};
    ret_data.push(ret);

    // console.log(req.body);

    async.map(ret_data, get_user_data, function (err, r) {

        // console.log(r);
        //console.log(r[0].length);
        var user_data = r[0];
        var flag = 0;

        if (user_data.length > 0) {
            // console.log('If Part');
            // console.log(user_data[0].pos_details);

            var details = user_data[0].pos_details;
            var req_data = JSON.parse(req.body.pos_details);
            // console.log('After parse');
            //console.log(req_data);

            // console.log(details);

            for (var i = 0; i < details.length; i++) {
                // console.log(i);
                if (details[i].month == req_data[0].month) {
                    details[i].pos = req_data[0].pos;
                    details[i].balance_amount = req_data[0].balance_amount;
                    flag = 1;
                    break;
                }
            }

            if (flag == 0) {
                details.push(req_data[0]);
            }

            pos.update_pos_data(details, req.body.retailer, req.body.created_by).then(function () {
                send_to_response("pos details saved", res);
            })
        }
        else {
            // console.log('Else Part');
            // console.log(req.body);
            var pos_array = JSON.parse(req.body.pos_details);
            // console.log(pos_array);

            var pos_data = {
                retailer: req.body.retailer,
                created_by: req.body.created_by,
                active: 1,
                bucket: req.body.bucket,
                total_purchased: req.body.total_purchased,
                total_uploaded: req.body.total_uploaded,
                total_balance: req.body.total_balance,
                created_date: new Date(),
                modified_by: "",
                modified_date: "",
                pos_details: pos_array
            };
            // console.log(pos_data);
            var p1 = new pos(pos_data);
            p1.save(function (req1) {
                mongoose.connection.close(function () {
                    console.log('  Mongoose connection disconnected save ');
                });
                //send_to_response("pos details saved", res);
                res.send('pos details saved');
            });
        }
    });


    counter_var--;
    logger.info("End Insert Pos Details For Bacth ProcessCounter Value" + counter_var);
    //console.log('Final Out');
    // send_to_response("pos details saved", res);
};

// End

exports.updating_si_status = function (req, res) {
    counter_var--;
    logger.info("End Insert Pos Details For Bacth ProcessCounter Value" + counter_var);
    sales_registry.get_invoice_date_of_company(req.body.company)
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('  Mongoose connection disconnected get_invoice_date_of_company ');
                });
                // console.log(results);
                var flag = 0;
                for (var i = 0; i < results.length; i++) {
                    var date = new Date(results[i].invoice_date);
                    var month = date.getMonth() + 1;
                    // console.log(month);
                    if (month < 6) {
                        flag = 1;
                        // console.log("Invoice after May");
                        break;
                    }
                }

                if (flag == 1) {
                    //  console.log("Inside Flag 1");
                    var conn = mysql.get_mysql_connection();
                    var sql = "update extra_customer_info set db_status = 3 where company_name = '" + req.body.company + "'";
                    conn.query(sql,
                        function (err, rows) {
                            if (err) {
                                logger.info(err);
                                res.end;
                            }
                            conn.end();
                            conn = null;

                            //logger.info("Update Complete");
                            // res.send(rows);
                        });

                    send_to_response("Updated", res);

                } else {
                    send_to_response("Invoice month before June", res);
                }
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_invoice_date_of_company ');
                });
                res.send(err);
            })
    counter_var++;

    logger.info("Get All Submissions Counter Value" + counter_var);
};

//*************************** END ********************************//

//***************************Seep Reporting **********************//

exports.disti_wise_sku_details = function (req, res) {
    var test = [];
    var t = {};
    test.push(t);
    counter_var++;

    logger.info("Disti Wise SKu  Counter Value" + counter_var);
    sku.get_all_sku_master()
        .then(
            function (results) {

                var sku_list = results;

                async.map(test, get_distiwise_sku_detail, function (err, r) {

                    // console.log('List OF SKU');
                    // console.log(r[0][0]);
                    if (r[0][0].length > 0) {
                        var disti_data = r[0][0];
                        // console.log('month data');
                        // console.log(month_data);
                        if (disti_data.length > 0) {
                            _.each(disti_data, function (element, index, list) {
                                var sku_data = _.where(results, {sku: element.sku});
                                // console.log('inside async details');
                                // console.log(pos_upld_data);

                                if (sku_data.length > 0) {
                                    // console.log('inside async1 details');
                                    element.brand = sku_data[0].brand;
                                    element.description = sku_data[0].description;

                                    // element = pos_upld_data[0];
                                }
                                //                            console.log('Inside Each');
                                //                            console.log(element);
                            });
                        }
                    }
                    send_to_response(disti_data, res);
                    //   console.log(r[0][0].length);
                    //  res.send(results);
                });
            },
            function (err) {
                res.send(err);
            }
        );
    counter_var--;

    logger.info("End Disti Wise SKu Counter Value" + counter_var);
};

exports.disti_wise_sku_details_order_by_desc = function (req, res) {
    var test = [];
    var t = {};
    test.push(t);
    counter_var++;

    logger.info("Get disti Wise  SKu Details Counter Value" + counter_var);
    sku.get_all_sku_master()
        .then(
            function (results) {
                mongoose.connection.close(function () {
                    console.log('   Mongoose connection disconnected get_all_sku_master ');
                });
                var sku_list = results;

                async.map(test, get_distiwise_sku_detail_desc, function (err, r) {

                    // console.log('List OF SKU');
                    // console.log(r[0][0]);
                    if (r[0][0].length > 0) {
                        var disti_data = r[0][0];
                        // console.log('month data');
                        // console.log(month_data);
                        if (disti_data.length > 0) {
                            _.each(disti_data, function (element, index, list) {
                                var sku_data = _.where(results, {sku: element.sku});
                                // console.log('inside async details');
                                // console.log(pos_upld_data);

                                if (sku_data.length > 0) {
                                    // console.log('inside async1 details');
                                    element.brand = sku_data[0].brand;
                                    element.description = sku_data[0].description;

                                    // element = pos_upld_data[0];
                                }
                                //                            console.log('Inside Each');
                                //                            console.log(element);
                            });
                        }
                    }
                    send_to_response(disti_data, res);
                    //   console.log(r[0][0].length);
                    //  res.send(results);
                });
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log(' error  Mongoose connection disconnected get_all_sku_master ');
                });
                res.send(err);
            }
        );
    counter_var--;

    logger.info("End Disti Wise SKU Details  Counter Value" + counter_var);
};

exports.region_wise_sku_details = function (req, res) {
    var test = [];
    var t = {};
    test.push(t);
    counter_var++;

    logger.info("Get Region Wise SKu Counter Value" + counter_var);
    sku.get_all_sku_master()
        .then(
            function (results) {

                var sku_list = results;

                async.map(test, get_regionwise_sku_detail, function (err, r) {

                    //  console.log('List OF SKU');
                    // console.log(r[0][0]);
                    if (r[0][0].length > 0) {
                        var disti_data = r[0][0];
                        // console.log('month data');
                        // console.log(month_data);
                        if (disti_data.length > 0) {
                            _.each(disti_data, function (element, index, list) {
                                var sku_data = _.where(results, {sku: element.sku});
                                // console.log('inside async details');
                                // console.log(pos_upld_data);

                                if (sku_data.length > 0) {
                                    // console.log('inside async1 details');
                                    element.brand = sku_data[0].brand;
                                    element.description = sku_data[0].description;

                                    // element = pos_upld_data[0];
                                }
                                //                            console.log('Inside Each');
                                //                            console.log(element);
                            });
                        }
                    }
                    send_to_response(disti_data, res);
                    //   console.log(r[0][0].length);
                    //  res.send(results);
                });
            },
            function (err) {
                mongoose.connection.close(function () {
                    console.log('  error  Mongoose connection disconnected get_all_sku_master ');
                });
                res.send(err);
            }
        );
    counter_var--;

    logger.info(" End Region Wise Sku Details Counter Value" + counter_var);
};

//*******************************END******************************//

//*************************AWS START************************//

exports.get_upload_url = function (req, res) {
    aws.config.loadFromPath('config/aws.config.json');
    //    var s3 = new aws.S3({computeChecksums: true});
    var s3 = new aws.S3();
    var params = {
        Bucket: util.get_aws().bucket,
        Key: req.params.filename,
        Expires: util.get_aws().upload_link_expiry
    };
    var url = s3.getSignedUrl('putObject', params);
    res.send(url);
};

var upload_file = function () {
    var filename;
    var filesize;
    var filetype;
};
//** Function to save files to disk
function onFileSaveToDisk(fieldname, file, filename, encoding, res) {

    var done;
    var fstream = fs.createWriteStream(path.join('./upload', path.basename(filename)));
    file.once('end', function () {
        console.log(fieldname + '(' + filename + ') EOF');
    });
    fstream.once('close', function () {
        console.log(fieldname + '(' + filename + ') written to disk');
        //        var stream = fs.createReadStream('upload/' + filename)
        var s3client = knox.createClient({
            key: util.get_aws().AWS_KEY,
            secret: util.get_aws().AWS_SECRET,
            bucket: util.get_aws().bucket
        });
        var header = {'x-amz-acl': 'public-read'};

        console.log("I m Here 2");
        console.log(filename);

        var f = 'upload/' + filename;
        s3client.putFile(f, filename, {'x-amz-acl': 'public-read'},
            function (err, r) {
                if (err) {
                    console.log("Error PUTing file in S3:", err);
                }
                //                console.log(r);
                if (r.statusCode == 200) {
                    console.log(r.req.url);
                    console.log("S3 RESPONSE:", r.statusCode);
                    // ready to delete
                    fs.unlink(f, function (err) {
                        if (err) throw err;
                        console.log('successfully deleted ' + f);
                        res.send(r.req.url);
                        //return true;//res.send( r.url);
                    });
                }
            });
        //next();
    });
    console.log(fieldname + '(' + filename + ') start saving');
    file.pipe(fstream);
}

//exports.upload_file = function(req, res) {
//    res.send("https://chandler-schneider-2.s3.amazonaws.com/seep-G12P16lWt.png");
//}

exports.upload_file = function (req, res) {
    counter_var++;
    logger.info("Inside Upload File Counter Value" + counter_var);
    busboy = new Busboy({headers: req.headers});
    // console.log("Busboy");
    //  console.log(busboy);

    var infiles, outfiles, done;

    busboy.on('file', function (fieldname, file, filename, encoding) {
        // ++infiles;
        filename = "baya-" + shortid.generate() + "." + filename.substring(filename.length - 3, filename.length);
        filename = filename.replace(/\s+/g, '');
        //        console.log("Mydata-",filename);
        //        console.log("Mydatafield-",fieldname);
        //        console.log("Mydatafile-",file);

        // console.log("I m Here 1");
        console.log(filename);

        onFileSaveToDisk(fieldname, file, filename, encoding, res, function () {
            //        onFileSaveToS3(fieldname, file, filename, encoding, function() {
            //++outfiles;
            if (done) {
                // ACTUAL EXIT CONDITION
                console.log('All parts written to disk');
                //                request.reply({ msg: filename}).type('text/html');

            }
        });
    });

    busboy.once('end', function () {
        console.log('Done parsing form!');
        done = true;
    });
    req.pipe(busboy);
    counter_var--;
    logger.info("End Inside Upload File Counter Value" + counter_var);
};

//exports.upload_file= function(req, res) {
//    var s3Client = knox.createClient({
//        key: util.get_aws().AWS_KEY,
//        secret: util.get_aws().AWS_SECRET,
//        bucket: util.get_aws().bucket
//    });
//    var object = { testing: "1234567890" };
//    var string = JSON.stringify(object);
//    var req = s3Client.put('/test/obj1.json', {
//        'Content-Length': string.length,
//        'Content-Type': 'application/json'
//    });
//    req.on('response', function (res) {
//        if (200 == res.statusCode) {
//            console.log('saved to %s', req.url);
//            //res.send(req.url);
//        }
//        else {
//            console.log(req);
//
//            //res.send ("error");
//        }
//    });
//
//}
//exports.get_upload_url= function(req, res) {
//    var s3Client = knox.createClient({
//        key: util.get_aws().AWS_KEY,
//        secret: util.get_aws().AWS_SECRET,
//        bucket: util.get_aws().bucket
//    });
//    var expires = new Date();
//    expires.setMinutes(expires.getMinutes() + 10);
//    var url= s3Client.signedUrl(req.params.filename, expires);
//    res.send (url);
//};

//*************************AWS END************************//


// ******************* private helper functions ***********************
var send_to_response = function (results, res) {

    res.contentType('application/json');
    if (util.is_array(results)) {
        var arr = [];
        results.forEach(function (r) {
            arr.push(r)
        });
        res.send(arr);
    } else {

        res.send(results);
    }
};

var return_back = function (results) {
    var arr = [];
    results.forEach(function (claim) {
        arr.push(claim)
    });
    return arr;
};

var send_empty_recordset = function (res) {
    var response = {"message": "no result found", "length": 0};

    res.contentType('application/json');
    res.send(response);
};

// ***********************************************************************
//limit and skip functionality
//Below functions are to be used by every function to ensure
//limit and skip values are legal
//and no error is generated as a result.
//Every developer needs to use these if they are implementing
// ***********************************************************************
var sanitize_limit = function (req) {
    var limit = req.params.limit;
    if ((limit === undefined) || (limit < 0)) {
        limit = util.get_fetch_limit();
    }
    return limit;
};

var sanitize_skip = function (req) {
    var skip = req.params.skip;
    if ((skip === undefined) || (skip < 0)) {
        skip = 0;
    }
    return skip;
};

// ***********************************************************************


// ***************************Dashboard******************************************

exports.get_total_sale_branch = function (req, res) {
    var conn = mysql.get_mysql_connection();
    counter_var++;

    logger.info("Get Total Sale branch Counter Value" + counter_var);
    var sql = "select participant_id from program_commerce.participants_relation where parent_2_id = '" + req.params.parent_id + "';"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            //console.log("Trying: " + req.params.parent_id + " as params");
            //logger.info(" validate from mysql");
            // console.log(rows);
            res.send(rows);
        });
    counter_var--;

    logger.info("Get Total Sale Branch Counter Value" + counter_var);
};

//
//exports.post_real_total_sale_branch = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    sku_wise_point_history.post_real_total_sale_branch(req.body)
//        .then (
//        function (results){
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};


//exports.post_real_total_sale_branch_hq = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    sku_wise_point_history.post_real_total_sale_branch_hq(req.body)
//        .then (
//        function (results){
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};


exports.get_total_sale_branch_hq = function (req, res) {
    var conn = mysql.get_mysql_connection();
    counter_var++;

    logger.info("Get Total Sale Branch Counter Value" + counter_var);
    var sql = "select participant_id from program_commerce.participants_relation where parent_3_id = '" + req.params.parent_id + "';"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            //  console.log("Trying: " + req.params.parent_id + " as params");
            // logger.info(" validate from mysql");
            //  console.log(rows);
            res.send(rows);
        });
    counter_var--;

    logger.info("End Get Total Sale Branch Counter Value" + counter_var);
};


exports.get_total_sale_dealer_normal = function (req, res) {
    var conn = mysql.get_mysql_connection();
    counter_var++;

    logger.info("Get get total sale dealer normal Counter Value" + counter_var);
    var sql = "select participant_id from program_commerce.participants_relation where parent_4_id = '" + req.params.parent_id + "' and parent_4_type = '4';"
    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }
            conn.end();
            conn = null;

            // console.log("Trying: " + req.params.parent_id + " as params");
            //  logger.info(" validate from mysql");
            //console.log(rows);
            res.send(rows);
        });
    counter_var--;

    logger.info("End Get Total Sale Dealer Normal Counter Value" + counter_var);
};

exports.get_sku_master_update = function (req, res) {

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get get sku master update Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sku_master');
            // console.log(sku)

            var project = {
                "brand": 1,
                "sku": 1,
                "description": 1,
                "model": 1,
                "active": 1,
                "mrp": 1,
                "bluerewards": 1,
                "series": 1,
                "pow_status": 1
            };

            col_todo.find({}, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in sku master" + err);
                }
                else {
                    //logger.info("Sucessfully getting sku master" + result.length);
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End SKu master Counter Value" + counter_var);
    });
};

exports.update_sku_pow = function (req, res) {

    //We need to work with "MongoClient" interface in order to connect to a mongodb server.
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Update SKu pow Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        } else {
            //Database has been connected.
            //logger.info("Connection Established To MongoDB");

            logger.info("incoming body" + req.body);
            // util.send_to_response({msg: 'SKU  POW STATUS Updated Successfully'}, res);

            var col_todo = db.collection('pow_master');

            col_todo.insert(req.body, function (err, result) {
                if (err) {
                    logger.info("Error In Inserting Todo Data,err:" + err.message);
                    util.send_to_response({msg: 'Error In Inserting Todo Data', err: err}, res);
                    //Close connection
                    db.close();
                } else {
                    //logger.info("POW Master Added Successfully");
                    //Close connection
                    db.close();
                    util.send_to_response({msg: 'POW Master Added Successfully'}, res);
                }
            });
        }
        counter_var--;

        logger.info("Update Sku Pow Counter Value" + counter_var);
    })
};

exports.insert_pow_master = function (req, res) {

    //We need to work with "MongoClient" interface in order to connect to a mongodb server.
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Insert Pow master Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        } else {
            //Database has been connected.
            //logger.info("Connection Established To MongoDB");

            var col_todo = db.collection('pow_master');

            col_todo.insert(req.body, function (err, result) {
                if (err) {
                    logger.info("Error In Inserting Data,err:" + err.message);
                    util.send_to_response({msg: 'Error In Inserting Data', err: err}, res);
                    //Close connection
                    db.close();
                } else {
                    //logger.info("POW Master Added Successfully");
                    //Close connection
                    db.close();
                    util.send_to_response({msg: 'POW Master Added Successfully'}, res);
                }
            });
        }
        counter_var--;

        logger.info("End Pow master Counter Value" + counter_var);
    });
};

exports.get_pow_master = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Pow master Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('pow_master');
            // console.log(sku)
            var find_qry = {"active": 1};

            var project = {
                "sku": 1,
                "description": 1,
                "start_dt": 1,
                "end_dt": 1,
                "active": 1,
                "created_by": 1,
                "created_dt": 1
            };

            col_todo.find(find_qry, project).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    //logger.info("Sucessfully getting the part code in pow" + result.length);
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;

        logger.info("End Get Pow master Counter Value" + counter_var);
    });
};

exports.update_pow_master = function (req, res) {
    //We need to work with "MongoClient" interface in order to connect to a mongodb server.
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Update Pow Master Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        } else {
            // console.log("")
            // console.log(req.body);
            var sku_value = [];
            var update_sku_value = {};
            var update_sku_array = [];
            var col_todo = db.collection('pow_master');

            // logger.info("incomning body" + JSON.stringify(req.body));

            sku_value = req.body;
            for (var i = 0; i < sku_value.length; i++) {
                update_sku_value = sku_value[i];
                // console.log("")
                // console.log(update_sku_value);
                update_sku_array.push(update_sku_value)

            }
            var update_qry = {"sku": {$in: update_sku_array}};
            var set_qry = {$set: {active: 0}};

            col_todo.update(update_qry, set_qry, {multi: true}, function (err, result) {
                if (err) {
                    logger.info("Error In Inserting Todo Data,err:" + err.message);
                    util.send_to_response({msg: 'Error In Inserting Todo Data', err: err}, res);
                }
                else {
                    //logger.info("Update  Successfully" + result);
                    util.send_to_response({msg: 'POW MASTER  ACTIVE Updated Successfully'}, res);
                    /*    var col_todo = db.collection('sku_master');
                     var update_qry = {"sku": {$in: update_sku_array}}
                     var set_qry = {$set: {"pow_status": 0}}
                     col_todo.update(update_qry, set_qry, {multi: true}, function (err, result) {
                     if (err) {
                     logger.info("Error In Inserting Todo Data,err:" + err.message);
                     util.send_to_response({msg: 'Error In Inserting Todo Data', err: err}, res);
                     }
                     else {
                     logger.info("Update  Successfully" + result);
                     util.send_to_response({msg: 'SKU MASTER  POW Updated Successfully'}, res);
                     }

                     })*/
                }
            })
        }
        counter_var--;

        logger.info("End  Pow master Counter Value" + counter_var);
    })
};

exports.update_sku_pow_status_master = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Update SKU POW Master Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            // console.log("")
            // console.log(req.body);
            var sku_value = [];
            var update_sku_value = {};
            var update_sku_array = [];
            var col_todo = db.collection('sku_master');

            // logger.info("incomning body" + JSON.stringify(req.body));

            // console.log("")
            // console.log(req.body);
            sku_value = req.body;

            for (var i = 0; i < sku_value.length; i++) {
                update_sku_value = sku_value[i];
                // console.log("")
                // console.log(update_sku_value)

                update_sku_array.push(update_sku_value)

            }
            var update_qry = {"sku": {$in: update_sku_array}};
            var set_qry = {$set: {"pow_status": 0}};

            col_todo.update(update_qry, set_qry, {multi: true}, function (err, result) {
                if (err) {
                    logger.info("Error In UPdating Data,err:" + err.message);
                    util.send_to_response({msg: 'Error In Inserting Todo Data', err: err}, res);
                }
                else {
                    //logger.info("Update  Successfully" + result);
                    util.send_to_response({msg: 'SKU MASTER  POW Updated Successfully'}, res);
                }
            })
        }
        counter_var--;

        logger.info("End Pow Master Counter Value" + counter_var);
    })
};

/////////////////////////////////////////////////////////////////////////////////////////////

exports.get_top_retailer_user_wise = function (req, res) {
    //sales_registry.get_pts_of(req.body)
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Get Top Retailer User Wise" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var user_id = req.body.user_id;
            var user_type = req.body.user_type;
            var obj = {};
            if (user_type) {

                if (user_type == "DistiChamp") {
                    obj = {disti_champ_id: user_id};
                }
                if (user_type == "Distributor") {
                    obj = {distributor_id: user_id};
                }
                if (user_type == "RM") {
                    obj = {rm_code: user_id};
                }
                if (user_type == "Sales Head") {
                    obj = {sales_head_code: user_id};
                }
                if (user_type == "Company") {
                    obj = {};
                }
            }

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            obj
                        ]
                    }
                },
                {
                    $group: {
                        _id: {'user_id': "$user_id", 'company_name': "$company_name"},
                        total_invoice_amount: {$sum: "$invoice_amount"},
                        total_invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                    }
                },
                {
                    $project: {
                        _id: 0, company_name: '$_id.company_name', 'user_id': '$_id.user_id',
                        total_invoice_amount: '$total_invoice_amount',
                        total_invoice_amount_excl_vat: '$total_invoice_amount_excl_vat'
                    }
                },
                {$sort: {total_invoice_amount_excl_vat: -1}},
                {$limit: 5}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in getting top retailer" + err);
                }
                else {
                    //logger.info("Sucessfully getting top retailer" + result.length);
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;
        logger.info("End Get Top Retailer User Wise" + counter_var);
    });
};

exports.get_top_five_distributor_user_wise = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");

    counter_var++;
    logger.info("Get Top Five Distributor Wise" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var user_id = req.body.user_id;
            var user_type = req.body.user_type;
            var obj = {};

            if (user_type) {
                if (user_type == "RM") {
                    obj = {rm_code: user_id};
                }
                if (user_type == "Sales Head") {
                    obj = {sales_head_code: user_id};
                }
                if (user_type == "Company") {
                    obj = {};
                }
            }

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {status: 'Verified & Approved'},
                            obj
                        ]
                    }
                },
                {
                    $group: {
                        _id: {'distributor_code': "$distributor_code", 'distributor_name': "$distributor_name"},
                        total_invoice_amount: {$sum: "$invoice_amount"},
                        total_invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                    }
                },
                {
                    $project: {
                        _id: 0, distributor_name: '$_id.distributor_name', 'distributor_code': '$_id.distributor_code',
                        total_invoice_amount: '$total_invoice_amount',
                        total_invoice_excl_vat: '$total_invoice_amount_excl_vat'
                    }
                },
                {$sort: {total_invoice_excl_vat: -1}},
                {$limit: 5}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + result.length);
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }

        counter_var--;
        logger.info("End Get Top Five Distributor Wise" + counter_var);
    });
};

exports.top_sku_user_wise = function (req, res) {
    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;
    logger.info("Top SKU User Wise Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)

            var user_id = req.body.user_id;
            var user_type = req.body.user_type;
            var obj = {};

            if (user_type) {
                if (user_type == "DistiChamp") {
                    obj = {disti_champ_id: user_id};
                }
                if (user_type == "Distributor") {
                    obj = {distributor_id: user_id};
                }
                if (user_type == "RM") {
                    obj = {rm_code: user_id};
                }
                if (user_type == "Sales Head") {
                    obj = {sales_head_code: user_id};
                }
                if (user_type == "Company") {
                    obj = {};
                }
            }

            var pipeline = [
                {$unwind: "$claim_details"},
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {'status': 'Verified & Approved'},
                            obj

                        ]
                    }
                },
                {$group: {_id: {'sku': "$claim_details.sku"}, total: {$sum: "$claim_details.quantity"}}},
                {$project: {_id: 0, sku: '$_id.sku', total_sku_qty: "$total"}},
                {$sort: {total_sku_qty: -1}},
                {$limit: 5}
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in checking part code in product of the week" + err);
                }
                else {
                    logger.info("Sucessfully getting the part code in pow" + result.length);
                    if (result.length > 0) {
                        util.send_to_response(result, res)
                    }
                }
            })
        }
        counter_var--;
        logger.info(" End Top SKU User Wise Counter Value" + counter_var);
    });

};

exports.getCompanySalesDetails = function (req, res) {

    //console.log(req.body.email_id);

    var responseArray = [];


    var conn = mysql.get_mysql_connection();

    //var sql = "call usp_GetChild('" + req.body.email_id + "')";
    // console.log('req.body.email_id');
    // console.log(req.body.email_id);
    var sql = "CALL `usp_GetChild`('" + req.body.email_id + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }


            //  console.log('rows');
            //  console.log(rows);

            conn.end();
            conn = null;

            responseArray.push({sql_result: rows[0]});

            //res.send(rows);

            if (rows && rows.length > 0) {
                var MongoClient = mongodb.MongoClient;
                var url = util.get_connection_string("mongo");

                MongoClient.connect(url, function (err, db) {
                    if (err) {
                        logger.info("Unable to connect to the mongoDB server. Error:" + err);
                        //Close connection
                        db.close();
                    }
                    else {
                        //logger.info("Connection Established To MongoDB");
                        var col_todo = db.collection('sales_registry');
                        // console.log(sku)

                        var pipeline = [
                            {
                                $match: {
                                    $and: [
                                        {active: 1},
                                        {status: 'Verified & Approved'}
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: {'sales_head_name': "$sales_head_name", 'sales_head_code': "$sales_head_code"},
                                    total_invoice_amount: {$sum: "$invoice_amount"},
                                    invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                                }
                            },
                            {
                                $project: {
                                    _id: 0, name: '$_id.sales_head_name', 'email_id': '$_id.sales_head_code',
                                    total_invoice_amount: '$total_invoice_amount',
                                    invoice_amount_excl_vat: '$invoice_amount_excl_vat'
                                }
                            }
                        ];
                        col_todo.aggregate(pipeline).toArray(function (err, result) {
                            if (err) {
                                logger.info("Error in checking part code in product of the week" + err);
                            }
                            else {
                                logger.info("Sucessfully getting the part code in pow" + result.length);
                                responseArray.push({sales_result: result});

                                //  console.log("Hi: " + vpresults);
                                send_to_response(responseArray, res);
                            }
                        })
                    }
                });
            }
            counter_var++;

            logger.info("Get DistiWise SKu Details Counter Value" + counter_var);
        });
};

exports.sales_head_sales_details = function (req, res) {

    //console.log(req.body.email_id);
    counter_var++;

    logger.info("Get Sales Head Sales Details Counter Value" + counter_var);

    var responseArray = [];

    var conn = mysql.get_mysql_connection();

    // console.log('req.body.email_id');
    // console.log(req.body.email_id);

    var sql = "call usp_GetChild('" + req.body.email_id + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            //console.log('rows');
            //console.log(rows);

            conn.end();
            conn = null;

            responseArray.push({sql_result: rows[0]});

            //res.send(rows);

            if (rows && rows.length > 0) {

                var MongoClient = mongodb.MongoClient;
                var url = util.get_connection_string("mongo");

                MongoClient.connect(url, function (err, db) {
                    if (err) {
                        logger.info("Unable to connect to the mongoDB server. Error:" + err);
                        //Close connection
                        db.close();
                    }
                    else {
                        //logger.info("Connection Established To MongoDB");
                        var col_todo = db.collection('sales_registry');
                        // console.log(sku)
                        var email_id = req.body.email_id;

                        var pipeline = [
                            {
                                $match: {
                                    $and: [
                                        {active: 1},
                                        {sales_head_code: email_id},
                                        {status: 'Verified & Approved'}
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: {'rm_name': "$rm_name", 'rm_code': "$rm_code"},
                                    total_invoice_amount: {$sum: "$invoice_amount"},
                                    invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                                }
                            },
                            {
                                $project: {
                                    _id: 0, 'name': '$_id.rm_name', 'email_id': '$_id.rm_code',
                                    total_invoice_amount: '$total_invoice_amount',
                                    invoice_amount_excl_vat: '$invoice_amount_excl_vat'
                                }
                            }
                        ];

                        col_todo.aggregate(pipeline).toArray(function (err, result) {
                            if (err) {
                                logger.info("Error in checking part code in product of the week" + err);
                            }
                            else {
                                logger.info("Sucessfully getting the part code in pow" + result.length);
                                responseArray.push({sales_result: result});

                                //  console.log("Hi: " + vpresults);
                                send_to_response(responseArray, res);
                            }
                        })
                    }
                });
            }
            counter_var--;

            logger.info("End Sales Head Sale Details Counter Value" + counter_var);
        });
};

exports.rm_sales_details = function (req, res) {

    //console.log(req.body.email_id);

    var responseArray = [];
    counter_var++;

    logger.info("Get Rm Sales Details Counter Value" + counter_var);
    var conn = mysql.get_mysql_connection();

    var sql = "call usp_GetChild('" + req.body.email_id + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            //console.log('rows');
            //console.log(rows);

            conn.end();
            conn = null;

            responseArray.push({sql_result: rows[0]});

            //res.send(rows);

            if (rows && rows.length > 0) {
                var MongoClient = mongodb.MongoClient;
                var url = util.get_connection_string("mongo");

                MongoClient.connect(url, function (err, db) {
                    if (err) {
                        logger.info("Unable to connect to the mongoDB server. Error:" + err);
                        //Close connection
                        db.close();
                    }
                    else {
                        //logger.info("Connection Established To MongoDB");
                        var col_todo = db.collection('sales_registry');
                        // console.log(sku)
                        var email_id = req.body.email_id;
                        var pipeline = [
                            {
                                $match: {
                                    $and: [
                                        {active: 1},
                                        {rm_code: email_id},
                                        {status: 'Verified & Approved'}
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: {'distributor_name': "$distributor_name", 'distributor_id': "$distributor_id"},
                                    total_invoice_amount: {$sum: "$invoice_amount"},
                                    invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                                }
                            },
                            {
                                $project: {
                                    _id: 0, 'name': '$_id.distributor_name', 'email_id': '$_id.distributor_id',
                                    total_invoice_amount: '$total_invoice_amount',
                                    invoice_amount_excl_vat: '$invoice_amount_excl_vat'
                                }
                            }
                        ];

                        col_todo.aggregate(pipeline).toArray(function (err, shresults) {
                            if (err) {
                                logger.info("Error in checking part code in product of the week" + err);
                            }
                            else {
                                logger.info("Sucessfully getting the part code in pow" + shresults.length);
                                responseArray.push({sales_result: shresults});

                                //  console.log("Hi: " + vpresults);
                                send_to_response(responseArray, res);
                            }
                        })
                    }
                });
            }
            counter_var--;

            logger.info("End Rm Sales Details Counter Value" + counter_var);
        });
};

exports.distributor_sales_details = function (req, res) {

    //console.log(req.body.email_id);
    counter_var++;

    logger.info("Get Distributor  Sales Details Counter Value" + counter_var);
    var responseArray = [];

    var conn = mysql.get_mysql_connection();

    var sql = "call usp_GetChild('" + req.body.email_id + "')";

    conn.query(sql,
        function (err, rows) {
            if (err) {
                logger.info(err);
                res.end;
            }

            //console.log('rows');
            //console.log(rows);

            conn.end();
            conn = null;

            responseArray.push({sql_result: rows[0]});

            //res.send(rows);

            if (rows && rows.length > 0) {

                var MongoClient = mongodb.MongoClient;
                var url = util.get_connection_string("mongo");

                MongoClient.connect(url, function (err, db) {
                    if (err) {
                        logger.info("Unable to connect to the mongoDB server. Error:" + err);
                        //Close connection
                        db.close();
                    }
                    else {
                        //logger.info("Connection Established To MongoDB");
                        var col_todo = db.collection('sales_registry');
                        // console.log(sku)
                        var email_id = req.body.email_id;
                        var pipeline = [
                            {
                                $match: {
                                    $and: [
                                        {active: 1},
                                        {distributor_id: email_id},
                                        {status: 'Verified & Approved'}
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: {'disti_champ_id': "$disti_champ_id", 'disti_champ_name': "$disti_champ_name"},
                                    total_invoice_amount: {$sum: "$invoice_amount"},
                                    invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                                }
                            },
                            {
                                $project: {
                                    _id: 0, 'email_id': '$_id.disti_champ_id', 'name': '$_id.disti_champ_name',
                                    total_invoice_amount: '$total_invoice_amount',
                                    invoice_amount_excl_vat: '$invoice_amount_excl_vat'
                                }
                            }
                        ];

                        col_todo.aggregate(pipeline).toArray(function (err, shresults) {
                            if (err) {
                                logger.info("Error in checking part code in product of the week" + err);
                            }
                            else {
                                logger.info("Sucessfully getting the part code in pow" + shresults.length);
                                responseArray.push({sales_result: shresults});

                                //  console.log("Hi: " + vpresults);
                                send_to_response(responseArray, res);
                            }
                        })
                    }
                });

            }
            counter_var--;

            logger.info("Get Distributor Sales Details Counter Value" + counter_var);
        });
};

// exports.distributor_sales_details = function (req, res) {
//
//     //console.log(req.body.email_id);
//
//     var responseArray = [];
//
//     sales_registry.distributor_sales_details(req.body.email_id)
//         .then(
//             function (shresults) {
//
//                 responseArray.push({sales_result: shresults});
//
//                 //  console.log("Hi: " + vpresults);
//                 send_to_response(responseArray, res);
//             },
//             function (err) {
//                 res.send(err);
//             }
//         );
// };

exports.disti_champ_sales_details = function (req, res) {

    //console.log(req.body.email_id);

    var responseArray = [];

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get Disti Champ Sales Details Counter Value" + counter_var);
    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var email_id = req.body.email_id;
            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {disti_champ_id: email_id},
                            {status: 'Verified & Approved'}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {'user_id': "$user_id", 'retailer_name': "$retailer_name"},
                        total_invoice_amount: {$sum: "$invoice_amount"},
                        invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                    }
                },
                {
                    $project: {
                        _id: 0, 'user_id': '$_id.user_id', 'retailer_name': '$_id.retailer_name',
                        total_invoice_amount: '$total_invoice_amount',
                        invoice_amount_excl_vat: '$invoice_amount_excl_vat'
                    }
                }
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in disit champ sales" + err);
                }
                else {
                    //logger.info("Sucessfully disit champ sales" + result.length);
                    responseArray.push({sales_result: result});

                    //  console.log("Hi: " + vpresults);
                    send_to_response(responseArray, res);
                }
            })
        }
        counter_var--;

        logger.info("Get Disti Champ Details Counter Value" + counter_var);
    });
};

exports.retailer_sales_details = function (req, res) {

    //console.log(req.body.email_id);

    var responseArray = [];
    var responseArray = [];

    var MongoClient = mongodb.MongoClient;
    var url = util.get_connection_string("mongo");
    counter_var++;

    logger.info("Get  Retailer Sales  Details Counter Value" + counter_var);

    MongoClient.connect(url, function (err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
            //Close connection
            db.close();
        }
        else {
            //logger.info("Connection Established To MongoDB");
            var col_todo = db.collection('sales_registry');
            // console.log(sku)
            var email_id = req.body.email_id;

            var pipeline = [
                {
                    $match: {
                        $and: [
                            {active: 1},
                            {user_id: email_id},
                            {status: 'Verified & Approved'}
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            'user_id': "$user_id",
                            'retailer_name': "$retailer_name",
                            "invoice_number": "$invoice_number",
                            "supporting_doc": "$supporting_doc"
                        },
                        total_invoice_amount: {$sum: "$invoice_amount"},
                        invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
                    }
                },
                {
                    $project: {
                        _id: 0, 'user_id': '$_id.user_id', 'retailer_name': '$_id.retailer_name',
                        invoice_number: "$_id.invoice_number",
                        supporting_doc: "$_id.supporting_doc",
                        total_invoice_amount: '$total_invoice_amount',
                        invoice_amount_excl_vat: '$invoice_amount_excl_vat'
                    }
                }
            ];

            col_todo.aggregate(pipeline).toArray(function (err, result) {
                if (err) {
                    logger.info("Error in retailer sales details" + err);
                }
                else {
                    //logger.info("Sucessfully retailer sales details" + result.length);
                    responseArray.push({sales_result: result});

                    //  console.log("Hi: " + vpresults);
                    send_to_response(responseArray, res);
                }
            })
        }
        counter_var--;

        logger.info(" End Get Retailer Sales  Details Counter Value" + counter_var);
    });
};

/////////////////////////////////////////////////////////////////////////////////////////////
//exports.post_real_total_sale_dealer_normal = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    sku_wise_point_history.post_real_total_sale_dealer_normal(req.body)
//        .then (
//        function (results){
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};


//exports.get_sku_of = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    sku_wise_point_history.get_sku_of(req.body)
//        .then (
//        function (results){
//            console.log("Hi: "+results);
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};

//exports.get_pts_of = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    //sales_registry.get_pts_of(req.body)
//    sku_wise_point_history.get_pts_of(req.body)
//        .then (
//        function (results){
//            console.log("Hi: "+results);
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};

//exports.get_ptsdetails = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    //sales_registry.get_ptsdetails(req.params.participantid)
//    sku_wise_point_history.get_ptsdetails(req.params.participantid)
//        .then (
//        function (results){
//            console.log("Hi: "+results);
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};


//exports.get_saledetails = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    //sales_registry.get_saledetails(req.params.participantid)
//    sku_wise_point_history.get_saledetails(req.params.participantid)
//        .then (
//        function (results){
//            console.log("Hi: "+results);
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};


//exports.get_dets_nutshell = function (req, res) {
//    console.log("Hello");
//    console.log(req.body);
//    //sales_registry.get_dets_nutshell(req.body)
//    sku_wise_point_history.get_dets_nutshell(req.body)
//        .then (
//        function (results){
//            console.log("Hcddadada: "+results);
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};


//exports.get_participant_details = function (req, res) {
//    sku_wise_point_history.get_participant_details(req.params.participantid)
//        .then (
//        function (results){
//            console.log("Hi: "+results);
//            send_to_response(results, res);
//        },
//        function (err){
//            res.send(err);
//        }
//
//    );
//};

// ********************End ***************************

this.renew_token = function (req, res, next) {
    var token_data = token.verify(req.headers.token);
    var token_renw = "";
    if (token_data) {
        token_renw = token.assign(token_data);
    }
    util.send_to_response(token_renw, res);

};

// ******************* private helper functions ***********************
