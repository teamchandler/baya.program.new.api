//<editor-fold desc="Variable Declaration">

var error_handler = require('./error').errorHandler,
    master_handler = require('./master'),
    express = require('express'),
    util = require('../util.js'),
    repo = require('../repo')
    ;


var logger = util.get_logger("server");
// define routers (new in Express 4.0)
var ping_route = express.Router(),
    master_route = express.Router(),
    transaction_route = express.Router(),
    help_route = express.Router(),
    login_route = express.Router()
    ;

var token = require('../token');
var login = require('./login');


//</editor-fold>


module.exports = exports = function (app, corsOptions) {
    //<editor-fold desc="Fast Phase">
    // protect any route starting with "master" with validation
    app.all("/master/*", authenticate, function (req, res, next) {
        next(); // if the middleware allowed us to get here,
        // just move on to the next route handler
    });

    // implement ping_route actions
    // This is specific way to inject code when this route is called
    // This executes for any ping route.
    // Similarly we can (and should) implement same for every route
    ping_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "ping", "PING"));
        // continue doing what we were doing and go to the route
        next();
    });
    ping_route.get('/', function (req, res) {
        res.send({'error': 0, 'msg': 'audit endpoints ready for data capture'});
    });
    ping_route.get('/check/mongo', function (req, res) {
        res.send({'error': 0, 'msg': 'Code for checking Mongo connection will be implemented here'});
    });
    ping_route.post('/check/post', function (req, res) {
        res.send('Checking Post Method' + req.body);
    });
    app.use('/ping', ping_route);
    // end ping route

    var multipart = require('connect-multiparty');
    var multipartMiddleware = multipart();
    // master route implementation
    master_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "master", "MASTER"));
        // continue doing what we were doing and go to the route
        next();
    });
    //</editor-fold>

    //<editor-fold desc="Add Code for new Collection">

    var user_master = master_handler.user_master;
//    var user_security_master = master_handler.user_security_master;
//    var proj_master = master_handler.proj_master;
//    var req_master = master_handler.req_master;
//    var testcase = master_handler.testcase_master;
//    var testscenario = master_handler.testscenario_master;
//    var testexecution = master_handler.testexecution_master;
    var api_master = master_handler.api_master;
    //<editor-fold desc="renew token 20141112 srijan">
    master_route.post('/renew/token', function (req, res) {
        api_master.renew_token(req, res);
    });
    //</editor-fold>

    //<editor-fold desc="user 20141106 srijan">

    help_route.get('/user', function (req, res) {
        user_master.help(req, res);
    });

    // Add Invoice

//    master_route.post('/invoice/schneider/add', function(req, res){
//        api_master.add_invoice(req,res);
//    });

    master_route.get('c', function (req, res) {
        api_master.get_retailer_list(req, res);
    });


    ////


    //*********************** Program Management**********************//
    master_route.post('/retailer_si_by_typeid', function (req, res) {
        api_master.get_retailer_si_by_typeid(req, res);
    });

    master_route.post('/si_by_retailerid', function (req, res) {
        api_master.get_si_by_retailerid(req, res);
    });

    master_route.get('/user/company', function (req, res) {
        api_master.get_company(req, res);
    });
    master_route.get('/user/details/:company', function (req, res) {
        api_master.get_user_details(req, res);
    });
    master_route.get('/userinfo/by/user/:email_id', function (req, res) {
        api_master.get_user_info(req, res);
    });
    master_route.get('/order/detail/by/email/:user_id', function (req, res) {
        api_master.get_my_order_details(req, res);
    });
    master_route.get('/get/shipping_details/by/order_id/:order_id', function (req, res) {
        api_master.get_shipping_detail_by_order_id(req, res);
    });
    master_route.get('/get/order_information/by/order_id/:order_id', function (req, res) {
        api_master.get_order_information(req, res);
    });
    master_route.get('/program/list/all', function (req, res) {
        api_master.get_all_program(req, res);
    });
    master_route.get('/program/by/id/:id', function (req, res) {
        api_master.get_program_by_id(req, res);
    });
    master_route.get('/program/list/company_region/:company/:region', function (req, res) {
        api_master.get_program_by_company_region(req, res);
    });

    master_route.post('/user/basic/info/update', function (req, res) {
        api_master.update_user_basic_info(req, res);
    });

    master_route.post('/user/basic/address/update', function (req, res) {
        api_master.update_user_address(req, res);
    });
    master_route.post('/program/save', function (req, res) {
        api_master.add_program(req, res);
    });

    //  app.get('/store/my_order_details/email/:user_id',user.get_my_order_details);

    //****//


// ********* Retailer Services ********************

//    master_route.get('/retailer/list/:p_id', function(req, res){
//        api_master.get_all_distributor(req,res);
//    });


    master_route.get('/retailer/list', function (req, res) {
        api_master.get_retailer_list(req, res);
    });

    login_route.post('/get/invoice/list', function (req, res) {
        // console.log('Invoice');
        api_master.get_invoice_list(req, res);
    });


    // ********* Workflow Services ********************


    //*****************New APIs start for seep_2016*****************//

    //get distributor list
    master_route.get('/distributor/list/', function (req, res) {
        api_master.get_distributor_list(req, res);
    });

    //*****************New APIs end for seep_2016*******************//


    // **************** Dashboard Distributor****************************

    master_route.get('/program/get/totalsale/amount/:user_id', function (req, res) {
        api_master.get_total_sale_verified(req, res);
    });

    master_route.get('/program/get/points_pending_for_paymt_appv/:user_id', function (req, res) {
        api_master.get_points_pending_for_paymt_appv(req, res);
    });

    master_route.get('/program/get/lostpoint/:user_id', function (req, res) {
        api_master.get_lostpoint(req, res);
    });

    master_route.get('/program/branch/total/sale/:parent_id', function (req, res) {
        api_master.get_total_sale_branch(req, res);
    });
    master_route.post('/program/real/branch/total/sale', function (req, res) {
        api_master.post_real_total_sale_branch(req, res);
    });

    master_route.post('/program/sku/toper', function (req, res) {
        api_master.get_sku_of(req, res);
    });
    master_route.post('/program/pts/acc', function (req, res) {
        api_master.get_pts_of(req, res);
    });

    // *********************** End Dashboard Distributor ****************


    // **************** Dashboard Dealer HQ****************************

    master_route.get('/program/branch/total/sale/hq/:parent_id', function (req, res) {
        api_master.get_total_sale_branch_hq(req, res);
    });

    master_route.post('/program/real/branch/total/sale/hq', function (req, res) {
        api_master.post_real_total_sale_branch_hq(req, res);
    });


    // *********************** End Dashboard Dealer HQ****************

    // **************** Dashboard Dealer Normal****************************

    master_route.get('/program/dealer/normal/sale/:parent_id', function (req, res) {
        api_master.get_total_sale_dealer_normal(req, res);
    });
    master_route.post('/program/real/dealer/normal/sale', function (req, res) {
        api_master.post_real_total_sale_dealer_normal(req, res);
    });

    master_route.get('/program/pts/details/:participantid', function (req, res) {
        api_master.get_ptsdetails(req, res);
    });

    master_route.get('/program/get/sale/amount/details/:participantid', function (req, res) {
        api_master.get_saledetails(req, res);
    });


    // **************** End Dashboard Dealer Normal****************************


    //**************************** Distributor Purpose*************************************//

    master_route.post('/program/details/pager', function (req, res) {
        api_master.get_dets_nutshell(req, res);
    });
    master_route.get('/program/parti/details/:participantid', function (req, res) {
        api_master.get_participant_details(req, res);
    });


    //*****************************************************************//


    // ******* SKU Transaction ************ //

//    master_route.post('/sku/quantity/update', function(req, res){
//        api_master.update_sku_quantity(req,res);
//    });

    master_route.get('/search/sku/availability/:sku', function (req, res) {
        api_master.search_availability_by_sku(req, res);
    });

    master_route.get('/get/sku/by/name/:name', function (req, res) {
        api_master.get_sku_by_name(req, res);
    });


    // ********* Claim Services ********************

    // GET Method
    master_route.get('/top/si', function (req, res) {
        api_master.get_top_si(req, res);
    });
    master_route.get('/top/retailer/by/points/cr', function (req, res) {
        api_master.get_top_retailer_by_points_cr(req, res);
    });
    master_route.get('/top/si/by/region', function (req, res) {
        api_master.get_top_si_by_region(req, res);
    });
    master_route.get('/special/program/qualifier/si', function (req, res) {
        api_master.special_program_qualifier(req, res);
    });
    master_route.get('/normal/program/qualifier/si', function (req, res) {
        api_master.normal_program_qualifier(req, res);
    });
    master_route.get('/si/by/region', function (req, res) {
        api_master.get_si_by_region(req, res);
    });
    master_route.get('/details/by/user/:user', function (req, res) {
        api_master.get_details_by_user(req, res);
    });
    master_route.get('/invoice/detail/by/id/:claim_id', function (req, res) {
        api_master.get_invoice_by_number(req, res);
    });
//    master_route.get('/get/invoice/detail/by/user_id/:inv_date/:inv_no/:inv_amount', function(req, res){
//        api_master.get_invoice_by_user_id(req,res);
//    });

    master_route.post('/get/invoice/detail/by/user_id', function (req, res) {
        api_master.get_invoice_by_user_id(req, res);
    });

    master_route.get('/get/invoice/details/by/user_id/:user_id', function (req, res) {
        api_master.get_invoice_details_by_user_id(req, res);
    });

    master_route.get('/get/invoice/detail/by/user/:user', function (req, res) {
        api_master.get_invoice_by_name(req, res);
    });
    /*New api */
    master_route.get('/get/invoice/detail/by/userid/invno/:user/:inv', function (req, res) {
        api_master.get_invoice_details_by_name_inv_no(req, res);
    });

    /* End */

    master_route.get('/get/invoice/detail/by/retailer/:retailer', function (req, res) {
        api_master.get_invoice_by_retailer(req, res);
    });
    master_route.get('/get/user_id_wise_invoice_amount/:user_id', function (req, res) {
        api_master.calc_user_id_wise_invoice_amount(req, res);
    });
    master_route.get('/top/distributor/by/invoice_amount', function (req, res) {
        api_master.top_distributor_invoice_amount(req, res);
    });
    master_route.get('/top/sku/', function (req, res) {
        api_master.top_sku(req, res);
    });
    master_route.get('/search/by/sku/:sku', function (req, res) {
        api_master.search_by_sku(req, res);
    });
    master_route.get('/search/sku/list/by/sku/:sku', function (req, res) {
        api_master.search_sku_list(req, res);
    });
    master_route.get('/search/invoice/by/:status/:company_name', function (req, res) {
        api_master.get_invoice_by_status_comp_name(req, res);
    });
    master_route.get('/claim/list/:user', function (req, res) {
        api_master.get_claim_by_user(req, res);
    });
    master_route.get('/claim/submission_total/:user', function (req, res) {
        api_master.get_total_submitted_value(req, res);
    });
    master_route.get('/claim/submissions/all/:user/:limit', function (req, res) {
        api_master.get_all_submissions(req, res);
    });
//    master_route.get('/early/bird/winner/:program_type', function(req, res){
//        api_master.get_early_bird_winners(req,res);
//    });

    login_route.post('/early/bird/winner', function (req, res) {
        api_master.get_early_bird_winners(req, res);
    });

    login_route.get('/points/credited/users/:program_type', function (req, res) {
        api_master.get_points_credited_users(req, res);
    });
    login_route.get('/all/schneider/users/:program_type/:company/:page_no', function (req, res) {
        api_master.get_all_schneider_users(req, res);
    });
    master_route.get('/all/points/eligible/users', function (req, res) {
        console.log('I Am Here 1..!')
        api_master.get_all_points_eligible_users(req, res);
    });

    // For Seep 2015 Reporting

    login_route.get('/reporting/soln/all/points/eligible/users', function (req, res) {

        console.log('I Am Here 2..!')

        api_master.new_get_all_points_eligible_users(req, res);

    });

    // End

    master_route.get('/details/totalsale/by/user/:user', function (req, res) {
        // api_master.get_user_details_for_zone_address(req,res);
        api_master.get_details_total_sale_by_user(req, res);
    });

    master_route.get('/details/zoneaddress/by/user/:user', function (req, res) {
        console.log('I Am Here 3..!')
        api_master.get_all_points_eligible_users(req, res);
    });

    master_route.get('/details/goal/value/of/user/:user', function (req, res) {
        api_master.get_user_goal_value(req, res);
    });

    master_route.get('/redeemed/points/of/user/:user', function (req, res) {
        api_master.get_user_redeemed_points(req, res);
    });

    master_route.get('/invoice/month/proceedings/of/user/:user/:current_date', function (req, res) {
        api_master.get_user_proceedings_invoice_current_month(req, res);
    });

    master_route.get('/si/by/region/dashboard/:regionName', function (req, res) {
        api_master.get_user_si_region_dashboard(req, res);
    });
    master_route.get('/si/by/allIndia/dashboard', function (req, res) {
        api_master.get_user_si_allIndia_dashboard(req, res);
    });

    //    app.get('/si/by/normal/points',sales_registry.get_si_by_region);


    // POST Method

    master_route.post('/invoice/schneider/add', function (req, res) {
        logger.info("Invoice Add Call In Index.js");
        api_master.add_invoice(req, res);
    });

    // CSV batch upload api For VST

    master_route.post('/invoice/csv/upload', function (req, res) {
        api_master.add_invoice_csv_upload(req, res);
    });

    // End


    // New Payment API Added

    login_route.post('/insert/retailer/payment', function (req, res) {
        api_master.add_payment_details(req, res);
    });

    login_route.post('/get/payment/details', function (req, res) {
        api_master.get_payment_details(req, res);
    });

    login_route.post('/get/invoice_wise/details', function (req, res) {
        api_master.get_details_by_invoice_number(req, res);
    });

    login_route.post('/get/company/name', function (req, res) {
        api_master.get_all_company_name(req, res);
    });

    login_route.post('/insert/retailer/payment/from_csv', function (req, res) {
        api_master.add_payment_details_from_csv(req, res);
    });


    // End


    master_route.post('/invoice/schneider/edit', function (req, res) {
        api_master.update_invoice(req, res);
    });
    master_route.post('/invoice/claim/details/update', function (req, res) {
        api_master.update_invoice_claim(req, res);
    });
    master_route.post('/search/duplicate/invoice/list', function (req, res) {
        api_master.search_duplicate_invoice_list(req, res);
    });
    master_route.post('/invoice/claim/add/sku', function (req, res) {
        api_master.update_claim(req, res);
    });
    master_route.post('/search/by/part/code', function (req, res) {
        api_master.search_by_part_code(req, res);
    });

    //shalini api to push points



    master_route.post('/get/data/push/points', function (req, res) {
        api_master.get_data_points_push(req, res);
    });


    master_route.post('/insert/user/points', function (req, res) {
        api_master.insert_user_points(req, res);
    });


    master_route.post('/insert/disti/points', function (req, res) {
        api_master.insert_disti_points(req, res);
    });
    master_route.post('/claim/add', function (req, res) {
        api_master.add_claim(req, res);
    });
    master_route.post('/override/sku/add', function (req, res) {
        api_master.add_override_sku(req, res);
    });
    master_route.post('/goal/set/by/user', function (req, res) {
        api_master.posting_goal_value(req, res);
    });
    master_route.post('/goal/update/by/user', function (req, res) {
        api_master.updating_goal_value(req, res);
    });

    // New (POS VS Upload)

    master_route.post('/get/month_wise/invoice_amount', function (req, res) {
        api_master.get_month_wise_invoice_amount(req, res);
    });


    master_route.post('/get/all_month_details', function (req, res) {
        api_master.get_all_month_details(req, res);
    });

    master_route.post('/insert_pos_details', function (req, res) {
        api_master.insert_pos_details(req, res);
    });

    ping_route.post('/insert_pos_details_for_batchprocess', function (req, res) {
        api_master.insert_pos_details_for_batchprocess(req, res);
    });


    ping_route.post('/insert_pos_details_for_batchprocess_by_month', function (req, res) {
        api_master.insert_pos_details_for_batchprocess_by_month(req, res);
    });


    // ********* Claim Services ********************


    // ********* SKU Services ********************

    master_route.post('/save/sku/details', function (req, res) {
        api_master.add_sku_details(req, res);
    });

    master_route.get('/sku/list/:company/:region_id', function (req, res) {
        api_master.get_sku_list(req, res);
    });

    //master_route.post('/sku/list/all', function (req, res) {
      //  api_master.get_all_sku_master(req, res);
    //});

  /*  master_route.post('/sku/list/search/key', function (req, res) {
        api_master.get_sku_by_search_key(req, res);
    });*/

    //Get Monthly SKU wise sale
    login_route.get('/monthly/sku/wise/sale', function (req, res) {
        api_master.get_monthly_sku_wise_sale(req, res);
    });




    //app.get('/sku/list/detailed/:company/:region_id', sku.get_sku_list_detailed());
    // ********* SKU Services ********************


    // ********* User Services ********************
    master_route.get('/user/company', function (req, res) {
        api_master.get_company(req, res);
    });
    master_route.get('/get/user/company/name/by/:company/:srch_key', function (req, res) {
        api_master.get_user_company_name(req, res);
    });

    master_route.get('/get/user/company/name/new/by/:company/:srch_key', function (req, res) {
        api_master.get_user_company_name_new(req, res);
    });

    master_route.post('/user/level/save', function (req, res) {
        api_master.save_user(req, res);
    });
    master_route.post('/user/change/password', function (req, res) {
        api_master.change_password(req, res);
    });
    master_route.post('/user/terms/condition/update', function (req, res) {
        api_master.terms_condition_update(req, res);
    });

    ping_route.post('/update_SI_status', function (req, res) {
        api_master.updating_si_status(req, res);
    });


    //*****************************************//

    // Seep Reporting Dashboard

    ping_route.post('/get_disti_wise_sku_details', function (req, res) {
        api_master.disti_wise_sku_details(req, res);
    });

    ping_route.post('/get_disti_wise_sku_details_order_by_desc', function (req, res) {
        api_master.disti_wise_sku_details_order_by_desc(req, res);
    });

    ping_route.post('/get_region_wise_sku_count', function (req, res) {
        api_master.region_wise_sku_details(req, res);
    });


    // End

    // Done By Souvick

    login_route.post('/get/invoice_details/by/invoice', function (req, res) {
        api_master.get_invoice_details_by_invoice_number(req, res);
    });

    // End


    //********** AWS *******************************

    master_route.get('/aws/get/upload_url/:filename', function (req, res) {
        api_master.get_upload_url(req, res);
    });

    master_route.post('/aws/upload/file', function (req, res) {
        api_master.upload_file(req, res);
    });
    master_route.post('/get/invoice/list/by/retailer', function (req, res) {
        api_master.get_inv_by_retailer(req, res);
    });

    //**********************************************

    //API to get the SKU master
    master_route.get('/get/sku/list/update', function (req, res) {
        api_master.get_sku_master_update(req, res);
    });

    master_route.post('/update/pow/sku', function (req, res) {
        api_master.update_sku_pow(req, res);
    });

    master_route.post('/insert/into/pow/master', function (req, res) {
        // console.log("inside to insert")
        api_master.insert_pow_master(req, res);
    });
    master_route.get('/get/pow/master', function (req, res) {
        api_master.get_pow_master(req, res);
    });
    master_route.post('/update/pow/master', function (req, res) {
        api_master.update_pow_master(req, res);
    });
    master_route.post('/update/pow/status', function (req, res) {
        api_master.update_sku_pow_status_master(req, res);
    });

    /////////////////////////////////////////////////////////////////////

    //Top 5 retailer user wise(total invoice uploaded)
    master_route.post('/top/retailer/user/wise', function (req, res) {
        api_master.get_top_retailer_user_wise(req, res);
    });

    //Top 5 distributor user wise(total invoice uploaded)
    master_route.post('/top/distributor/user/wise', function (req, res) {
        api_master.get_top_five_distributor_user_wise(req, res);
    });

    //Top 5 SKU user wise
    master_route.post('/top/sku/user/wise', function (req, res) {
        api_master.top_sku_user_wise(req, res);
    });

    master_route.post('/company/sales/details', function (req, res) {
        api_master.getCompanySalesDetails(req, res);
    });

    master_route.post('/sales/head/sales/details', function (req, res) {
        api_master.sales_head_sales_details(req, res);
    });

    master_route.post('/rm/sales/details', function (req, res) {
        api_master.rm_sales_details(req, res);
    });

    // master_route.post('/rm/sales/details', function (req, res) {
    //     api_master.rm_sales_details(req, res);
    // });

    master_route.post('/distributor/sales/details', function (req, res) {
        api_master.distributor_sales_details(req, res);
    });

    master_route.post('/disti/champ/sales/details', function (req, res) {
        api_master.disti_champ_sales_details(req, res);
    });

    master_route.post('/retailer/sales/details', function (req, res) {
        api_master.retailer_sales_details(req, res);
    });

    /////////////////////////////////////////////////////////////////////
    //</editor-fold>


    //</editor-fold>


    //<editor-fold desc="Last Phase">

    // implement help_route actions
    help_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "help", "HELP"));
        // continue doing what we were doing and go to the route
        next();
    });
    help_route.get('/', function (req, res) {
        var r_list = [];
        get_routes(ping_route.stack, r_list, "ping");
        get_routes(master_route.stack, r_list, "master");
        get_routes(help_route.stack, r_list, "help");
        res.send(r_list);
    });
    help_route.get('/show', function (req, res) {
        var r_list = [];
        get_routes(ping_route.stack, r_list, "ping");
        get_routes(master_route.stack, r_list, "master");
        get_routes(help_route.stack, r_list, "help");
        //res.send(r_list);
        console.log(r_list);
        res.render('show', {'routes': r_list});
    });
    app.use('/help', help_route);
    // end help route

    //implement login_route action
    login_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "login", "LOGIN"));
        // continue doing what we were doing and go to the route
        next();
    });
    //login validation for user
//    login_route.post('/validateuser', function(req, res){
//        login.validate_user(req,res);
//    });

    login_route.post('/by', function (req, res) {
        api_master.user_login(req, res);
    });


    app.use('/login', login_route);
    app.use('/help', help_route);
    app.use('/master', master_route);

    //end login_route

    // This will be refactored later for adding more features and making it more generic
    var get_routes = function (r, r_list, route_sub_system) {
        for (var i = 0; i < r.length; i++) {
            if (typeof r[i].route != "undefined") {

                r_list.push(
                    {
                        'path': "/" + route_sub_system + r[i].route.path,
                        'method': r[i].route.methods
                    }
                )
            }
        }
        return r_list;
    }

    // and it will validate based on registered data
    function authenticate(req, res, next) {
        var token_data = token.verify(req.headers.token);
        if (token_data) {
            next();
        } else {

            res.send('Not authorised');
        }
    }

    // Error handling middleware
    app.use(error_handler);

    //</editor-fold>


}