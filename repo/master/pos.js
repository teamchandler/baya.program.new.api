/**
 * Created by dev11 on 9/30/2015.
 */


var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var pos_schema = new Schema({
    "month" : { type: String, trim: true },
    "invoice_amount_excl_vat" : {type: Number},
    "pos" : {type: Number},
    "balance_amount" : {type: Number},
    _id : 0
});

var posSchema = new Schema({
    retailer        :   { type: String, trim: true },
    active          :   { type: Number },
    created_by      :   { type: String, trim: true },
    created_date    :   { type: Date },
    modified_by     :  { type: String, trim: true },
    modified_date   :  { type: Date },
    bucket          :  { type: String, trim: true },
    total_purchased :  {type: Number},
    total_uploaded  :  {type: Number},
    total_balance   :  {type: Number},
    pos_details     :  [pos_schema]

});

var include_fields = " -_id";
var default_sort = ""
// static methods

posSchema.statics.get_retailer_data = function(user){
    var pipeline =
        [
        {
            $match: {"retailer":user}
        },

        {$project: {_id:0, pos_details: '$pos_details'}}

    ];

    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}



posSchema.statics.update_pos_data = function(data,r_name,mod_by){

    var pos_data = data;
    console.log('Inside retailer');
    //  console.log(retailer_image_data);

    var q ={"retailer":r_name};
    return this.update
    (
        q,
        {
            "modified_by": mod_by,
            "modified_date" : new Date(),
            "pos_details":pos_data

        }
    ).exec();

}



posSchema.set('collection', 'pos')
//module.exports = mongoose.model('category', catSchema);

module.exports = mongo.get_mongoose_connection().model('pos', posSchema);
