/**
 * Created by developer6 on 4/25/2014.
 */
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var userschema = new Schema({
    user_id: { type: String,  trim: true }
    , company : { type: String,  trim: true }
    , region_id: { type: String,  trim: true }
    , region_name: { type: String,  trim: true }
    , title:{ type: String}

},{ versionKey: false });

userschema.set('collection', 'user_levels')
module.exports = mongo.get_mongoose_connection().model('user_levels', userschema);

