/**
 * Created by rahulguha on 12/06/14.
 */
/**
 * Created by developer6 on 4/22/2014.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var retailerSchema = new Schema({
    name : { type: String,  trim: true }
    , address: { type: String,  trim: true }
    , phone: { type: String,  trim: true }
    , active:{ type: Number}
    , city:{ type: String,  trim: true}
},{ versionKey: false });

var include_fields ="-_id name phone city";
var default_sort = "name"


retailerSchema.statics.get_retailer_list= function(){
    var q={"active" :1};
    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec();

}

retailerSchema.set('collection', 'retailer_master')
module.exports = mongo.get_mongoose_connection().model('retailer', retailerSchema);

