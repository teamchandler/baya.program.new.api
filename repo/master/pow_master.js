var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var powschema = new Schema({
    sku: {type: String, trim: true},
    description: {type: String, trim: true},
    start_dt: {type: String, trim: true},
    end_dt: {type: String, trim: true},
    active: {type: Number},
    created_by: {type: String, trim: true},
    created_dt: {type: String, trim: true}
}, {versionKey: false});

var include_fields = " sku description start_dt end_dt active created_by created_dt";

powschema.statics.get_pow_master = function () {
    //var claims = [];
    return this.find({"active":1})
        .select(include_fields)
        //.sort(default_sort)
        .exec();// Should return a Promise
};

powschema.set('collection', 'pow_master');
module.exports = mongo.get_mongoose_connection().model('pow_master', powschema);
