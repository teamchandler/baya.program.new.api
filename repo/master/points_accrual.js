/**
 * Created by dev11 on 1/22/2016.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');
var util = require('../../util.js');

var pointAccrualSchema = new Schema({
    user_id : { type: String, trim: true },
    accrual_point: { type: String, trim: true },
    invoice_number : { type: String, trim: true },
    created_date :{type: Date},
    invoice_amount:{type: Number},
    active : { type: Number}

},{ versionKey: false });

var include_fields = "user_id accrual_point active  -_id";
var default_sort = "";



pointAccrualSchema.set('collection', 'point_accrual')
module.exports = mongo.get_mongoose_connection().model('point_accrual', pointAccrualSchema);