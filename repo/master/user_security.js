/**
 * Created by Sriajn.
 */

//<editor-fold desc="Variable Declaration">
var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID
    ,util  = require('../../util');

var mongo = require('../../db_connect/mongoose.js');
//</editor-fold>


var user_securitySchema = new Schema(
    {
        user_id :  { type: String,  trim: true ,required: true, unique: true},
        email : { type: String,  trim: true },
        pwd : { type: String,  trim: true },
        verification : {
            status : { type: String,  trim: true },
            code : { type: String,  trim: true },
            started : { type: String,  trim: true },
            completed : { type: String,  trim: true },
            initiated_by : { type: String,  trim: true }
        }
    }
,{ versionKey: false });

var include_field = util.get_include_fieldjs("user_security");

//<editor-fold desc="Update & find &">

user_securitySchema.statics.update_upsert= function (q,data) {
    return this.update(
        q,data,{upsert: true}
    )
        .exec();
}

user_securitySchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

user_securitySchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}

user_securitySchema.statics.delete = function (query) {
    return this.remove(query)
        .exec();// Should return a Promise
}



//</editor-fold>


user_securitySchema.set('collection', 'user_security');

module.exports = mongo.get_mongoose_connection().model('user_security', user_securitySchema);
