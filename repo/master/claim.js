﻿/**

 * User: Hassan
 * Date: 10/03/14
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var sku_schema = new Schema({
    "sku" : { type: String, trim: true },
    "status": { type: String, trim: true },
    "quantity": {type: Number , trim: true},
    "description" : { type: String, trim: true },
    "unit_price" : {type: Number, trim: true },
    "amount":{type: Number, trim: true },
    "discount":{type: Number, trim: true },
    "is_free":{type: Number, trim: true },
    "model": { type: String,  trim: true},
    _id : 0
});

var claimSchema = new Schema({
    claim_id        :   { type: String, trim: true },
    user_id         :   { type: String,  trim: true },
    active          :   { type: Number },
    invoice_date    :   { type: Date },
    invoice_amount  :   {type: Number},
    invoice_number  :   { type: String,  trim: true },
    retailer        :   { type: String,  trim: true },
    status          :   { type: String,  trim: true },
    supporting_doc  :   { type: String,  trim: true },
    approval_comments        :   { type: String,  trim: true },
    claim_details:[sku_schema]
    });

var include_fields ="claim_id user_id  invoice_date invoice_number invoice_amount claim_details approval_comments  supporting_doc  -_id";
var default_sort = "invoice_date"
// static methods


claimSchema.statics.get_total_submitted_value = function(user_id){
    var pipeline = [
            {$match :
                {$and : [
                        {active : 1},{user_id : user_id},
                        {'status' : 'pending'}
                        ]
                }
            },
        {$group : {_id : "user_id", total : {$sum : "$invoice_amount"}}},
        {$project : {_id:0, total_submitted_sale : "$total"}}
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

claimSchema.statics.get_all_submissions = function(user_id, limit){
    var pipeline = [
        {$match : {$and : [{user_id : user_id},
            {active : 1}

        ]}},
        { $project : {_id:0, claim_id : 1, invoice_date : 1,
            'invoice_amount' : 1, 'invoice_number' : 1,
            'retailer' : 1, 'supporting_doc' : 1,
            'status' :1, 'claim_details' : 1, 'approval_comments': 1
        }},
        {$sort : {invoice_date : 1}},
        {$limit : parseInt( limit)}
    ];
    return this.aggregate (pipeline)
        .exec();// Should return a Promise
}

//claimSchema.statics.get_company_name= function(){
//
//    return this.find()
//        .select(include_fields)
//        // .sort(default_sort)
//        .exec();// Should return a Promise
//
//},

//    claimSchema.statics.get_user_name_by_company= function(name){
//
//        return this.find({company : name})
//            .select(include_fields)
//            // .sort(default_sort)
//            .exec();// Should return a Promise
//    },




claimSchema.set('collection', 'sales_registry')
//module.exports = mongoose.model('category', catSchema);

module.exports = mongo.get_mongoose_connection().model('sales_registry', claimSchema);
