/**
 * Created by dev11 on 1/27/2016.
 */


var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var paymentSchema = new Schema({
    retailer_name     :   { type: String, trim: true },
    invoice_number    :   { type: String, trim: true },
    active            :   { type: Number },
    payment_date      :   { type: Date },
    amount_received   :   {type: Number},
    payment_received  :   {type: String ,trim: true}


},{ versionKey: false });

var include_fields= "retailer_name invoice_number payment_date amount_received payment_received -_id";

paymentSchema.statics.get_field_by_invoice_number= function(number){

    return this.find({invoice_number : number})
        .select(include_fields)
        // .sort(default_sort)
        .exec();// Should return a Promise
};

paymentSchema.statics.get_duplicate_invoice= function(number){
    console.log("Payment");
    console.log(number);
    return this.find({invoice_number : number})
        .select(include_fields)
        // .sort(default_sort)
        .exec();// Should return a Promise
};

paymentSchema.statics.payment_update=function(data){
    var q ={"invoice_number": data.invoice_number };
    var  upd_qry={
        "retailer_name": data.retailer_name,
        "invoice_number" : data.invoice_number,
        "payment_date" : data.payment_date,
        "amount_received" : data.amount_received,
        "payment_received" : data.payment_received
    };

    return this.update(
        q,upd_qry
    )
        .exec();
};

paymentSchema.set('collection', 'payment');


module.exports = mongo.get_mongoose_connection().model('payment', paymentSchema);
