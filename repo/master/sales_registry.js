var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');

var sku_schema = new Schema({
    "sku": {type: String, trim: true},
    "status": {type: String, trim: true},
    "quantity": {type: Number, trim: true},
    "description": {type: String, trim: true},
    "unit_price": {type: Number, trim: true},
    "amount": {type: Number, trim: true},
    "discount": {type: Number, trim: true},
    "is_free": {type: Number, trim: true},
    "model": {type: String, trim: true},
    "series": {type: String, trim: true},
    "pow_status": {type: Number, trim: true},
    _id: 0
});

var approve_schema = new Schema({
    "comments": {type: String, trim: true},
    "user_type": {type: String, trim: true},
    "user_id": {type: String, trim: true},
    "comment_date": {type: Date},
    _id: 0
});

var amount_schema = new Schema({
    user_id: {type: String, trim: true},
    invoice_amount: {type: Number},
    track_date: {type: Date},
    invoice_amount_excl_vat: {type: Number},
    _id: 0

});


var inv_date_schema = new Schema({
    user_id: {type: String, trim: true},
    inv_date: {type: Date},
    track_date: {type: Date},
    _id: 0

});

//done by souvick
var payment_track_schema = new Schema({
    "amount_received": {type: String, trim: true},
    "payment_comment": {type: String, trim: true},
    "payment_received": {type: String, trim: true},
    "payment_date": {type: Date},
    "modified_date": {type: Date},
    "modified_by": {type: String, trim: true},
    _id: 0
});


var sales_registrySchema = new Schema({
    claim_id: {type: String, trim: true},
    user_id: {type: String, trim: true},
    active: {type: Number},
    invoice_date: {type: Date},
    invoice_amount: {type: Number},
    invoice_amount_excl_vat: {type: Number},
    invoice_number: {type: String, trim: true},
    retailer: {type: String, trim: true},
    status: {type: String, trim: true},
    supporting_doc: {type: String, trim: true},
    program_name: {type: String, trim: true},
    region: {type: String, trim: true},
    claim_details: [sku_schema],
    approval_comments: [approve_schema],
    amount_track: [amount_schema],
    inv_date_track: [inv_date_schema],
    payment_track: [payment_track_schema],
    company_name: {type: String, trim: true},
    created_date: {type: Date},
    modified_by: {type: String, trim: true},
    modified_date: {type: Date},
    ispoint_calculated: {type: Number},
    retailer_code: {type: String, trim: true},
    retailer_name: {type: String, trim: true},
    participant_id: {type: Number},
    verified_amount: {type: Number},
    upload_from: {type: String, trim: true},
    name: {type: String, trim: true},
    distributor_code: {type: String, trim: true},
    distributor_name: {type: String, trim: true},
    distributor_city: {type: String, trim: true},
    disti_champ_code: {type: String, trim: true},
    disti_champ_name: {type: String, trim: true},
    uploaded_month: {type: String, trim: true},
    payment_received: {type: String, trim: true},
    credit_period: {type: Date},
    within_credit_period: {type: String, trim: true},
    paid_amount: {type: String, trim: true},
    points_accrued: {type: String, trim: true},
    created_by: {type: String, trim: true}
}, {versionKey: false});

var include_fields = "active claim_id user_id invoice_date invoice_amount invoice_number retailer status supporting_doc claim_details approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name distributor_code retailer_name invoice_amount_excl_vat payment_received -_id payment_track";
var include_fields1 = "claim_id user_id invoice_date retailer_name invoice_amount invoice_number retailer claim_details status approval_comments amount_track inv_date_track company_name participant_code participant_name verified_amount name distributor_code invoice_amount_excl_vat";
var include_fields2 = "invoice_number -_id";
var include_fields3 = "retailer_name invoice_number invoice_amount invoice_date payment_date payment_received user_id status  -_id";
var include_fields4 = "credit_period invoice_date payment_track";
var default_sort = "invoice_date";
// static methods

//done by souvick on 29/1/16
sales_registrySchema.statics.payment_track_details = function (data) {
    //console.log('SR');
    //console.log(data);
    return this.find({invoice_number: data})
        .select(include_fields4)
        //.sort(default_sort)
        .exec();// Should return a Promise
    
}


sales_registrySchema.statics.update_invoice = function (data) {

    var q = {"claim_id": data.claim_id};

    return this.update(
        q,
        {
            "user_id": data.user_id,
            "company": data.company,
            "active": data.active,
            "invoice_number": data.invoice_number,
            "retailer": data.retailer,
            "status": data.status,
            "approval_comments": data.approval_comments
        }
    )
        .exec();
    
}


sales_registrySchema.statics.get_invoice_details_by_name_inv_no = function (data) {

    return this.find({user_id: data.user, invoice_number: data.inv})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.get_all_invoice = function (inv, retailer) {
console.log("entered schema")
    return this.find({invoice_number: inv, retailer: retailer})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
    

}




sales_registrySchema.statics.get_invoice_by_retailer = function (retailer) {

    return this.find({retailer: retailer})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.calc_user_id_wise_invoice_amount = function (user_id) {

//    return this.find({user_id : user_id})
//        .select(include_fields)
//        .sort(default_sort)
//        .exec();// Should return a Promise
    var pipeline = [
            {
                $unwind: "$claim_details"
            },
            {
                $match: {
                    $and: [
                        {status: "Verified & Approved"},
                        {user_id: user_id},
                        {active: 1}
                    ]
                }
            },
            {
                $project: {
                    _id: 0,
                    user_id: 1,
                    retailer_code: 1,
                    claim_details: 1
                }
            }
        ]
        ;
    console.log(pipeline);
    return this.aggregate(pipeline)

        .exec()

    


}


sales_registrySchema.statics.search_duplicate_invoice_list = function (invoice_number) {

//    var q="{sku: /^"+ sku +"$/i}";
    var q = {'invoice_number': invoice_number};

    console.log(q);

    return this.find(q)
        .select(include_fields)
        .sort(default_sort)
        .exec(); //Should return a Promise
    
}

sales_registrySchema.statics.update_invoice_claim = function (data, prog_name,retailer_points_object) {
    var q = {"claim_id": data.claim_id};
    console.log("data" +JSON.stringify(retailer_points_object));

    console.log("pr"+"nrml");
    var upd_qry =
    {
        "claim_details": data.claim_details,
        "status": data.status,
        "approval_comments": data.approval_comments,
        "invoice_amount": data.invoice_amount,
        "amount_track": data.amount_track,
        "inv_date_track": data.inv_date_track,
        "modified_by": data.modified_by,
        "modified_date": data.modified_date,
        "program_name": "nrml",
        "invoice_date": data.invoice_date,
        "invoice_amount_excl_vat": data.invoice_amount_excl_vat,
        "retailer_points_details":retailer_points_object

    }
        ;
    if (prog_name == "") {
        delete upd_qry.program_name;
        delete upd_qry.invoice_date;
    }

    return this.update(
        q, upd_qry, false, true
    )
        .exec();
    
}

sales_registrySchema.statics.get_claim_by_user = function (user_id) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1}, {user_id: user_id},
                    {'status': 'pending'}
                ]
            }
        }
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.get_total_sale_verified = function (user_id) {
    var pipeline = [{
        $match: {
            $and: [
                {active: 1}, {"user_id": user_id},
                {'status': 'Verified & Approved'},
                {"payment_received": "Y"},
                {"within_credit_period": "Y"}
            ]
        }
    },
        {$group: {_id: "user_id", total: {$sum: "$invoice_amount_excl_vat"}}},
        {$project: {_id: 0, total_sale: "$total"}}];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
}


sales_registrySchema.statics.get_points_pending_for_paymt_appv = function (user_id) {
    var pipeline = [{
        $match: {
            $and: [
                {active: 1}, {"user_id": user_id},
                {'status': 'Verified & Approved'},
                {"payment_received": "N"}
            ]
        }
    },
        {$group: {_id: "user_id", total: {$sum: "$invoice_amount_excl_vat"}}},
        {$project: {_id: 0, total_sale: "$total"}}];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.get_lostpoint = function (user_id) {
    var pipeline = ([{
        $match: {
            $and: [
                {active: 1}, {user_id: user_id},
                {'status': 'Verified & Approved'},
                {'within_credit_period': 'N'}
            ]
        }
    },
        {$group: {_id: "user_id", total: {$sum: "$invoice_amount_excl_vat"}}},
        {$project: {_id: 0, total_lost_sale: "$total"}}]);
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
}

//sales_registrySchema.statics.get_early_bird_winners = function(program_type){
sales_registrySchema.statics.get_early_bird_winners = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {program_name: 'nrml'},
                    {invoice_date: {$gte: new Date('04/01/2015')}},
                    {invoice_date: {$lte: new Date('12/31/2015')}}
                ]

            }
        },
        {
            $group: {
                _id: {'user_id': '$user_id', 'company': '$company_name'},
                'total_sale': {$sum: '$invoice_amount'}
            }
        },
        {
            $project: {
                _id: 0, "company": "$_id.company",
                "user_id": "$_id.user_id", "total_sale": 1
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.get_invoice_by_status_comp_name = function (status, company_name) {

    var q = {active: 1, status: status, company_name: company_name};

    if (status == "All") {
        delete q.status;
    }
    if (status == "payment_rec") {
        var q = {active: 1, 'payment_received': 'Y', 'company_name': company_name};
    }
    if (status == "payment_not_rec") {
        var q = {active: 1, 'payment_received': 'N', 'status': 'Verified & Approved', 'company_name': company_name};
    }
    if (company_name == "" || company_name == null || company_name == 'undefined') {
        delete q.company_name;
    }

    console.log(q);

    return this.find(q)
        .select(include_fields)
        // .sort(default_sort)
        .exec();// Should return a Promise
    

}

sales_registrySchema.statics.update_claim = function (data) {
    var q = {"claim_id": data.claim_id};

    return this.update(
        q,
        {
            "claim_details": data.claim_details

        }
    )
        .exec();
    
}

//done by souvick on 28/1/16
sales_registrySchema.statics.update_payment_recieved = function (data, arry, wcp, paid_amount) {
    var q = {"invoice_number": data.invoice_number};

    return this.update(
        q,
        {
            "payment_received": data.payment_received,
            "modified_by": data.user_id,
            "modified_date": new Date(),
            "payment_track": arry,
            "within_credit_period": wcp,
            "paid_amount": paid_amount

        }
    )
        .exec();
    
}

sales_registrySchema.statics.update_payment_recieved_new = function (data, arry, wcp, paid_amount) {
    var q = {"invoice_number": data.invoice_number};

    return this.update(
        q,
        {
            "payment_received": data.payment_received,
            "modified_by": data.user_id,
            "modified_date": new Date(),
            "payment_track": arry,
            "within_credit_period": wcp,
            "paid_amount": paid_amount


        }
    )
        .exec();

    
}

sales_registrySchema.statics.get_field_by_invoice_number = function (number) {

    return this.find({invoice_number: number})
        .select(include_fields3)
        // .sort(default_sort)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_inv_by_retailer = function (q) {
    return this.find(q).select(include_fields3).exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_all_company_name = function (q) {
    //
    //var pipeline =[
    //    {$group: { _id: {'retailer_name':'$retailer_name','retailer_code':'$retailer_code'}
    //    }},
    //    {$project: {
    //        _id: 0, "retailer_name" : "$_id.retailer_name","retailer_code" : "$_id.retailer_code"
    //    }}
    //];
    //return this.aggregate (pipeline).exec();// Should return a Promise

    return this.find(q).select("retailer_name retailer_code -_id").exec();// Should return a Promise
    
}

//end

sales_registrySchema.statics.get_field_by_invoice_number = function (number) {
    return this.find({invoice_number: number})
        .select("invoice_number invoice_amount invoice_date -_id")
        // .sort(default_sort)
        .exec();// Should return a Promise
    
};


sales_registrySchema.statics.get_invoice_by_number = function (number) {

    return this.find({claim_id: number})
        .select(include_fields1)
        // .sort(default_sort)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.get_invoice_by_user_id = function (date, inv_no, inv_amount) {

    //var p = {"invoice_number":new RegExp(inv_no ,'i'),"invoice_date":date,"invoice_amount":inv_amount};
    var p = {"invoice_number": inv_no, "invoice_date": date, "invoice_amount": inv_amount};


    console.log(p);

    //  var q_data ={invoice_number:"/^"+ inv_no +"/"};

    // return this.find({invoice_number:{ $regex: /^"+ inv_no +"/  ,$options: 'i'}})
    return this.find(p)
        .select(include_fields1)
        // .sort(default_sort)
        .exec();// Should return a Promis
    
}


sales_registrySchema.statics.get_invoice_details_by_user_id = function (user_id) {

    var p = {"user_id": user_id};


    console.log(p);

    return this.find(p)
        .select(include_fields1)
        // .sort(default_sort)
        .exec();// Should return a Promis
    
}

//Schneider Reports

sales_registrySchema.statics.get_top_si = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}]
            }
        },
        {
            $group: {
                _id: "$retailer",
                total_sale: {$sum: "$invoice_amount"}
            }
        },
        {$project: {_id: 0, retailer: '$_id', total_sale: '$total_sale'}},
        {$sort: {total_sale: -1}},
        {$limit: 15}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
}

sales_registrySchema.statics.get_top_si_by_region = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}]
            }
        },
        {
            $group: {
                _id: {'region': '$region', 'SI': "$company_name"},
                total_sale: {$sum: "$invoice_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, region: '$_id.region', 'SI': '$_id.SI',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};


sales_registrySchema.statics.get_si_by_region = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {region: {$in: ['East', 'West', 'North', 'South']}}]
            }
        },
        {
            $group: {
                _id: {'region': '$region'},
                total_sale: {$sum: "$invoice_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, region: '$_id.region',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_user_proceedings_invoice_current_month = function (user, current_date) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {invoice_date: {$gte: new Date(current_date)}},
                    {user_id: user}
                ]
            }

        },

        /*{$group: { _id: {'user_id' : '$user_id'},
         total_sale: {$sum: "$invoice_amount"},
         total_count : {$sum : 1}

         }},*/
        {
            $project: {
                _id: 0,
                companyname: '$company_name',
                invoiceAmount: '$invoice_amount',
                invoiceDate: '$invoice_date'
            }
        },
        {$sort: {total_sale: -1}}

    ];

    console.log(pipeline);

    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.special_program_qualifier = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {program_name: 'spl1'}
                ]
            }
        },
        {
            $group: {
                _id: '$user_id',
                total_sale: {$sum: "$invoice_amount"}
            }
        },
        {
            $match: {total_sale: {$gte: 100000}}
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.normal_program_qualifier = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {program_name: 'nrml'}
                ]
            }
        },
        {
            $group: {
                _id: '$user_id',
                total_sale: {$sum: "$invoice_amount"}
            }
        },
        {
            $match: {total_sale: {$gte: 100000}}
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.top_sku = function () {
    var pipeline = [{$unwind: "$claim_details"}, {
        $match: {
            $and: [
                {active: 1},
                {'status': 'Verified & Approved'},

            ]
        }
    },
        {$group: {_id: {'sku': "$claim_details.sku"}, total: {$sum: "$claim_details.quantity"}}},
        {$project: {_id: 0, sku: '$_id.sku', total_sku_qty: "$total"}}, {$sort: {total_sku_qty: -1}}, {$limit: 5}];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.top_distributor_invoice_amount = function () {


    var pipeline = [{
        $match: {
            $and: [
                {active: 1},
                {'status': 'Verified & Approved'},

            ]
        }
    },
        {$group: {_id: {'retailer': "$retailer"}, total: {$sum: "$invoice_amount_excl_vat"}}},
        {
            $project: {
                _id: 0,
                distributor: '$_id.retailer',
                total_sale: "$total"
            }
        }, {$sort: {total_sale: -1}}, {$limit: 5}];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_all_users = function () {


    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}

                ]

            }
        },
        {$group: {_id: '$user_id', 'total_sale': {$sum: '$invoice_amount'}}}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    


};

sales_registrySchema.statics.get_details_by_user = function (user_id) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {"user_id": user_id}
                ]
            }
        },
        {
            $group: {
                _id: {'program_name': '$program_name', 'user_id': "$user_id"},
                total_sale: {$sum: "$invoice_amount_excl_vat"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, program_name: '$_id.program_name', 'user_id': '$_id.user_id',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },

        {$sort: {'user_id': 1}}


    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_sku_of = function (ids) {

    var myarray = ids;
    var barray = [];
    for (var i = 0; i < myarray.length; i++) {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline = [
        {
            $match: {
                $or: [
                    {participant_id: {$in: barray}}]
            }
        },
        {$project: {_id: 0, claim_details: '$claim_details', cds: '$claim_details.quantity'}},
        {$sort: {cds: -1}},
        {$limit: 3}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_dets_nutshell = function (ids) {

    var myarray = ids;
    var barray = [];
    for (var i = 0; i < myarray.length; i++) {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: {$in: barray}}]
            }
        },
        {
            $project: {
                _id: 0,
                name: '$name',
                verified_amount: '$verified_amount',
                claim_details: '$claim_details',
                participant_id: '$participant_id'
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_participant_details = function (participantid) {


    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: parseInt(participantid)}]
            }
        },
        {$project: {_id: 0, name: '$name', claim_details: '$claim_details', participant_id: '$participant_id'}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_user_wise_total_claim = function (user_email) {

    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {user_id: user_email}]
            }
        },
        {
            $group: {
                _id: {'program_name': '$program_name', 'user_id': "$user_id"},
                total_sale: {$sum: "$invoice_amount_excl_vat"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, program_name: '$_id.program_name', 'user_id': '$_id.user_id',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    


};

sales_registrySchema.statics.get_details_total_sale_by_user = function (user_id) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {"user_id": user_id}
//            {"status":"Verified & Approved"}
                ]
            }
        },
        {
            $group: {
                _id: {'status': '$status', 'user_id': "$user_id"},
                total_sale: {$sum: "$invoice_amount_excl_vat"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, status: '$_id.status', 'user_id': '$_id.user_id',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        }


    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_user_si_region_dashboard = function (regionName) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {region: regionName},
                    {status: 'Verified & Approved'}]
            }
        },
        {
            $group: {
                _id: {'region': '$region', 'SI': "$company_name", 'user_id': "$user_id"},
                total_sale: {$sum: "$invoice_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, region: '$_id.region', 'SI': '$_id.SI', 'user_id': '$_id.user_id',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_user_si_allIndia_dashboard = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}]
            }
        },
        {
            $group: {
                _id: {'region': '$region', 'SI': "$company_name", 'user_id': "$user_id"},
                total_sale: {$sum: "$invoice_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, region: '$_id.region', 'SI': '$_id.SI', 'user_id': '$_id.user_id',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.post_real_total_sale_branch = function (ids) {

    var myarray = ids;
    var barray = [];
    for (var i = 0; i < myarray.length; i++) {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: {$in: barray}}]
            }
        },
        {
            $group: {
                _id: {'participant_id': '$participant_id', 'retailer': '$retailer'},
                total_sale: {$sum: "$verified_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, participant_id: '$_id.participant_id',
                'retailer': '$_id.retailer',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.post_real_total_sale_branch_hq = function (ids) {

    var myarray = ids;
    var barray = [];
    for (var i = 0; i < myarray.length; i++) {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: {$in: barray}}]
            }
        },
        {
            $group: {
                _id: {'participant_id': '$participant_id', 'retailer': '$retailer'},
                total_sale: {$sum: "$invoice_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, participant_id: '$_id.participant_id',
                'retailer': '$_id.retailer',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.post_real_total_sale_dealer_normal = function (ids) {

    var myarray = ids;
    var barray = [];
    for (var i = 0; i < myarray.length; i++) {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: {$in: barray}}]
            }
        },
        {
            $group: {
                _id: {'participant_id': '$participant_id', 'retailer': '$retailer'},
                total_sale: {$sum: "$invoice_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, participant_id: '$_id.participant_id',
                'retailer': '$_id.retailer',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_pts_of = function (ids) {

    var myarray = ids;
    var barray = [];
    for (var i = 0; i < myarray.length; i++) {
        barray.push(parseInt(myarray[i].participant_id));
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: {$in: barray}}]
            }
        },
        {
            $group: {
                _id: {'participant_id': '$participant_id'},
                total_sale: {$sum: "$total_cal_pt"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, participant_id: '$_id.participant_id',
                'retailer': '$_id.retailer',
                total_sale: '$total_sale', total_count: '$total_count'
            }
        },
        {$sort: {total_sale: -1}}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_ptsdetails = function (participantid) {


    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: parseInt(participantid)}]
            }
        },
        {
            $group: {
                _id: {'participant_id': '$participant_id'},
                total_pts: {$sum: "$total_cal_pt"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, participant_id: '$_id.participant_id',
                'retailer': '$_id.retailer',
                total_pts: '$total_pts', total_count: '$total_count'
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_saledetails = function (participantid) {


    var pipeline = [
        {
            $match: {
                $and: [
                    {participant_id: parseInt(participantid)}]
            }
        },
        {
            $group: {
                _id: {'participant_id': '$participant_id'},
                total_sale_count: {$sum: "$verified_amount"},
                total_count: {$sum: 1}

            }
        },
        {
            $project: {
                _id: 0, participant_id: '$_id.participant_id',
                'retailer': '$_id.retailer',
                total_sale_count: '$total_sale_count', total_count: '$total_count'
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_all_skus = function () {

    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}
                ]

            }
        },
        {$unwind: '$claim_details'},
        {$group: {_id: "$claim_details.sku", total_qty: {$sum: "$claim_details.quantity"}}}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_month_wise_sku_qty = function (sku, month) {

    var pipeline = [{
        $match: {
            $and: [
                {active: 1},
                {status: 'Verified & Approved'}
            ]
        }
    },
        {
            $project: {
                user_id: '$user_id',
                invoice_number: "$invoice_number",
                claim_details: "$claim_details",
                month: {$month: '$invoice_date'}
            }
        },
        {
            $match: {
                $and: [
                    {month: month},
                    {claim_details: {"$elemMatch": {sku: sku}}}
                ]
            }
        }];

    //  console.log(pipeline);

    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_today_sku_qty = function (sku, year, month, day) {

    var pipeline = [{
        $match: {
            $and: [
                {active: 1},
                {status: 'Verified & Approved'}
            ]

        }
    },
        {
            $project: {
                user_id: '$user_id', invoice_number: "$invoice_number",
                claim_details: "$claim_details",
                year: {$year: '$modified_date'},
                month: {$month: '$modified_date'},
                day: {$dayOfMonth: '$modified_date'}

            }
        },
        {
            $match: {
                $and: [
                    {year: year},
                    {month: month},
                    {day: day},
                    {claim_details: {"$elemMatch": {sku: sku}}}
                ]
            }
        }];

//       console.log(pipeline);

    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    


};

sales_registrySchema.statics.get_month_wise_invoice_amount = function (month, retailer) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {uploaded_month: month},
                    {company_name: retailer}

                ]

            }
        },
        {
            $group: {
                _id: {'company_name': '$company_name', 'uploaded_month': '$uploaded_month'},
                'total_sale': {$sum: '$invoice_amount_excl_vat'}
            }
        },
        {
            $project: {
                _id: 0,
                "company_name": "$_id.company_name", "uploaded_month": "$_id.uploaded_month", "total_sale": 1
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_invoice_date_of_company = function (comp) {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    {company_name: comp}

                ]
            }
        },
        {
            $project: {
                "company_name": "$company_name", "uploaded_month": "$uploaded_month", "invoice_date": "$invoice_date"
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_distiwise_sku_detail = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}
                ]
            }
        },
        {$unwind: "$claim_details"},
        {
            $group: {
                _id: {'sku': '$claim_details.sku', 'distributor': '$retailer', 'region': '$region'}, count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                'sku': '$_id.sku',
                'region': '$_id.region',
                'distributor': '$_id.distributor',
                'count': '$count'
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

// Disti wise SKU List order by Desc

sales_registrySchema.statics.get_distiwise_sku_detail_desc = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'}
                ]
            }
        },
        {$unwind: "$claim_details"},
        {
            $group: {
                _id: {'sku': '$claim_details.sku', 'distributor': '$retailer', 'region': '$region'}, count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                'sku': '$_id.sku',
                'region': '$_id.region',
                'distributor': '$_id.distributor',
                'count': '$count'
            }
        },
        {$sort: {count: -1}},
        {$limit: 50}

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

// Region wise SKU details

sales_registrySchema.statics.get_regionwise_sku_detail = function () {
    var pipeline = [
        {
            $match: {
                $and: [
                    {"active": 1},
                    {"status": "Verified & Approved"}
                ]
            }
        },
        {$unwind: "$claim_details"},
        {
            $group: {
                _id: {'sku': '$claim_details.sku', 'region': '$region'},
                count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                sku: '$_id.sku',
                region: '$_id.region',
                count: '$count'
            }
        }

    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.get_invoice_list = function () {
    var q = {"active": 1};
    return this.find(q)
        .select(include_fields2)
        .sort()
        .exec();

};

sales_registrySchema.statics.get_invoice_by_name = function (user) {

    return this.find({user_id: user})
        .select(include_fields)
        .sort(default_sort)
        .exec();// Should return a Promise
    
};

////////////////////////////////////////////////////////////////////////////////////////////////////////

sales_registrySchema.statics.get_top_retailer_user_wise = function (user_id, user_type) {

    var obj = {};

    if (user_type) {

        if (user_type == "DistiChamp") {
            obj = {disti_champ_id: user_id};
        }
        if (user_type == "Distributor") {
            obj = {distributor_id: user_id};
        }
        if (user_type == "RM") {
            obj = {rm_code: user_id};
        }
        if (user_type == "Sales Head") {
            obj = {sales_head_code: user_id};
        }
        if (user_type == "Company") {
            obj = {};
        }
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    obj
                ]
            }
        },
        {
            $group: {
                _id: {'user_id': "$user_id", 'company_name': "$company_name"},
                total_invoice_amount: {$sum: "$invoice_amount"},
                total_invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
            }
        },
        {
            $project: {
                _id: 0, company_name: '$_id.company_name', 'user_id': '$_id.user_id',
                total_invoice_amount: '$total_invoice_amount',
                total_invoice_amount_excl_vat: '$total_invoice_amount_excl_vat'
            }
        },
        {$sort: {total_invoice_amount_excl_vat: -1}},
        {$limit: 5}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.get_top_five_distributor = function (user_id, user_type) {

    var obj = {};

    if (user_type) {
        if (user_type == "RM") {
            obj = {rm_code: user_id};
        }
        if (user_type == "Sales Head") {
            obj = {sales_head_code: user_id};
        }
        if (user_type == "Company") {
            obj = {};
        }
    }

    var pipeline = [
        {
            $match: {
                $and: [
                    {active: 1},
                    {status: 'Verified & Approved'},
                    obj
                ]
            }
        },
        {
            $group: {
                _id: {'distributor_code': "$distributor_code", 'distributor_name': "$distributor_name"},
                total_invoice_amount: {$sum: "$invoice_amount"},
                total_invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
            }
        },
        {
            $project: {
                _id: 0, distributor_name: '$_id.distributor_name', 'distributor_code': '$_id.distributor_code',
                total_invoice_amount: '$total_invoice_amount',
                total_invoice_excl_vat: '$total_invoice_amount_excl_vat'
            }
        },
        {$sort: {total_invoice_excl_vat: -1}},
        {$limit: 5}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.top_sku_user_wise = function (user_id, user_type) {

    var obj = {};

    if (user_type) {
        if (user_type == "DistiChamp") {
            obj = {disti_champ_id: user_id};
        }
        if (user_type == "Distributor") {
            obj = {distributor_id: user_id};
        }
        if (user_type == "RM") {
            obj = {rm_code: user_id};
        }
        if (user_type == "Sales Head") {
            obj = {sales_head_code: user_id};
        }
        if (user_type == "Company") {
            obj = {};
        }
    }

    var pipeline = [
        {$unwind: "$claim_details"},
        {
            $match: {
                $and: [
                    {active: 1},
                    {'status': 'Verified & Approved'},
                    obj

                ]
            }
        },
        {$group: {_id: {'sku': "$claim_details.sku"}, total: {$sum: "$claim_details.quantity"}}},
        {$project: {_id: 0, sku: '$_id.sku', total_sku_qty: "$total"}},
        {$sort: {total_sku_qty: -1}},
        {$limit: 5}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    
};

sales_registrySchema.statics.company_sales_details = function () {

    var pipeline = [
        {$match: { $and: [
            {active: 1},
            {status: 'Verified & Approved'}
        ]
        }
        },
        {$group: { _id: {'sales_head_name': "$sales_head_name", 'sales_head_code': "$sales_head_code"},
            total_invoice_amount: {$sum: "$invoice_amount"},
            invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
        }},
        {$project: {_id: 0, name: '$_id.sales_head_name', 'email_id': '$_id.sales_head_code',
            total_invoice_amount: '$total_invoice_amount',
            invoice_amount_excl_vat: '$invoice_amount_excl_vat'
        }}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.sales_head_sales_details = function (email_id) {

    var pipeline = [
        {$match: { $and: [
            {active: 1},
            {sales_head_code : email_id},
            {status: 'Verified & Approved'}
        ]
        }
        },
        {$group: { _id: {'rm_name': "$rm_name", 'rm_code': "$rm_code"},
            total_invoice_amount: {$sum: "$invoice_amount"},
            invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
        }},
        {$project: {_id: 0, 'name': '$_id.rm_name', 'email_id': '$_id.rm_code',
            total_invoice_amount: '$total_invoice_amount',
            invoice_amount_excl_vat: '$invoice_amount_excl_vat'
        }}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.rm_sales_details = function (email_id) {

    var pipeline = [
        {$match: { $and: [
            {active: 1},
            {rm_code : email_id},
            {status: 'Verified & Approved'}
        ]
        }
        },
        {$group: { _id: {'distributor_name': "$distributor_name", 'distributor_id': "$distributor_id"},
            total_invoice_amount: {$sum: "$invoice_amount"},
            invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
        }},
        {$project: {_id: 0, 'name': '$_id.distributor_name', 'email_id': '$_id.distributor_id',
            total_invoice_amount: '$total_invoice_amount',
            invoice_amount_excl_vat: '$invoice_amount_excl_vat'
        }}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.distributor_sales_details = function (email_id) {

    // var pipeline = [
    //     {$match: { $and: [
    //         {active: 1},
    //         {distributor_id : email_id},
    //         {status: 'Verified & Approved'}
    //     ]
    //     }
    //     },
    //     {$group: { _id: {'disti_champ_id': "$disti_champ_id", 'disti_champ_name': "$disti_champ_name"},
    //         total_invoice_amount: {$sum: "$invoice_amount"},
    //         invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
    //     }},
    //     {$project: {_id: 0, 'name': '$_id.disti_champ_name', 'email_id': '$_id.disti_champ_id',
    //         total_invoice_amount: '$total_invoice_amount',
    //         invoice_amount_excl_vat: '$invoice_amount_excl_vat'
    //     }}
    // ];

    var pipeline = [
        {$match: { $and: [
            {active: 1},
            {distributor_id : email_id},
            {status: 'Verified & Approved'}
        ]
        }
        },
        {$group: { _id: {'disti_champ_id': "$disti_champ_id", 'disti_champ_name': "$disti_champ_name"},
            total_invoice_amount: {$sum: "$invoice_amount"},
            invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
        }},
        {$project: {_id: 0, 'email_id': '$_id.disti_champ_id', 'name': '$_id.disti_champ_name',
            total_invoice_amount: '$total_invoice_amount',
            invoice_amount_excl_vat: '$invoice_amount_excl_vat'
        }}
    ];

    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.disti_champ_sales_details = function (email_id) {

    var pipeline = [
        {$match: { $and: [
            {active: 1},
            {disti_champ_id : email_id},
            {status: 'Verified & Approved'}
        ]
        }
        },
        {$group: { _id: {'user_id': "$user_id", 'retailer_name': "$retailer_name"},
            total_invoice_amount: {$sum: "$invoice_amount"},
            invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
        }},
        {$project: {_id: 0, 'user_id': '$_id.user_id', 'retailer_name': '$_id.retailer_name',
            total_invoice_amount: '$total_invoice_amount',
            invoice_amount_excl_vat: '$invoice_amount_excl_vat'
        }}
    ];
    return this.aggregate(pipeline)
        .exec();// Should return a Promise
    

};

sales_registrySchema.statics.retailer_sales_details = function (email_id) {

    var pipeline = [
        {$match: { $and: [
            {active: 1},
            {user_id : email_id},
            {status: 'Verified & Approved'}
        ]
        }
        },
        {$group: { _id: {'user_id': "$user_id", 'retailer_name': "$retailer_name", "invoice_number" : "$invoice_number", "supporting_doc" : "$supporting_doc"},
            total_invoice_amount: {$sum: "$invoice_amount"},
            invoice_amount_excl_vat: {$sum: "$invoice_amount_excl_vat"}
        }},
        {$project: {_id: 0, 'user_id': '$_id.user_id', 'retailer_name': '$_id.retailer_name',
            invoice_number : "$_id.invoice_number",
            supporting_doc : "$_id.supporting_doc",
            total_invoice_amount: '$total_invoice_amount',
            invoice_amount_excl_vat: '$invoice_amount_excl_vat'
        }}
    ];

    return this.aggregate(pipeline)
        .exec();// Should return a Promise

    
};

////////////////////////////////////////////////////////////////////////////////////////////////////////

sales_registrySchema.set('collection', 'sales_registry');
//module.exports = mongoose.model('category', catSchema);

module.exports = mongo.get_mongoose_connection().model('sales_registry', sales_registrySchema);
